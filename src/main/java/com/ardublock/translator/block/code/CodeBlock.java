package com.ardublock.translator.block.code;

import com.ardublock.translator.Translator;
import com.ardublock.translator.block.TranslatorBlock;
import com.ardublock.translator.block.exception.SocketNullException;
import com.ardublock.translator.block.exception.SubroutineNotDeclaredException;

public class CodeBlock extends TranslatorBlock
{
	public CodeBlock(Long blockId, Translator translator, String codePrefix, String codeSuffix, String label)
	{
		super(blockId, translator, codePrefix, codeSuffix, label);
	}

	@Override
	public String toCode() throws SocketNullException, SubroutineNotDeclaredException
	{
		/*Old version with String-Blocks
		TranslatorBlock translatorBlock = this.getRequiredTranslatorBlockAtSocket(0);
		String ret = translatorBlock.toCode();
		//remove " from StringBlock but leave " from user input
		if(ret.contains("\"\"")){
			ret = ret.replaceAll("\"\"", "\"")+"\n";//.replaceAll("\"", "")+"\n";
		}else {
			ret = ret.replaceAll("\"", "")+"\n";
		}
		*/
		return label;
	}
}
