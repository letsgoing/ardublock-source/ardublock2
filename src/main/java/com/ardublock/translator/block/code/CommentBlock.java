package com.ardublock.translator.block.code;

import com.ardublock.translator.Translator;
import com.ardublock.translator.block.TranslatorBlock;
import com.ardublock.translator.block.exception.SocketNullException;
import com.ardublock.translator.block.exception.SubroutineNotDeclaredException;

import tec.letsgoing.ardublock.simulator.simcode.SimCode;

public class CommentBlock extends TranslatorBlock
{
	public CommentBlock(Long blockId, Translator translator, String codePrefix, String codeSuffix, String label)
	{
		super(blockId, translator, codePrefix, codeSuffix, label);
	}

	@Override
	public String toCode() throws SocketNullException, SubroutineNotDeclaredException
	{
		/*Old version with String-Blocks
		TranslatorBlock translatorBlock = this.getRequiredTranslatorBlockAtSocket(0);
		String comment = translatorBlock.toCode().replaceAll("\"", "");
		String ret = "\n//"+ comment+"\n";
		return ret;*/
		
		if(label.startsWith("//")){
			return "\n"+ label+"\n";
		}
		else {
			return "\n//"+ label+"\n";
		}
	}
	
	@Override
	public SimCode toSim() {
		//SimCodeObject sim = new SimCodeObject(); //-> return empty object or null?
		return null;
	}
}
