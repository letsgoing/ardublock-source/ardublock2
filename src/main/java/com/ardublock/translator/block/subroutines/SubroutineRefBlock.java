package com.ardublock.translator.block.subroutines;

import com.ardublock.translator.Translator;
import com.ardublock.translator.block.TranslatorBlock;
import com.ardublock.translator.block.exception.SocketNullException;
import com.ardublock.translator.block.exception.SubroutineNotDeclaredException;

public class SubroutineRefBlock extends TranslatorBlock
{

	public SubroutineRefBlock(Long blockId, Translator translator,
			String codePrefix, String codeSuffix, String label) {
		super(blockId, translator, codePrefix, codeSuffix, label);
	}

	@Override
	public String toCode() throws SocketNullException, SubroutineNotDeclaredException
	{
		String subroutineName = label.trim();
		String var = "";
		
		if (!translator.containFunctionName(subroutineName))
		{
			throw new SubroutineNotDeclaredException(blockId);
		}
		
		TranslatorBlock translatorBlock = this.getTranslatorBlockAtSocket(0);
		//if Parameter exists 
		//TODO: add loop for reading Parameters
		if(translatorBlock != null) {
			var = translatorBlock.toCode().replaceAll("\\s*_.new\\b\\s*", "");
		}
		return subroutineName + "("+var+")";
	}
}
