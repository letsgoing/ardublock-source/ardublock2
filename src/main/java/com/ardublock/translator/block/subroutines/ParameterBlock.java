package com.ardublock.translator.block.subroutines;

import java.util.ResourceBundle;

import com.ardublock.translator.Translator;
import com.ardublock.translator.block.TranslatorBlock;
import com.ardublock.translator.block.exception.BlockException;
import com.ardublock.translator.block.exception.SocketNullException;
import com.ardublock.translator.block.exception.SubroutineNotDeclaredException;
import com.ardublock.translator.block.numbers.LocalVariableCharBlock;
import com.ardublock.translator.block.numbers.LocalVariableDigitalBlock;
import com.ardublock.translator.block.numbers.LocalVariableNumberBlock;

public class ParameterBlock extends TranslatorBlock
{
	private static ResourceBundle uiMessageBundle = ResourceBundle.getBundle("com/ardublock/block/ardublock");
	
	public ParameterBlock(Long blockId, Translator translator, String codePrefix, String codeSuffix, String label)
	{
		super(blockId, translator, codePrefix, codeSuffix, label);
	}

	@Override
	public String toCode() throws SocketNullException, SubroutineNotDeclaredException
	{	
		String splitMarker ="";// "><"; //split marker
		String ret = "";
		String var = "";
		
		TranslatorBlock tB_var = this.getTranslatorBlockAtSocket(0);
		
		TranslatorBlock tB_nextParam = this.getTranslatorBlockAtSocket(1);
		
		
		if(tB_var != null){
			var = tB_var.toCode().replaceAll("\\s*_.new\\b\\s*", "");
			
			if (tB_var instanceof LocalVariableNumberBlock) {
				ret = "int " + splitMarker + var;
			}
			else if(tB_var instanceof LocalVariableDigitalBlock){
				ret = "bool "+ splitMarker + var;
			}
			else if(tB_var instanceof LocalVariableCharBlock){
				ret = "char "+ splitMarker + var;
				
			}else{
				throw new BlockException(blockId, uiMessageBundle.getString("ardublock.error_msg.local_var_slot"));
			}
		}
		//add comma for next parameter
		if(tB_nextParam != null) {
			ret += ", " + tB_nextParam.toCode().replaceAll("\\s*_.new\\b\\s*", "");
		}
		
		return  ret;
	}
}