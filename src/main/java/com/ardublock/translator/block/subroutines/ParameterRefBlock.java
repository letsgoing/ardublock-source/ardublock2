package com.ardublock.translator.block.subroutines;

import com.ardublock.translator.Translator;
import com.ardublock.translator.block.TranslatorBlock;
import com.ardublock.translator.block.exception.SocketNullException;
import com.ardublock.translator.block.exception.SubroutineNotDeclaredException;

public class ParameterRefBlock extends TranslatorBlock
{	
	public ParameterRefBlock(Long blockId, Translator translator, String codePrefix, String codeSuffix, String label)
	{
		super(blockId, translator, codePrefix, codeSuffix, label);
	}

	@Override
	public String toCode() throws SocketNullException, SubroutineNotDeclaredException
	{	
		String ret = "";

		TranslatorBlock tB_var = this.getTranslatorBlockAtSocket(0);
		TranslatorBlock tB_nextParam = this.getTranslatorBlockAtSocket(1);
		
		ret = tB_var.toCode().replaceAll("\\s*_.new\\b\\s*", "");;
		
		//add comma for next parameter
		if(tB_nextParam != null) {
			ret += ", " + tB_nextParam.toCode().replaceAll("\\s*_.new\\b\\s*", "");
		}
		return  ret;
	}
}