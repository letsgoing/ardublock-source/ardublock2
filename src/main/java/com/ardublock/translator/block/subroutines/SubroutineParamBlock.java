package com.ardublock.translator.block.subroutines;

import java.util.ResourceBundle;

import com.ardublock.translator.Translator;
import com.ardublock.translator.block.TranslatorBlock;
import com.ardublock.translator.block.exception.BlockException;
import com.ardublock.translator.block.exception.SocketNullException;
import com.ardublock.translator.block.exception.SubroutineNotDeclaredException;

public class SubroutineParamBlock extends TranslatorBlock
{
	private static ResourceBundle uiMessageBundle = ResourceBundle.getBundle("com/ardublock/block/ardublock");
	
	public SubroutineParamBlock(Long blockId, Translator translator,
			String codePrefix, String codeSuffix, String label) {
		super(blockId, translator, codePrefix, codeSuffix, label);
	}

	@Override
	public String toCode() throws SocketNullException, SubroutineNotDeclaredException
	{
		String subroutineName = label.trim();
		String type = "";
		String comment = "";
		String parameter = "";
		String code = "";
		String ret = "";
		String retArray[]= {"","","","","","","",""};
		
		TranslatorBlock tb_comment = getRequiredTranslatorBlockAtSocket(0);
		comment = "//"+tb_comment.toCode().replaceAll("\"", "")+"\n";
		
		TranslatorBlock tb_parameter = getTranslatorBlockAtSocket(1);
		if(tb_parameter != null) {
			parameter = tb_parameter.toCode().replaceAll("\\s*_.new\\b\\s*", "");
		} 
		
		TranslatorBlock tb_code = getTranslatorBlockAtSocket(2);
		while (tb_code != null){
			code += tb_code.toCode();
			tb_code = tb_code.nextTranslatorBlock();
		}
		
		TranslatorBlock tb_return = getTranslatorBlockAtSocket(3);
		if(tb_return != null) {
			ret = tb_return.toCode().replaceAll("\\s*_.new\\b\\s*", "");
			retArray = ret.split(",");
			//if more than one parameter at return
			//TODO test
			if(retArray.length > 1) {
				throw new BlockException(blockId, uiMessageBundle.getString("ardublock.error_msg.return_var_slot"));
			}
			retArray = ret.split(" ");
			type = retArray[0]; //DATATYPE
			ret = " " + retArray[1];  //VARNAME
			
		}else {
			type = "void";
		}
		
		translator.addDefinitionCommand(type + " " + subroutineName + "(" + parameter + ");\n");

		return comment + type + " " + subroutineName + "(" + parameter+ "){\n" + code + "\n" + "return" + ret + ";\n }\n";
	}
}


//separieren der Parameter erst nach Komma -> einzelner Parameter mit Typ -> dann nach Leerzeichen -> [0] Typ; [1] Name
