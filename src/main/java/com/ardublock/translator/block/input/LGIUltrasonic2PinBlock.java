package com.ardublock.translator.block.input;

import com.ardublock.translator.Translator;
import com.ardublock.translator.block.TranslatorBlock;
import com.ardublock.translator.block.exception.SocketNullException;
import com.ardublock.translator.block.exception.SubroutineNotDeclaredException;

public class LGIUltrasonic2PinBlock extends TranslatorBlock
{
	public LGIUltrasonic2PinBlock(Long blockId, Translator translator, String codePrefix,	String codeSuffix, String label)
	{
		super(blockId, translator, codePrefix, codeSuffix, label);
	}

	private final static String ultraSonic2PinFunction = "\nint ultraschallSensor2Pin(int triggerPin, int echoPin){\n long duration, cm; \n\n //starte Trigger \n digitalWrite(triggerPin, LOW);  \n delayMicroseconds(2); \n digitalWrite(triggerPin, HIGH); \n delayMicroseconds(10); \n digitalWrite(triggerPin, LOW); \n\n //messe Echo-Laufzeit \n duration = pulseIn(echoPin, HIGH); \n\n //berechne Abstand \n cm =  duration / 29.1 / 2; \n return cm;\n }\n";
	
	public String toCode() throws SocketNullException, SubroutineNotDeclaredException
	{
		
		TranslatorBlock tb_trigger = this.getRequiredTranslatorBlockAtSocket(0);
		TranslatorBlock tb_echo = this.getRequiredTranslatorBlockAtSocket(1);
		String triggerPin = tb_trigger.toCode().replaceAll("\\s*_.new\\b\\s*", "");
		String echoPin = tb_echo.toCode().replaceAll("\\s*_.new\\b\\s*", "");
		
		translator.addSetupCommand("pinMode( " + triggerPin + " , INPUT); // Ultraschallsensor Trigger-Pin");
		translator.addSetupCommand("pinMode( " + echoPin + " , INPUT); // Ultraschallsensor Echo-Pin\n");
		
		translator.addDefinitionCommand(ultraSonic2PinFunction);
		
		String ret = "ultraschallSensor2Pin(" + triggerPin + ", "+ echoPin + ")";
		

		return codePrefix + ret + codeSuffix;
	}
}