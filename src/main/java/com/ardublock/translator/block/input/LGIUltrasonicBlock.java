package com.ardublock.translator.block.input;

import com.ardublock.translator.Translator;
import com.ardublock.translator.block.TranslatorBlock;
import com.ardublock.translator.block.exception.SocketNullException;
import com.ardublock.translator.block.exception.SubroutineNotDeclaredException;

public class LGIUltrasonicBlock extends TranslatorBlock
{
	public LGIUltrasonicBlock(Long blockId, Translator translator, String codePrefix,	String codeSuffix, String label)
	{
		super(blockId, translator, codePrefix, codeSuffix, label);
	}

	private final static String ultraSonicFunction = "\nint ultraschallSensor(int sensorPin){\n long duration, cm; \n\n //starte Trigger \n pinMode(sensorPin, OUTPUT); \n digitalWrite(sensorPin, LOW);  \n delayMicroseconds(2); \n digitalWrite(sensorPin, HIGH); \n delayMicroseconds(5); \n digitalWrite(sensorPin, LOW); \n\n //messe Echo-Laufzeit \n pinMode(sensorPin, INPUT); \n duration = pulseIn(sensorPin, HIGH); \n\n //berechne Abstand  \n cm =  duration / 29.1 / 2; \n return cm;\n }\n";
	
	public String toCode() throws SocketNullException, SubroutineNotDeclaredException
	{
		String SensorPin;

		TranslatorBlock translatorBlock = this.getRequiredTranslatorBlockAtSocket(0);
		SensorPin = translatorBlock.toCode().replaceAll("\\s*_.new\\b\\s*", "");

		
		translator.addSetupCommand("digitalWrite(" + SensorPin + ", LOW );\n");
		
		translator.addDefinitionCommand(ultraSonicFunction);
		
		String ret = "ultraschallSensor(" + SensorPin + ")";
		

		return codePrefix + ret + codeSuffix;
	}
}