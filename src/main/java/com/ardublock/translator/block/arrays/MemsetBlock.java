package com.ardublock.translator.block.arrays;


import java.util.ResourceBundle;

import com.ardublock.translator.Translator;
import com.ardublock.translator.block.TranslatorBlock;
import com.ardublock.translator.block.exception.BlockException;
import com.ardublock.translator.block.exception.SocketNullException;
import com.ardublock.translator.block.exception.SubroutineNotDeclaredException;
import com.ardublock.translator.block.numbers.NumberBlock;
import com.ardublock.translator.block.numbers.StringBlock;

public class MemsetBlock extends TranslatorBlock
{
	private static ResourceBundle uiMessageBundle = ResourceBundle.getBundle("com/ardublock/block/ardublock");
	
	public MemsetBlock(Long blockId, Translator translator, String codePrefix, String codeSuffix, String label)
	{
		super(blockId, translator, codePrefix, codeSuffix, label);
	}

	@Override
	public String toCode() throws SocketNullException, SubroutineNotDeclaredException
	{
		TranslatorBlock tb_destination = this.getRequiredTranslatorBlockAtSocket(0);
		TranslatorBlock tb_insert	   = this.getRequiredTranslatorBlockAtSocket(1);
		TranslatorBlock tb_length	   = this.getRequiredTranslatorBlockAtSocket(2);

		String destinationArray   = tb_destination.toCode();
		String insertChar         = tb_insert.toCode();
		String numOfReplacedChars = tb_length.toCode();


		if ((tb_destination instanceof StringBlock) || (tb_destination instanceof NumberBlock)) {
		      throw new BlockException(blockId, uiMessageBundle.getString("ardublock.error_msg. 	var_slot"));
		    }
		
		String ret = "memset("+destinationArray+", " + insertChar + ", "+numOfReplacedChars+");";

		return ret;
	}

}



