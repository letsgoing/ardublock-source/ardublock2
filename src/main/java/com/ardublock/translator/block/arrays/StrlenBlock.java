package com.ardublock.translator.block.arrays;


import java.util.ResourceBundle;

import com.ardublock.translator.Translator;
import com.ardublock.translator.block.TranslatorBlock;
import com.ardublock.translator.block.exception.BlockException;
import com.ardublock.translator.block.exception.SocketNullException;
import com.ardublock.translator.block.exception.SubroutineNotDeclaredException;
import com.ardublock.translator.block.numbers.NumberBlock;
import com.ardublock.translator.block.numbers.StringBlock;

public class StrlenBlock extends TranslatorBlock
{
	private static ResourceBundle uiMessageBundle = ResourceBundle.getBundle("com/ardublock/block/ardublock");
	
	public StrlenBlock(Long blockId, Translator translator, String codePrefix, String codeSuffix, String label)
	{
		super(blockId, translator, codePrefix, codeSuffix, label);
	}

	@Override
	public String toCode() throws SocketNullException, SubroutineNotDeclaredException
	{
		TranslatorBlock tb_name   = this.getRequiredTranslatorBlockAtSocket(0);

		String arrayName       = tb_name.toCode();


		if ((tb_name instanceof StringBlock) || (tb_name instanceof NumberBlock)) {
		      throw new BlockException(blockId, uiMessageBundle.getString("ardublock.error_msg.var_slot"));
		    }

		return "strlen("+arrayName+")";
	}

}



