package com.ardublock.translator.block.arrays;


import java.util.ResourceBundle;

import com.ardublock.translator.Translator;
import com.ardublock.translator.block.TranslatorBlock;
import com.ardublock.translator.block.exception.BlockException;
import com.ardublock.translator.block.exception.SocketNullException;
import com.ardublock.translator.block.exception.SubroutineNotDeclaredException;
import com.ardublock.translator.block.numbers.ConstantStringBlock;
import com.ardublock.translator.block.numbers.LocalVariableStringBlock;
import com.ardublock.translator.block.numbers.VariableStringBlock;

public class StrcpyBlock extends TranslatorBlock
{
	private static ResourceBundle uiMessageBundle = ResourceBundle.getBundle("com/ardublock/block/ardublock");
	
	public StrcpyBlock(Long blockId, Translator translator, String codePrefix, String codeSuffix, String label)
	{
		super(blockId, translator, codePrefix, codeSuffix, label);
	}

	@Override
	public String toCode() throws SocketNullException, SubroutineNotDeclaredException
	{
		TranslatorBlock tb_destination = this.getRequiredTranslatorBlockAtSocket(0);
		TranslatorBlock tb_source	   = this.getRequiredTranslatorBlockAtSocket(1);

		String destinationArray = tb_destination.toCode();
		String sourceArray      = tb_source.toCode();


		if (!(tb_destination instanceof VariableStringBlock) && !(tb_destination instanceof LocalVariableStringBlock)  && !(tb_destination instanceof ConstantStringBlock) 
			&& !(tb_source instanceof VariableStringBlock) && !(tb_source instanceof LocalVariableStringBlock)  && !(tb_source instanceof ConstantStringBlock)) {
		      throw new BlockException(blockId, uiMessageBundle.getString("ardublock.error_msg.string_var_slot"));
		    }

		String ret = "strcpy("+destinationArray+", " + sourceArray + ");";

		return ret;
	}

}



