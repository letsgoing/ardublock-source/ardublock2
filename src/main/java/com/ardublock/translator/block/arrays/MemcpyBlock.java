package com.ardublock.translator.block.arrays;


import java.util.ResourceBundle;

import com.ardublock.translator.Translator;
import com.ardublock.translator.block.TranslatorBlock;
import com.ardublock.translator.block.exception.BlockException;
import com.ardublock.translator.block.exception.SocketNullException;
import com.ardublock.translator.block.exception.SubroutineNotDeclaredException;
import com.ardublock.translator.block.numbers.NumberBlock;
import com.ardublock.translator.block.numbers.StringBlock;

public class MemcpyBlock extends TranslatorBlock
{
	private static ResourceBundle uiMessageBundle = ResourceBundle.getBundle("com/ardublock/block/ardublock");
	
	public MemcpyBlock(Long blockId, Translator translator, String codePrefix, String codeSuffix, String label)
	{
		super(blockId, translator, codePrefix, codeSuffix, label);
	}

	@Override
	public String toCode() throws SocketNullException, SubroutineNotDeclaredException
	{
		TranslatorBlock tb_destination = this.getRequiredTranslatorBlockAtSocket(0);
		TranslatorBlock tb_source	   = this.getRequiredTranslatorBlockAtSocket(1);
		TranslatorBlock tb_length	   = this.getRequiredTranslatorBlockAtSocket(2);

		String destinationArray = tb_destination.toCode();
		String sourceArray      = tb_source.toCode();
		String lengthOfData      = tb_length.toCode();


		if ((tb_destination instanceof StringBlock) || (tb_destination instanceof NumberBlock) || (tb_source instanceof StringBlock) || (tb_source instanceof NumberBlock) ) {
		      throw new BlockException(blockId, uiMessageBundle.getString("ardublock.error_msg.string_var_slot"));
		    }
		
		String ret = "memcpy("+destinationArray+", " + sourceArray + ", "+lengthOfData+");";

		return ret;
	}

}



