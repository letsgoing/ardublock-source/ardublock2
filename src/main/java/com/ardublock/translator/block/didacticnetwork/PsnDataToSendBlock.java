package com.ardublock.translator.block.didacticnetwork;

import com.ardublock.translator.Translator;
import com.ardublock.translator.block.TranslatorBlock;
import com.ardublock.translator.block.exception.SocketNullException;
import com.ardublock.translator.block.exception.SubroutineNotDeclaredException;

public class PsnDataToSendBlock extends TranslatorBlock
{
	public PsnDataToSendBlock(Long blockId, Translator translator, String codePrefix, String codeSuffix, String label)
	{
		super(blockId, translator, codePrefix, codeSuffix, label);
	}

	@Override
	public String toCode() throws SocketNullException, SubroutineNotDeclaredException
	{
		//Label autoText set in edu.mit.blocks.renderable.BlockLabel -> generateLabelText()
		String name = label.trim();
		if(name.contains(" ")) {
			name = name.substring(0, name.indexOf(" "));
		}		
		//TranslatorBlock tb_name = getRequiredTranslatorBlockAtSocket(0);
		//String name = tb_name.toCode().replaceAll("\\s*_.new\\b\\s*", "").replaceAll("\"", "");
		return name + ".isDataToSend();";
	}
}
