package com.ardublock.translator.block.didacticnetwork;

import com.ardublock.translator.Translator;
import com.ardublock.translator.block.TranslatorBlock;
import com.ardublock.translator.block.exception.SocketNullException;
import com.ardublock.translator.block.exception.SubroutineNotDeclaredException;

public class PsnClientBlock extends TranslatorBlock
{
	public PsnClientBlock(Long blockId, Translator translator, String codePrefix, String codeSuffix, String label)
	{
		super(blockId, translator, codePrefix, codeSuffix, label);
	}

	@Override
	public String toCode() throws SocketNullException, SubroutineNotDeclaredException
	{
		String serialName = "sSerial";
		//String pubSubName = "";
		String callbackName = "";
		String pubSubType = "DidacticPSNetClient";
		
		//Label autoText set in edu.mit.blocks.renderable.BlockLabel -> generateLabelText()
		String pubSubName = label.trim();
		if(pubSubName.contains(" ")) {
			pubSubName = pubSubName.substring(pubSubName.lastIndexOf(" ")+1, pubSubName.length());
		}
		
		//TranslatorBlock psnName = this.getRequiredTranslatorBlockAtSocket(0);//Name
		TranslatorBlock rxPin = this.getRequiredTranslatorBlockAtSocket(0);//Pin Rx
		TranslatorBlock txPin = this.getRequiredTranslatorBlockAtSocket(1);//Pin Tx
		TranslatorBlock cbName = this.getRequiredTranslatorBlockAtSocket(2);//Pin Tx
		
		//pubSubName = psnName.toCode().replaceAll("\\s*_.new\\b\\s*\"", "").replaceAll("\"", "");
		callbackName = cbName.toCode().replaceAll("\\s*_.new\\b\\s*\"", "").replaceAll("\"", "");
		
		translator.addHeaderFile("DidacticNet.h");
		translator.addHeaderFile("SoftwareSerial.h");
		
		translator.addDefinitionCommand("SoftwareSerial " + serialName+"(" + rxPin.toCode().replaceAll("\\s*_.new\\b\\s*", "") + ", "+ txPin.toCode().replaceAll("\\s*_.new\\b\\s*", "") +");\n");
		translator.addDefinitionCommand(pubSubType + " " +pubSubName+";\n");
		
		translator.addSetupCommand(serialName+".begin(2400);");
		translator.addSetupCommand(pubSubName + ".begin(" + serialName +", "+ callbackName +");\n");		
		return "";
	}
}
