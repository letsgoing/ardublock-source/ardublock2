package com.ardublock.translator.block.didacticnetwork;

import java.util.ResourceBundle;

import com.ardublock.translator.Translator;
import com.ardublock.translator.block.TranslatorBlock;
import com.ardublock.translator.block.exception.BlockException;
import com.ardublock.translator.block.exception.SocketNullException;
import com.ardublock.translator.block.exception.SubroutineNotDeclaredException;
import com.ardublock.translator.block.numbers.LocalVariableStringBlock;


public class PsnCallbackBlock extends TranslatorBlock
{
	private static ResourceBundle uiMessageBundle = ResourceBundle.getBundle("com/ardublock/block/ardublock");

	public PsnCallbackBlock(Long blockId, Translator translator,
			String codePrefix, String codeSuffix, String label) {
		super(blockId, translator, codePrefix, codeSuffix, label);
	}

	@Override
	public String toCode() throws SocketNullException, SubroutineNotDeclaredException
	{
		String newMarker = "_.new";
		String regex = "\\s*"+newMarker+"\\b\\s*";
		
		String callbackName = label.trim();
		String topicVar = "";
		//String topicLenVar = "";
		String dataVar = "";
		//String dataLenVar = "";
		String ret = "";
				
		TranslatorBlock tb_commentBlock = getRequiredTranslatorBlockAtSocket(0);
		ret += "//"+tb_commentBlock.toCode().replaceAll("\"", "")+"\n";
		
		TranslatorBlock tb_topicVar = getRequiredTranslatorBlockAtSocket(1);
		//TranslatorBlock tb_topicLenVar = getRequiredTranslatorBlockAtSocket(2);
		TranslatorBlock tb_dataVar = getRequiredTranslatorBlockAtSocket(2);//3);
		//TranslatorBlock tb_dataLenVar = getRequiredTranslatorBlockAtSocket(4);
		
		TranslatorBlock tb_code = getTranslatorBlockAtSocket(3);//5);
		
		topicVar = tb_topicVar.toCode();//.replaceAll("\\s*_.new\\b\\s*", "");
		//topicLenVar = tb_topicLenVar.toCode();//.replaceAll("\\s*_.new\\b\\s*", "");
		dataVar = tb_dataVar.toCode();//.replaceAll("\\s*_.new\\b\\s*", "");
		//dataLenVar = tb_dataLenVar.toCode();//.replaceAll("\\s*_.new\\b\\s*", "");
		
		//LOCAL VARs
		if ((tb_topicVar instanceof LocalVariableStringBlock) && topicVar.contains(newMarker)) {
			topicVar = topicVar.replaceAll(regex, "");
						translator.addNumberVariable(topicVar, topicVar);  //remove the "new" Tag after declaration
		}
		
		/*if ((tb_topicLenVar instanceof LocalVariableNumberBlock) && topicLenVar.contains(newMarker)) {
			topicLenVar = topicLenVar.replaceAll(regex, "");
						translator.addNumberVariable(topicLenVar, topicLenVar);  //remove the "new" Tag after declaration
		}*/
		
		if ((tb_dataVar instanceof LocalVariableStringBlock) && dataVar.contains(newMarker)) {
			dataVar = dataVar.replaceAll(regex, "");
						translator.addNumberVariable(dataVar, dataVar);  //remove the "new" Tag after declaration
		}
		
		/*if ((tb_dataLenVar instanceof LocalVariableNumberBlock) && dataLenVar.contains(newMarker)) {
			dataLenVar = dataLenVar.replaceAll(regex, "");
						translator.addNumberVariable(dataLenVar, dataLenVar);  //remove the "new" Tag after declaration
		}*/
		
		/*if (tb_topicVar instanceof LocalVariableStringBlock && tb_topicLenVar instanceof LocalVariableNumberBlock && tb_dataVar instanceof LocalVariableStringBlock && tb_dataLenVar instanceof LocalVariableNumberBlock) {
			translator.addDefinitionCommand("void " + callbackName + "(char*, int, char*, int);\n");
			ret += "void " + callbackName + "(char* "+ topicVar +", int "+ topicLenVar +", char* "+ dataVar +", int "+ dataLenVar +" ){\n";
			
			while (tb_code != null)
			{
				ret += tb_code.toCode();
				tb_code = tb_code.nextTranslatorBlock();
			}
			ret += "}\n\n";
		}*/
		if (tb_topicVar instanceof LocalVariableStringBlock && tb_dataVar instanceof LocalVariableStringBlock ) {
			translator.addDefinitionCommand("void " + callbackName + "(char*, int, char*, int);\n");
			ret += "void " + callbackName + "(char* "+ topicVar +", char* "+ dataVar+" ){\n";
			
			while (tb_code != null)
			{
				ret += tb_code.toCode();
				tb_code = tb_code.nextTranslatorBlock();
			}
			ret += "}\n\n";
		}
		else{
			throw new BlockException(blockId, uiMessageBundle.getString("ardublock.error_msg.local_var_slot"));
		}

		
		return ret;
	}
}

/*
 * 		if (tb_topicVar instanceof LocalVariableStringBlock && tb_topicLenVar instanceof LocalVariableNumberBlock && tb_dataVar instanceof LocalVariableStringBlock && tb_dataLenVar instanceof LocalVariableNumberBlock) {
			translator.addDefinitionCommand("void " + callbackName + "(char*, int, char*, int);\n");
			ret += "void " + callbackName + "(char* _topic, int _topicLength, char* _data , int _dataLength ){\n";
			ret += "char " + topicVar +"[_topicLength+1] = \"\";\n";
			ret += "int "+ topicLenVar+" = _topicLength;\n";
			ret += "char " + dataVar +"[_dataLength+1]= \"\";\n";
			ret += "int "+ dataLenVar+" = _dataLength;\n";
			ret += "\n";
			ret += "strcpy("+topicVar+",_topic);\n";
			ret += "strcpy("+dataVar+",_data);\n";
			ret += "\n";
			
			while (tb_code != null)
			{
				ret += tb_code.toCode();
				tb_code = tb_code.nextTranslatorBlock();
			}
			ret += "}\n\n";
		}
		else{
			throw new BlockException(blockId, uiMessageBundle.getString("ardublock.error_msg.local_var_slot"));
		}

		
		return ret;
	}
}
 * */
