package com.ardublock.translator.block.didacticnetwork;

import com.ardublock.translator.Translator;
import com.ardublock.translator.block.TranslatorBlock;
import com.ardublock.translator.block.exception.SocketNullException;
import com.ardublock.translator.block.exception.SubroutineNotDeclaredException;
import com.ardublock.translator.block.numbers.StringBlock;


public class PsnPublishOnChangeBoolBlock extends TranslatorBlock
{
	public PsnPublishOnChangeBoolBlock(Long blockId, Translator translator, String codePrefix, String codeSuffix, String label)
	{
		super(blockId, translator, codePrefix, codeSuffix, label);
	}

	@Override
	public String toCode() throws SocketNullException, SubroutineNotDeclaredException
	{
		
		//Label autoText set in edu.mit.blocks.renderable.BlockLabel -> generateLabelText()
		//TODO: add OnChange to autolabel
		String name = label.trim();
		if(name.contains(" ")) {
			name = name.substring(0, name.indexOf(" "));
		}
		//TranslatorBlock tb_name = getRequiredTranslatorBlockAtSocket(0);
		TranslatorBlock tb_topic = getRequiredTranslatorBlockAtSocket(0);
		TranslatorBlock tb_data = getRequiredTranslatorBlockAtSocket(1);
		
		//String name = tb_name.toCode().replaceAll("\\s*_.new\\b\\s*\"", "").replaceAll("\"", "");
		String topic = tb_topic.toCode().replaceAll("\\s*_.new\\b\\s*", "");
		String data = tb_data.toCode().replaceAll("\\s*_.new\\b\\s*", "");
		
		//TODO: test f check for VarBlock is needed
		if(!(tb_topic instanceof StringBlock)) {
			topic = topic.replaceAll("\"", "");
		}
		if(!(tb_data instanceof StringBlock)) {
			data = data.replaceAll("\"", "");
		}
		
		return name + ".publishOnChange("+topic+", "+data+");";
	}
}
