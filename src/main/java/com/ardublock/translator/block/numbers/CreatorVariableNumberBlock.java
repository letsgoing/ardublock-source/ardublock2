package com.ardublock.translator.block.numbers;

import java.util.ResourceBundle;

import com.ardublock.translator.Translator;
import com.ardublock.translator.block.TranslatorBlock;
import com.ardublock.translator.block.exception.BlockException;
import com.ardublock.translator.block.exception.SocketNullException;
import com.ardublock.translator.block.exception.SubroutineNotDeclaredException;

import tec.letsgoing.ardublock.simulator.simcode.SimCode;
import tec.letsgoing.ardublock.simulator.simcode.datatypes.SimTypeInt;
import tec.letsgoing.ardublock.simulator.simcode.datatypes.SimTypeString;
import tec.letsgoing.ardublock.simulator.simcode.vars.CodeIntSet;

public class CreatorVariableNumberBlock extends TranslatorBlock
{
	private static ResourceBundle uiMessageBundle = ResourceBundle.getBundle("com/ardublock/block/ardublock");

	public CreatorVariableNumberBlock(Long blockId, Translator translator, String codePrefix, String codeSuffix, String label){
		super(blockId, translator, codePrefix, codeSuffix, label);
	}

	@Override
	public String toCode() throws SocketNullException, SubroutineNotDeclaredException
	{
		String suffix = "";
		String newMarker = "_.new";
		String regex = "\\s*"+newMarker+"\\b\\s*";
		String dataType = "int";
		String stdInitVal= "0";
		
		TranslatorBlock tb_Name = this.getRequiredTranslatorBlockAtSocket(0);
		String variableName = tb_Name.toCode();
		TranslatorBlock tb_Value = this.getTranslatorBlockAtSocket(1);
		String value = tb_Value.toCode().replaceAll(regex, "");
		
		if (!(tb_Name instanceof VariableNumberBlock)){//  && !(tb_Name instanceof ConstantNumberBlock)) {
		    throw new BlockException(blockId, uiMessageBundle.getString("ardublock.error_msg.number_var_slot"));
		}
		
		else {
			variableName = variableName.replaceAll(regex, "");
			translator.addNumberVariable(variableName, variableName);  //remove the "new" Tag after declaration
			if(tb_Value instanceof NumberBlock){
				return dataType + " " + variableName + " = "+ value + suffix +";";
			}else{
				return dataType + " " + variableName + " = " + stdInitVal + ";";
			}
		}
	}
	
	/* TODO: ADD local Var handling to simulator
	 */
	/*
	public SimCode toSim() throws SocketNullException {
		TranslatorBlock translatorBlock = this.getRequiredTranslatorBlockAtSocket(0);
		SimTypeString variableName = new SimTypeString(translatorBlock.toString());
		if (translatorBlock instanceof VariableNumberBlock) {
			translator.addNumberSim(translatorBlock.toString(), translatorBlock.toString());
		}
		translatorBlock = this.getRequiredTranslatorBlockAtSocket(1);
		SimTypeInt value = new SimTypeInt(translatorBlock.toSim());
		
		return new CodeIntSet(variableName,value);
	}
	*/
}
