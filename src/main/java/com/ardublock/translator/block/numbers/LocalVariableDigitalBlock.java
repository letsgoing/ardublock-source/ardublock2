package com.ardublock.translator.block.numbers;

import com.ardublock.translator.Translator;
import com.ardublock.translator.block.TranslatorBlock;

import tec.letsgoing.ardublock.simulator.simcode.SimCode;
import tec.letsgoing.ardublock.simulator.simcode.datatypes.SimTypeString;
import tec.letsgoing.ardublock.simulator.simcode.vars.CodeBoolGet;

public class LocalVariableDigitalBlock extends TranslatorBlock
{
	public LocalVariableDigitalBlock(Long blockId, Translator translator, String codePrefix, String codeSuffix, String label)
	{
		super(blockId, translator, codePrefix, codeSuffix, label);
	}

	@Override
	public String toCode()
	{
		//Label autoText set in edu.mit.blocks.renderable.BlockLabel -> generateLabelText()
		String newMarker = "_.new";
		String internalVariableName = translator.getNumberVariable(translator.buildVariableName(label));
		String newInternalName = internalVariableName;
		
		if (internalVariableName == null)
		{
			internalVariableName = translator.buildVariableName(label);		
			newInternalName = internalVariableName + newMarker; //add the "new" Tag for varDeclaration
			translator.addNumberVariable(internalVariableName, (newInternalName));
		}
		return codePrefix + newInternalName + codeSuffix;
	}
	
	public String toString() {
		return label;
	}
	
	public SimCode toSim() {
		return new CodeBoolGet(new SimTypeString(label));
		
	}
}
