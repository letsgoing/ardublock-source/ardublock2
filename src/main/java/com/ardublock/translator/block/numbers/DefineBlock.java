package com.ardublock.translator.block.numbers;

import java.util.ResourceBundle;

import com.ardublock.translator.Translator;
import com.ardublock.translator.block.TranslatorBlock;
import com.ardublock.translator.block.exception.BlockException;
import com.ardublock.translator.block.exception.SocketNullException;
import com.ardublock.translator.block.exception.SubroutineNotDeclaredException;


public class DefineBlock extends TranslatorBlock
{
	private static ResourceBundle uiMessageBundle = ResourceBundle.getBundle("com/ardublock/block/ardublock");
	
	public DefineBlock(Long blockId, Translator translator, String codePrefix, String codeSuffix, String label) {
		super(blockId, translator, codePrefix, codeSuffix, label);
	}

	@Override
	public String toCode() throws SocketNullException, SubroutineNotDeclaredException
	{
		//Label autoText set in edu.mit.blocks.renderable.BlockLabel -> generateLabelText()
		//String suffix = "";
		String newMarker = "_.new";
		String regex = "\\s*"+newMarker+"\\b\\s*";
		
		TranslatorBlock tb_Name = this.getRequiredTranslatorBlockAtSocket(0);
		String variableName = tb_Name.toCode();
		TranslatorBlock tb_Value = this.getRequiredTranslatorBlockAtSocket(1);
		String value = tb_Value.toCode().replaceAll(regex, "");
		
		if (!(tb_Name instanceof ConstantDigitalBlock) && !(tb_Name instanceof ConstantNumberBlock)  && !(tb_Name instanceof ConstantCharBlock) && !(tb_Name instanceof ConstantStringBlock)) {
		    throw new BlockException(blockId, uiMessageBundle.getString("ardublock.error_msg.define_slot"));
		}
		
		if (variableName.contains(newMarker)){
			variableName = variableName.replaceAll(regex, ""); //remove the "new" Tag after declaration
			translator.addNumberVariable(variableName, variableName);
			translator.addDefinitionCommand("#define " + variableName + " " + value);
		}else {
			throw new BlockException(blockId, uiMessageBundle.getString("ardublock.error_msg.number_const_write"));
		}
		
		return "";
	}
	
	//TODO: add Sim
	/*
	public SimCode toSim() throws SocketNullException {
		TranslatorBlock translatorBlock = this.getRequiredTranslatorBlockAtSocket(0);
		SimTypeString variableName = new SimTypeString(translatorBlock.toString());
		if (translatorBlock instanceof VariableDigitalBlock) {
			translator.addBooleanSim(translatorBlock.toString(), translatorBlock.toString());
		}
		translatorBlock = this.getRequiredTranslatorBlockAtSocket(1);
		SimTypeBool value = new SimTypeBool(translatorBlock.toSim());
		return new CodeBoolSet(variableName,value);
	}*/

}
