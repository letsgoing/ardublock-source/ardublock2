package com.ardublock.translator.block.communication;

import java.util.ResourceBundle;

import com.ardublock.translator.Translator;
import com.ardublock.translator.block.TranslatorBlock;
import com.ardublock.translator.block.exception.BlockException;
import com.ardublock.translator.block.exception.SocketNullException;
import com.ardublock.translator.block.exception.SubroutineNotDeclaredException;
import com.ardublock.translator.block.numbers.ConstantStringBlock;
import com.ardublock.translator.block.numbers.LocalVariableNumberBlock;
import com.ardublock.translator.block.numbers.LocalVariableStringBlock;
import com.ardublock.translator.block.numbers.StringBlock;
import com.ardublock.translator.block.numbers.VariableNumberBlock;
import com.ardublock.translator.block.numbers.VariableStringBlock;

public class PlotMessageBlock extends TranslatorBlock
{
	private static ResourceBundle uiMessageBundle = ResourceBundle.getBundle("com/ardublock/block/ardublock");
	
	public PlotMessageBlock(Long blockId, Translator translator, String codePrefix, String codeSuffix, String label)
	{
		super(blockId, translator, codePrefix, codeSuffix, label);
	}

	@Override
	public String toCode() throws SocketNullException, SubroutineNotDeclaredException
	{	
		String VarMarker = "><"; //split marker for SerialPrintBlock
		String ret = "";
		
		TranslatorBlock tB_label = this.getTranslatorBlockAtSocket(0);
		TranslatorBlock tB_value = this.getRequiredTranslatorBlockAtSocket(1);
		
		if (!(tB_label instanceof StringBlock)) {
		    throw new BlockException(blockId, uiMessageBundle.getString("ardublock.error_msg.string_only_slot"));
		}
		
		if(tB_label != null){
			StringBuilder tb_label_buffer = new StringBuilder(tB_label.toCode().replaceAll("\\s*_.new\\b\\s*", ""));
			tb_label_buffer.replace(tb_label_buffer.lastIndexOf("\""), tb_label_buffer.lastIndexOf("\"") + 1, ":\"" );
			ret = tb_label_buffer.toString()+ VarMarker;
		}
		if(tB_value != null){
			ret += tB_value.toCode().replaceAll("\\s*_.new\\b\\s*", "");
		}
		
		return  ret;
	}
} 


