package com.ardublock.translator.block.communication;

import com.ardublock.translator.Translator;
import com.ardublock.translator.block.TranslatorBlock;
import com.ardublock.translator.block.exception.SocketNullException;
import com.ardublock.translator.block.exception.SubroutineNotDeclaredException;


public class SerialPlotBlock extends TranslatorBlock
{
	public SerialPlotBlock(Long blockId, Translator translator, String codePrefix, String codeSuffix, String label)
	{
		super(blockId, translator, codePrefix, codeSuffix, label);
	}

	@Override
	public String toCode() throws SocketNullException, SubroutineNotDeclaredException
	{	
		String VarMarker = "><"; //split marker used in GlueBlock
		String ret = "";
		
		if(!translator.containsSetupCommand("Serial.begin")){
			translator.addSetupCommand("Serial.begin(9600);");
		}
		
		TranslatorBlock translatorBlock = this.getRequiredTranslatorBlockAtSocket(0);
		TranslatorBlock tb_upperLimit = null;
		TranslatorBlock tb_lowerLimit = null;
		
		try {
			tb_upperLimit = this.getTranslatorBlockAtSocket(1);
			tb_lowerLimit = this.getTranslatorBlockAtSocket(2);
		} catch (Exception e) {}
		
		while (translatorBlock != null)
		{
			String[] buffer = translatorBlock.toCode().split(VarMarker);
			translatorBlock = translatorBlock.nextTranslatorBlock();
			
			if(translatorBlock == null && (tb_upperLimit == null || tb_lowerLimit == null)) {
				ret += "Serial.print(" + buffer[0] + ");\n";
				ret += "Serial.println(" + buffer[1] + ");\n";	
			}
			else {
				ret += "Serial.print(" + buffer[0] + ");\n";
				ret += "Serial.print(" + buffer[1] + ");\n";
				ret += "Serial.print(\" \");";
			}
		}
			
		if(tb_upperLimit != null && tb_lowerLimit != null) {
			ret += "Serial.print(\"Limit:\");\n";
			ret += "Serial.print(" + tb_upperLimit.toCode() + ");\n";
			ret += "Serial.print(\" \");";
			ret += "Serial.print(\"Limit:\");\n";
			ret += "Serial.println(" + tb_lowerLimit.toCode() + ");\n";
		}
		
		return ret;
	}
	
}
