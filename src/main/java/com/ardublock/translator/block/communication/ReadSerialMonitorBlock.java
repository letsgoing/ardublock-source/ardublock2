package com.ardublock.translator.block.communication;

import java.util.ResourceBundle;

import com.ardublock.translator.Translator;
import com.ardublock.translator.block.TranslatorBlock;
import com.ardublock.translator.block.exception.BlockException;
import com.ardublock.translator.block.exception.SocketNullException;
import com.ardublock.translator.block.exception.SubroutineNotDeclaredException;
import com.ardublock.translator.block.numbers.LocalVariableStringBlock;
import com.ardublock.translator.block.numbers.VariableStringBlock;

public class ReadSerialMonitorBlock extends TranslatorBlock
{
	private static ResourceBundle uiMessageBundle = ResourceBundle.getBundle("com/ardublock/block/ardublock");
	
	public ReadSerialMonitorBlock(Long blockId, Translator translator, String codePrefix, String codeSuffix, String label)
	{
		super(blockId, translator, codePrefix, codeSuffix, label);
	}

	@Override
	public String toCode() throws SocketNullException, SubroutineNotDeclaredException
	{	
		String readArray = null;
		
		translator.addHeaderFile("DidacticNet.h");
		translator.addDefinitionCommand("SerialReader sReader;");
		
		
		if(!translator.containsSetupCommand("Serial.begin")){
			translator.addSetupCommand("Serial.begin(2400);");
		}
		
		if(!translator.containsSetupCommand("sReader.begin")){
			translator.addSetupCommand("sReader.begin(Serial);");
		}
		
		TranslatorBlock tB0 = this.getRequiredTranslatorBlockAtSocket(0);
		
		if((tB0 instanceof VariableStringBlock) || (tB0 instanceof LocalVariableStringBlock)){
			readArray = tB0.toCode().replaceAll("\\s*_.new\\b\\s*", "");
		} else {
			throw new BlockException(blockId, uiMessageBundle.getString("ardublock.error_msg.number_var_slot"));
		}
		
		return "sReader.readSerialData("+readArray+", DN_ASCII_CR)";
	}
}
