package com.ardublock.translator.block.communication;

import com.ardublock.translator.Translator;
import com.ardublock.translator.block.TranslatorBlock;
import com.ardublock.translator.block.exception.BlockException;
import com.ardublock.translator.block.exception.SocketNullException;
import com.ardublock.translator.block.exception.SubroutineNotDeclaredException;

import tec.letsgoing.ardublock.simulator.simcode.SimCode;
import tec.letsgoing.ardublock.simulator.simcode.comm.CodeConnectString;

public class GlueBlock extends TranslatorBlock
{
	public GlueBlock(Long blockId, Translator translator, String codePrefix, String codeSuffix, String label)
	{
		super(blockId, translator, codePrefix, codeSuffix, label);
	}

	@Override
	public String toCode() throws SocketNullException, SubroutineNotDeclaredException
	{	
		String VarMarker = "><"; //split marker for SerialPrintBlock
		String ret = "";
		
		TranslatorBlock tB2 = this.getTranslatorBlockAtSocket(1);
		
		TranslatorBlock tB1 = this.getTranslatorBlockAtSocket(0);
		if(tB1 != null){
			ret = tB1.toCode().replaceAll("\\s*_.new\\b\\s*", "") + VarMarker;
		}
		if(tB2 != null){
			ret += tB2.toCode().replaceAll("\\s*_.new\\b\\s*", "");
		}
		
		return  ret + " ";
	}
	
	public SimCode toSim() throws BlockException, SocketNullException {
		TranslatorBlock tB2 = this.getTranslatorBlockAtSocket(1);
		TranslatorBlock tB1 = this.getTranslatorBlockAtSocket(0);
		SimCode tB1Sim = null;
		SimCode tB2Sim = null;
		
		if(tB1 != null) {
			tB1Sim =  tB1.toSim();
		}
		if(tB2 != null){
			tB2Sim =  tB2.toSim();
		}
		return new CodeConnectString(tB1Sim,tB2Sim);
	}
} 


