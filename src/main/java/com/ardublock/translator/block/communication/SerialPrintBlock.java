package com.ardublock.translator.block.communication;

import com.ardublock.translator.Translator;
import com.ardublock.translator.block.TranslatorBlock;
import com.ardublock.translator.block.exception.BlockException;
import com.ardublock.translator.block.exception.SocketNullException;
import com.ardublock.translator.block.exception.SubroutineNotDeclaredException;

import tec.letsgoing.ardublock.simulator.simcode.SimCode;
import tec.letsgoing.ardublock.simulator.simcode.comm.CodeSerialPrint;
import tec.letsgoing.ardublock.simulator.simcode.datatypes.SimTypeBool;
import tec.letsgoing.ardublock.simulator.simcode.datatypes.SimTypeString;

public class SerialPrintBlock extends TranslatorBlock
{
	public SerialPrintBlock(Long blockId, Translator translator, String codePrefix, String codeSuffix, String label)
	{
		super(blockId, translator, codePrefix, codeSuffix, label);
	}

	@Override
	public String toCode() throws SocketNullException, SubroutineNotDeclaredException
	{	
		String VarMarker = "><"; //split marker used in GlueBlock
		String ret = "";
		
		TranslatorBlock tB1 = this.getRequiredTranslatorBlockAtSocket(0); //Code
		TranslatorBlock tB2 = this.getRequiredTranslatorBlockAtSocket(1); //newLine?
		
		if(!translator.containsSetupCommand("Serial.begin")){
			translator.addSetupCommand("Serial.begin(9600);");
		}
		
		String stringInput = tB1.toCode();
		String[] stringParts = stringInput.split(VarMarker);
		String newLine = tB2.toCode().replaceAll("\\s*_.new\\b\\s*", "");
		
		for(int i = 0; i < stringParts.length; i += 1){
			if(stringParts[i].endsWith("\"")){
				stringParts[i] = stringParts[i].substring(0,stringParts[i].length() - 1) + " \"";// SPACE added at the end of every part
			}
			
			if((newLine.equals("true")||newLine.equals("HIGH")) && i == stringParts.length-1){ // Add \n if last SerialPrint & newline is true
			    ret += "Serial.println(" + stringParts[i] +  ");\n";	
			}else {
				ret += "Serial.print(" + stringParts[i] +  ");\n";
			}
			
		}
		return ret;
	}
	
	public SimCode toSim() throws BlockException, SocketNullException {
		TranslatorBlock translatorBlock = this.getRequiredTranslatorBlockAtSocket(0); //Code
		SimTypeString data=new SimTypeString(translatorBlock.toSim());
		translatorBlock = this.getRequiredTranslatorBlockAtSocket(1); //newLine?
		SimTypeBool newLine= new SimTypeBool(translatorBlock.toSim());
		
		return new CodeSerialPrint(data,newLine);
	}
	
}
