package com.ardublock.translator.block.cast;

import java.util.ResourceBundle;

import com.ardublock.translator.Translator;
import com.ardublock.translator.block.TranslatorBlock;
import com.ardublock.translator.block.exception.BlockException;
import com.ardublock.translator.block.exception.SocketNullException;
import com.ardublock.translator.block.exception.SubroutineNotDeclaredException;
import com.ardublock.translator.block.numbers.ConstantStringBlock;
import com.ardublock.translator.block.numbers.LocalVariableStringBlock;
import com.ardublock.translator.block.numbers.VariableStringBlock;

public class CastItoa extends TranslatorBlock
{
	private static ResourceBundle uiMessageBundle = ResourceBundle.getBundle("com/ardublock/block/ardublock");
	
	public CastItoa(Long blockId, Translator translator, String codePrefix, String codeSuffix, String label) {
		super(blockId, translator, codePrefix, codeSuffix, label);
	}

	@Override
	public String toCode() throws SocketNullException, SubroutineNotDeclaredException
	{
		TranslatorBlock tb_buffer = this.getRequiredTranslatorBlockAtSocket(0);
		TranslatorBlock tb_value = this.getRequiredTranslatorBlockAtSocket(1);
		
		String intValue = tb_value.toCode().replaceAll("\\s*_.new\\b\\s*", "");
		String charBuffer = tb_buffer.toCode().replaceAll("\\s*_.new\\b\\s*", "");
		
		if (!(tb_buffer instanceof VariableStringBlock) && !(tb_buffer instanceof LocalVariableStringBlock)  && !(tb_buffer instanceof ConstantStringBlock)) {
		    throw new BlockException(blockId, uiMessageBundle.getString("ardublock.error_msg.string_var_slot"));
		}
	
		return "itoa("+ intValue + ", " + charBuffer + ", 10);";
	}

}
