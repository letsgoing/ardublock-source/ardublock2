package com.ardublock.translator.block.logic;

import com.ardublock.translator.Translator;
import com.ardublock.translator.block.TranslatorBlock;
import com.ardublock.translator.block.exception.SocketNullException;
import com.ardublock.translator.block.exception.SubroutineNotDeclaredException;

import tec.letsgoing.ardublock.simulator.simcode.SimCode;
import tec.letsgoing.ardublock.simulator.simcode.datatypes.SimTypeBool;
import tec.letsgoing.ardublock.simulator.simcode.logic.CodeOR;

public class OrBlock extends TranslatorBlock
{
	public OrBlock(Long blockId, Translator translator, String codePrefix, String codeSuffix, String label)
	{
		super(blockId, translator, codePrefix, codeSuffix, label);
	}

	@Override
	public String toCode() throws SocketNullException, SubroutineNotDeclaredException
	{
		String ret = "( ";
		TranslatorBlock translatorBlock = this.getRequiredTranslatorBlockAtSocket(0);
		ret = ret + translatorBlock.toCode().replaceAll("\\s*_.new\\b\\s*", "");
		ret = ret + " || ";
		translatorBlock = this.getRequiredTranslatorBlockAtSocket(1);
		ret = ret + translatorBlock.toCode().replaceAll("\\s*_.new\\b\\s*", "");
		ret = ret + " )";
		return codePrefix + ret + codeSuffix;
	}
	
	public SimCode toSim() throws SocketNullException {
		//FIXME Check if ervy Block accepts Vars
		TranslatorBlock b1 = this.getRequiredTranslatorBlockAtSocket(0);
		TranslatorBlock b2 = this.getRequiredTranslatorBlockAtSocket(1);
		return new CodeOR(new SimTypeBool(b1.toSim()),new SimTypeBool(b2.toSim()));
	}

}
