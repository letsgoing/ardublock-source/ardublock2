package com.ardublock.translator.block.control;

import java.util.Vector;

import com.ardublock.translator.Translator;
import com.ardublock.translator.block.TranslatorBlock;
import com.ardublock.translator.block.exception.BlockException;
import com.ardublock.translator.block.exception.SocketNullException;
import com.ardublock.translator.block.exception.SubroutineNotDeclaredException;

import tec.letsgoing.ardublock.simulator.simcode.SimCode;
import tec.letsgoing.ardublock.simulator.simcode.control.CodeElse;
import tec.letsgoing.ardublock.simulator.simcode.datatypes.SimTypeIf;

public class ElseBlock extends TranslatorBlock
{
	public ElseBlock(Long blockId, Translator translator, String codePrefix, String codeSuffix, String label)
	{
		super(blockId, translator);
	}

	@Override
	public String toCode() throws SocketNullException, SubroutineNotDeclaredException
	{
		String ret = "else {\n";
		TranslatorBlock translatorBlock = getTranslatorBlockAtSocket(0);
		while (translatorBlock != null)
		{
			ret = ret + translatorBlock.toCode();
			translatorBlock = translatorBlock.nextTranslatorBlock();
		}
		ret = ret + "}\n";
		return ret;
	}
	
	public SimTypeIf toIfSim() throws BlockException, SocketNullException {
		
		
		Vector<SimCode> vector = new Vector<SimCode>();
		TranslatorBlock translatorBlock = getTranslatorBlockAtSocket(0);
		while (translatorBlock != null)
		{
			if (translatorBlock.toSim()!=null) {
				vector.add(translatorBlock.toSim());
				}
			translatorBlock = translatorBlock.nextTranslatorBlock();
		}
		
		
		return new CodeElse(vector); 
	}
	
	public SimCode toSim() {
		return null;
	}
}
