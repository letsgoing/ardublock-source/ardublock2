package com.ardublock.translator.block.control;

import java.util.Map;
import java.util.Vector;

import com.ardublock.translator.Translator;
import com.ardublock.translator.block.TranslatorBlock;
import com.ardublock.translator.block.exception.BlockException;
import com.ardublock.translator.block.exception.SocketNullException;
import com.ardublock.translator.block.exception.SubroutineNotDeclaredException;

import tec.letsgoing.ardublock.simulator.Simulator;
import tec.letsgoing.ardublock.simulator.simcode.SimCode;
import tec.letsgoing.ardublock.simulator.simcode.control.CodeWhile;
import tec.letsgoing.ardublock.simulator.simcode.datatypes.SimTypeBool;
import tec.letsgoing.ardublock.simulator.simcode.datatypes.SimTypeInt;
import tec.letsgoing.ardublock.simulator.simcode.datatypes.SimTypeString;
import tec.letsgoing.ardublock.simulator.simcode.functions.CodeExecuteFunction;
import tec.letsgoing.ardublock.simulator.simcode.functions.SimCodeFunction;
import tec.letsgoing.ardublock.simulator.simcode.vars.CodeBoolSet;
import tec.letsgoing.ardublock.simulator.simcode.vars.CodeIntSet;


public class LoopBlock extends TranslatorBlock
{
	public LoopBlock(Long blockId, Translator translator, String codePrefix, String codeSuffix, String label)
	{
		super(blockId, translator);
	}

	@Override
	public String toCode() throws SocketNullException, SubroutineNotDeclaredException
	{
		String ret;
		ret = "void loop() {\n";

		TranslatorBlock translatorBlock = getTranslatorBlockAtSocket(0);
		while (translatorBlock != null)
		{
			ret = ret + translatorBlock.toCode();
			translatorBlock = translatorBlock.nextTranslatorBlock();
		}
		
		ret = ret + "}\n\n";
		return ret;
	}
	
	public SimCode toSim() throws BlockException, SocketNullException {
		Simulator sim = Simulator.getInstance();
		Vector<SimCode> vec =new Vector<SimCode>();
		TranslatorBlock translatorBlock = getTranslatorBlockAtSocket(0);
		while (translatorBlock != null)
		{
			SimCode newSimCode = translatorBlock.toSim();
			translatorBlock = translatorBlock.nextTranslatorBlock();
			if (newSimCode != null) {
				vec.add(newSimCode);
			}
		}
		SimCodeFunction loop =new SimCodeFunction("loop",vec);
		Vector<SimCode> mainVec = new Vector<SimCode>();
		
		Map<String,String> numbers=translator.getNuberMap();
		Map<String,String> bools=translator.getBooleanMap();
		for (String var : numbers.keySet()) {
			mainVec.add(new CodeIntSet(new SimTypeString(var),new SimTypeInt(0)));
		}
		
		for (String var : bools.keySet()) {
			mainVec.add(new CodeBoolSet(new SimTypeString(var),new SimTypeBool(false)));
		}
		
		Vector<SimCode> loopVec = new Vector<SimCode>();
		loopVec.add(new CodeExecuteFunction("loop"));
		SimTypeBool boolTrue = new SimTypeBool(true);
		mainVec.add(new CodeWhile(boolTrue, loopVec));
		SimCodeFunction main = new SimCodeFunction("main", mainVec);

		
		sim.addFunctionsCode(loop);
		sim.addFunctionsCode(main);
		return null;
		
	}
}
