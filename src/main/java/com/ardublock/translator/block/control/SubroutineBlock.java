package com.ardublock.translator.block.control;

import java.util.Vector;

import com.ardublock.translator.Translator;
import com.ardublock.translator.block.TranslatorBlock;
import com.ardublock.translator.block.exception.BlockException;
import com.ardublock.translator.block.exception.SocketNullException;
import com.ardublock.translator.block.exception.SubroutineNotDeclaredException;

import tec.letsgoing.ardublock.simulator.Simulator;
import tec.letsgoing.ardublock.simulator.simcode.SimCode;
import tec.letsgoing.ardublock.simulator.simcode.functions.SimCodeFunction;

public class SubroutineBlock extends TranslatorBlock
{

	public SubroutineBlock(Long blockId, Translator translator,
			String codePrefix, String codeSuffix, String label) {
		super(blockId, translator, codePrefix, codeSuffix, label);
	}

	@Override
	public String toCode() throws SocketNullException, SubroutineNotDeclaredException
	{
		
		String subroutineName = label.trim();
		translator.addDefinitionCommand("void " + subroutineName + "();\n");
		String ret = "";
		
		TranslatorBlock tb_commentBlock = getRequiredTranslatorBlockAtSocket(0);
		ret += "//"+tb_commentBlock.toCode().replaceAll("\"", "")+"\n";
		
		ret += "void " + subroutineName + "()\n{\n";
		TranslatorBlock tb_comandsBlock = getTranslatorBlockAtSocket(1);
		while (tb_comandsBlock != null)
		{
			ret += tb_comandsBlock.toCode();
			tb_comandsBlock = tb_comandsBlock.nextTranslatorBlock();
		}
		ret += "}\n\n";
		return ret;
	}
	
	public SimCode toSim() throws BlockException, SocketNullException {
		Simulator sim=Simulator.getInstance();
		String funcName= label.trim();
		Vector<SimCode> vec =new Vector<SimCode>();
		TranslatorBlock translatorBlock = getTranslatorBlockAtSocket(1);
		while (translatorBlock != null)
		{
			SimCode newSimCode = translatorBlock.toSim();
			translatorBlock = translatorBlock.nextTranslatorBlock();
			if (newSimCode != null) {
				vec.add(newSimCode);
			}
			
			
		}
		SimCodeFunction func =new SimCodeFunction(funcName,vec);
		//System.out.println("Add new Function");
		sim.addFunctionsCode(func);
		return null;

	}
}
