package com.ardublock.translator.block.control;

import java.util.Vector;

import com.ardublock.translator.Translator;
import com.ardublock.translator.block.TranslatorBlock;
import com.ardublock.translator.block.exception.BlockException;
import com.ardublock.translator.block.exception.SocketNullException;
import com.ardublock.translator.block.exception.SubroutineNotDeclaredException;

import tec.letsgoing.ardublock.simulator.simcode.SimCode;
import tec.letsgoing.ardublock.simulator.simcode.control.CodeElseIf;
import tec.letsgoing.ardublock.simulator.simcode.datatypes.SimTypeBool;
import tec.letsgoing.ardublock.simulator.simcode.datatypes.SimTypeIf;

public class ElseIfBlock extends TranslatorBlock
{
	public ElseIfBlock(Long blockId, Translator translator, String codePrefix, String codeSuffix, String label)
	{
		super(blockId, translator);
	}

	@Override
	public String toCode() throws SocketNullException, SubroutineNotDeclaredException
	{
		String ret = "else if(";
		TranslatorBlock translatorBlock = this.getRequiredTranslatorBlockAtSocket(0);
		ret = ret + translatorBlock.toCode().replaceAll("\\s*_.new\\b\\s*", "");
		ret = ret + ") {\n\n";
		translatorBlock = getTranslatorBlockAtSocket(1);
		while (translatorBlock != null)
		{
			ret = ret + translatorBlock.toCode();
			translatorBlock = translatorBlock.nextTranslatorBlock();
		}
		ret = ret + "}\n";
		return ret;
	}
	
	@Override
	public SimCode toSim() throws BlockException, SocketNullException {
		return null;
	}
	
	public SimTypeIf toIfSim() throws BlockException, SocketNullException {
		
				SimTypeBool condition=new SimTypeBool(this.getRequiredTranslatorBlockAtSocket(0).toSim());
				Vector<SimCode> vector = new Vector<SimCode>();
				TranslatorBlock translatorBlock = getTranslatorBlockAtSocket(1);
				while (translatorBlock != null)
				{
					if (translatorBlock.toSim()!=null) {
						vector.add(translatorBlock.toSim());
						}
					translatorBlock = translatorBlock.nextTranslatorBlock();
				}
				SimTypeIf followCode=null;
				TranslatorBlock followBlock=this.nextTranslatorBlock();
				if (followBlock instanceof ElseIfBlock) {
					followCode=((ElseIfBlock)followBlock).toIfSim();
				} else if (followBlock instanceof ElseBlock) {
					followCode=((ElseBlock)followBlock).toIfSim();

				}

				
				return new CodeElseIf(followCode,condition,vector); 
	}
	
}
