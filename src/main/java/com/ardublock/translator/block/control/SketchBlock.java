package com.ardublock.translator.block.control;

import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.Vector;

import com.ardublock.translator.Translator;
import com.ardublock.translator.block.TranslatorBlock;
import com.ardublock.translator.block.arrays.SetterCharArrayBlock;
import com.ardublock.translator.block.arrays.SetterNumberArrayBlock;
import com.ardublock.translator.block.code.CommentBlock;
import com.ardublock.translator.block.code.CodeBlock;
import com.ardublock.translator.block.exception.BlockException;
import com.ardublock.translator.block.exception.SocketNullException;
import com.ardublock.translator.block.exception.SubroutineNotDeclaredException;

import tec.letsgoing.ardublock.simulator.Simulator;
import tec.letsgoing.ardublock.simulator.simcode.SimCode;
import tec.letsgoing.ardublock.simulator.simcode.control.CodeWhile;
import tec.letsgoing.ardublock.simulator.simcode.datatypes.SimTypeBool;
import tec.letsgoing.ardublock.simulator.simcode.datatypes.SimTypeInt;
import tec.letsgoing.ardublock.simulator.simcode.datatypes.SimTypeString;
import tec.letsgoing.ardublock.simulator.simcode.functions.CodeExecuteFunction;
import tec.letsgoing.ardublock.simulator.simcode.functions.SimCodeFunction;
import tec.letsgoing.ardublock.simulator.simcode.vars.CodeBoolSet;
import tec.letsgoing.ardublock.simulator.simcode.vars.CodeIntSet;


@SuppressWarnings("unused")
public class SketchBlock extends TranslatorBlock
{
	private List<String> setupCommand;
	private List<String> headerCommand;
	private static ResourceBundle uiMessageBundle = ResourceBundle.getBundle("com/ardublock/block/ardublock");
	
	public SketchBlock(Long blockId, Translator translator, String codePrefix, String codeSuffix, String label)
	{
		super(blockId, translator);
		setupCommand = new LinkedList<String>();
		headerCommand = new LinkedList<String>();
	}
	
	

	@Override
	public String toCode() throws SocketNullException, SubroutineNotDeclaredException
	{
	    String ret="";
	    
	    TranslatorBlock headerBlocks = getTranslatorBlockAtSocket(0);
		while (headerBlocks != null)
		{
			ret = headerBlocks.toCode();
			headerBlocks = headerBlocks.nextTranslatorBlock();
			
			//TODO: IF suitable Block (e.g. create Variable) THEN add returnValue to definitionCommand
			/*if(!ret.contentEquals("")){ //remove empty commands //TODO: select suitable Blocks  && (headerBlocks instanceof CodeLoopBlock || headerBlocks instanceof CodeCommentBlock || headerBlocks instanceof SetterNumberArrayBlock || headerBlocks instanceof SetterCharArrayBlock)
				this.headerCommand.add(ret);
			}else{
				throw new BlockException(blockId, uiMessageBundle.getString("ardublock.error_msg.wrong_block_header"));
			}*/
		}
	    
		TranslatorBlock setupBlocks = getTranslatorBlockAtSocket(1);
		while (setupBlocks != null)
		{
			ret = setupBlocks.toCode();
			setupBlocks = setupBlocks.nextTranslatorBlock();
			if(!ret.contentEquals("")){ //remove empty commands e.g. definition commands
				this.setupCommand.add(ret);
			}
		}
		
		translator.registerBodyTranslateFinishCallback(this);

		ret="";
		ret = "void loop() {\n";
		TranslatorBlock loopBlocks = getTranslatorBlockAtSocket(2);
		while (loopBlocks != null)
		{
			ret = ret + loopBlocks.toCode();
			loopBlocks = loopBlocks.nextTranslatorBlock();
		}
	
		ret = ret + "}\n\n";
		return ret;
	}
	
	@Override
	public void onTranslateBodyFinished()
	{
		for (String command : headerCommand)
		{
			translator.addDefinitionCommand(command);
		}
		
		for (String command : setupCommand)
		{
			translator.addSetupCommand(command);
		}
	}
	
	public SimCode toSim() throws BlockException, SocketNullException {
		Simulator sim = Simulator.getInstance();
		
		Vector<SimCode> mainVec =new Vector<SimCode>();
		Vector<SimCode> setup =new Vector<SimCode>();
		Vector<SimCode> loop =new Vector<SimCode>();
		
		
		
		//Setup
		TranslatorBlock translatorBlock = getTranslatorBlockAtSocket(1);
		while (translatorBlock != null)
		{
			SimCode newSimCode = translatorBlock.toSim();
			translatorBlock = translatorBlock.nextTranslatorBlock();
			if (newSimCode != null) {
				setup.add(newSimCode);
			}		
		}
		//Loop
		translatorBlock = getTranslatorBlockAtSocket(2);
		while (translatorBlock != null)
		{
			SimCode newSimCode = translatorBlock.toSim();
			translatorBlock = translatorBlock.nextTranslatorBlock();
			if (newSimCode != null) {
				loop.add(newSimCode);
			}		
		}
		SimCodeFunction setupFunc =new SimCodeFunction("setup",setup);
		SimCodeFunction loopFunc =new SimCodeFunction("loop",loop);
		
		//MainPart
				Map<String,String> numbers=translator.getNuberMap();
				Map<String,String> bools=translator.getBooleanMap();
				for (String var : numbers.keySet()) {
					mainVec.add(new CodeIntSet(new SimTypeString(var),new SimTypeInt(0)));
				}
				
				for (String var : bools.keySet()) {
					mainVec.add(new CodeBoolSet(new SimTypeString(var),new SimTypeBool(false)));
				}
				
				translatorBlock = getTranslatorBlockAtSocket(0);
				while (translatorBlock != null)
				{
					SimCode newSimCode = translatorBlock.toSim();
					translatorBlock = translatorBlock.nextTranslatorBlock();
					if (newSimCode != null) {
						mainVec.add(newSimCode);
					}		
				}
		
		
		SimTypeBool boolTrue = new SimTypeBool(true);
		mainVec.add(new CodeExecuteFunction("setup"));
		
		Vector<SimCode> whileVec = new Vector<SimCode>();
		whileVec.add(new CodeExecuteFunction("loop"));
		
		mainVec.add(new CodeWhile(boolTrue, whileVec));
		SimCodeFunction main = new SimCodeFunction("main", mainVec);

		//System.out.println("Add new Functions");
		
		sim.addFunctionsCode(setupFunc);
		sim.addFunctionsCode(loopFunc);
		sim.addFunctionsCode(main);
		return null;
		
		
	}
	
}
