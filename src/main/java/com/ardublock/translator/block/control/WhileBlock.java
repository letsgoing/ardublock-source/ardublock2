package com.ardublock.translator.block.control;

import java.util.Vector;

import com.ardublock.translator.Translator;
import com.ardublock.translator.block.TranslatorBlock;
import com.ardublock.translator.block.exception.BlockException;
import com.ardublock.translator.block.exception.SocketNullException;
import com.ardublock.translator.block.exception.SubroutineNotDeclaredException;

import tec.letsgoing.ardublock.simulator.simcode.SimCode;
import tec.letsgoing.ardublock.simulator.simcode.control.CodeWhile;
import tec.letsgoing.ardublock.simulator.simcode.datatypes.SimTypeBool;

public class WhileBlock extends TranslatorBlock
{

	public WhileBlock(Long blockId, Translator translator, String codePrefix, String codeSuffix, String label)
	{
		super(blockId, translator);
	}
	
	@Override
	public String toCode() throws SocketNullException, SubroutineNotDeclaredException
	{
		String ret = "while ( ";
		TranslatorBlock translatorBlock = this.getRequiredTranslatorBlockAtSocket(0);
		ret = ret + translatorBlock.toCode().replaceAll("\\s*_.new\\b\\s*", "");
		ret = ret + " ) {\n";
		translatorBlock = getTranslatorBlockAtSocket(1);
		while (translatorBlock != null)
		{
			ret = ret +translatorBlock.toCode();
			translatorBlock = translatorBlock.nextTranslatorBlock();
		}
		ret = ret + "}\n";
		return ret;
	}
	
	public SimCode toSim() throws BlockException, SocketNullException {
		TranslatorBlock translatorBlock = this.getRequiredTranslatorBlockAtSocket(0);
		SimTypeBool condition=new SimTypeBool(translatorBlock.toSim());
		translatorBlock = getTranslatorBlockAtSocket(1);
		Vector<SimCode> vector= new Vector<SimCode>();
		while (translatorBlock != null)
		{
			SimCode block=translatorBlock.toSim();
			if (block!=null) {
				vector.add(block);
			}
			translatorBlock = translatorBlock.nextTranslatorBlock();
		}
		return new CodeWhile(condition,vector);
	}

}
