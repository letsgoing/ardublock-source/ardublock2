package com.ardublock.translator.block.operators;

import java.util.ResourceBundle;

import com.ardublock.translator.Translator;
import com.ardublock.translator.block.TranslatorBlock;
import com.ardublock.translator.block.exception.BlockException;
import com.ardublock.translator.block.exception.SocketNullException;
import com.ardublock.translator.block.exception.SubroutineNotDeclaredException;

public class PidBlock extends TranslatorBlock
{
	private static ResourceBundle uiMessageBundle = ResourceBundle.getBundle("com/ardublock/block/ardublock");
	
	public PidBlock(Long blockId, Translator translator, String codePrefix, String codeSuffix, String label)
	{
		super(blockId, translator, codePrefix, codeSuffix, label);
	}

	@Override
	public String toCode() throws SocketNullException, SubroutineNotDeclaredException
	{
		TranslatorBlock tb_input 	= this.getRequiredTranslatorBlockAtSocket(0);
		TranslatorBlock tb_setpoint = this.getRequiredTranslatorBlockAtSocket(1);
		TranslatorBlock tb_interval = this.getRequiredTranslatorBlockAtSocket(2);
		TranslatorBlock tb_kp 		= this.getRequiredTranslatorBlockAtSocket(3);
		TranslatorBlock tb_ki 		= this.getTranslatorBlockAtSocket(4);
		TranslatorBlock tb_kd 		= this.getTranslatorBlockAtSocket(5);
		TranslatorBlock tb_limitL 	= null;
		TranslatorBlock tb_limitH 	= null;
		
		try {
			tb_limitL 	= this.getTranslatorBlockAtSocket(6);
			tb_limitH 	= this.getTranslatorBlockAtSocket(7);
			
		} catch (Exception e) {}
		
		boolean integrative = tb_ki != null;
		boolean derivative  = tb_kd != null;
		boolean limited     = (tb_limitL != null) && (tb_limitH != null);
		
		//TODO: TEST CONTROLLER FUNCTIONALITY!!!!!!!
		
		//FUNCTION CODE ASSEMBLY
		String functionCode = "double computePID(double input, double setpoint, long interval, double kp";
		
		if(integrative) {
			functionCode +=", double ki";
		}
		if(derivative) {
			functionCode +=", double kd";
		}
		if(limited) {
			functionCode +=", double limitLow, double limitHigh";
		}
		
		functionCode += "){";
		
		if(derivative) {
			functionCode +="  static double  lastError = 0;\n";
		}
		if(integrative) {
			functionCode +="static double cumError  = 0; \n";	
		}
					
		functionCode+="  static double  out       = 0;\n"
					+ "  static long lastTime  = 0L;\n\n"
					+ "  double intervalSecond = interval / 1000.0;\n"
					+ "  long currentTime     = millis();\t//get current time\n\n"
					+ "  if (currentTime - lastTime >= interval) {\n"
					+ "    long elapsedTime = (double)(currentTime - lastTime);  //compute time elapsed from previous computation\n"
					+ "    \n" 
					+ "    long error       = setpoint - input;                  // get error\n" 
					+ "    out = kp * error;                                     //P output\n\n";
		if(integrative) {
			functionCode +="    cumError  = cumError + (error * intervalSecond);    // compute integral\n";
			if(limited) {
				functionCode += "    cumError    = constrain(cumError, limitLow * 100.0, limitHigh * 100.0);            //limit integral against windup\n";
			}
			functionCode +=  "    out = out + (ki * cumError);                            //I output\n\n";
		}
		if(derivative) {
			functionCode +="    long rateError   = (error - lastError)  / intervalSecond; // compute derivative\n" 
						+  "    out = out + kd * rateError;                           //D output\n\n";
		}
		if(limited) {
			functionCode +="    out = constrain(out, limitLow, limitHigh);            //limit output\n\n";
		}
		if(derivative) {
			functionCode += "    lastError = error;         //remember current error\n";
		}
		functionCode += "    lastTime = currentTime;    //remember current time\n" 
					+   "  }\n" 
					+   "  return out;                  //return the PID output\n" 
					+	"}";
		
		translator.addDefinitionCommand(functionCode);
		
		//RETURN (BLOCK) CODE ASSEMBLY
		String ret = "computePID(";
		ret += tb_input.toCode().replaceAll("\\s*_.new\\b\\s*", "")+", "+tb_setpoint.toCode().replaceAll("\\s*_.new\\b\\s*", "") + ", " + tb_interval.toCode().replaceAll("\\s*_.new\\b\\s*", "") + ", " + tb_kp.toCode().replaceAll("\\s*_.new\\b\\s*", "");
		
		if(integrative) {
			ret +=", " + tb_ki.toCode().replaceAll("\\s*_.new\\b\\s*", "");
		}
		if(derivative) {
			ret +=", " + tb_kd.toCode().replaceAll("\\s*_.new\\b\\s*", "");
		}
		if(limited) {
			String limitL = tb_limitL.toCode().replaceAll("\\s*_.new\\b\\s*", "");
			String limitH = tb_limitH.toCode().replaceAll("\\s*_.new\\b\\s*", "");
			
			if(Integer.parseInt(limitL) < Integer.parseInt(limitH)) {
				ret +=", " + limitL + ", " + limitH;
			}else {
				throw new BlockException(blockId, uiMessageBundle.getString("ardublock.error_msg.pid_limit_error"));
			}
			
			
		}
		ret += ")";
		
		return codePrefix + ret + codeSuffix;
	}
	
}
