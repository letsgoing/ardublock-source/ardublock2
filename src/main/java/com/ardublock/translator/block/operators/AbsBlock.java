package com.ardublock.translator.block.operators;

import com.ardublock.translator.Translator;
import com.ardublock.translator.block.TranslatorBlock;
import com.ardublock.translator.block.exception.SocketNullException;
import com.ardublock.translator.block.exception.SubroutineNotDeclaredException;

import tec.letsgoing.ardublock.simulator.simcode.SimCode;
import tec.letsgoing.ardublock.simulator.simcode.datatypes.SimTypeInt;
import tec.letsgoing.ardublock.simulator.simcode.math.CodeAbs;

public class AbsBlock extends TranslatorBlock
	{

		public AbsBlock(Long blockId, Translator translator, String codePrefix, String codeSuffix, String label)
		{
			super(blockId, translator, codePrefix, codeSuffix, label);
		}

		@Override
		public String toCode() throws SocketNullException, SubroutineNotDeclaredException
		{
			String ret = "abs( ";
			TranslatorBlock translatorBlock = this.getRequiredTranslatorBlockAtSocket(0);
			ret = ret + translatorBlock.toCode().replaceAll("\\s*_.new\\b\\s*", "");
			ret = ret + " )";
			return codePrefix + ret + codeSuffix;
		}
		
		public SimCode toSim() throws SocketNullException {
			TranslatorBlock b1 = this.getRequiredTranslatorBlockAtSocket(0);
			return new CodeAbs(new SimTypeInt(b1.toSim()));
		}
		
	}
