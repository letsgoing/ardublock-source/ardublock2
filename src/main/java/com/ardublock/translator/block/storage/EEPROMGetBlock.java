package com.ardublock.translator.block.storage;

import java.util.ResourceBundle;

//import com.ardublock.core.Context;
import com.ardublock.translator.Translator;
import com.ardublock.translator.block.TranslatorBlock;
import com.ardublock.translator.block.exception.BlockException;
import com.ardublock.translator.block.exception.SocketNullException;
import com.ardublock.translator.block.exception.SubroutineNotDeclaredException;
import com.ardublock.translator.block.numbers.LocalVariableNumberBlock;
import com.ardublock.translator.block.numbers.VariableNumberBlock;

public class EEPROMGetBlock extends TranslatorBlock
{
	
	private static ResourceBundle uiMessageBundle = ResourceBundle.getBundle("com/ardublock/block/ardublock");
	
	public EEPROMGetBlock(Long blockId, Translator translator, String codePrefix, String codeSuffix, String label)
	{
		super(blockId, translator, codePrefix, codeSuffix, label);
	}

	@Override
	public String toCode() throws SocketNullException, SubroutineNotDeclaredException
	{
		translator.addHeaderFile("EEPROM.h");

		TranslatorBlock tb = this.getRequiredTranslatorBlockAtSocket(0);
		TranslatorBlock tb2 = this.getRequiredTranslatorBlockAtSocket(1);
		if (!(tb2 instanceof VariableNumberBlock) && !(tb2 instanceof LocalVariableNumberBlock)) {
		    throw new BlockException(blockId, uiMessageBundle.getString("ardublock.error_msg.number_var_slot"));
		}
		String ret = "EEPROM.get(" +tb.toCode().replaceAll("\\s*_.new\\b\\s*", "") + ", " + tb2.toCode().replaceAll("\\s*_.new\\b\\s*", "") + ");\n";
		
	return codePrefix + ret + codeSuffix;
	}
}
