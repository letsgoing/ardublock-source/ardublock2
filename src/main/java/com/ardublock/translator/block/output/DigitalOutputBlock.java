package com.ardublock.translator.block.output;

import com.ardublock.translator.Translator;
import com.ardublock.translator.block.TranslatorBlock;
import com.ardublock.translator.block.exception.BlockException;
import com.ardublock.translator.block.exception.SocketNullException;
import com.ardublock.translator.block.exception.SubroutineNotDeclaredException;
import com.ardublock.translator.block.numbers.NumberBlock;
import com.ardublock.translator.block.numbers.VariableNumberBlock;

import tec.letsgoing.ardublock.simulator.simcode.SimCode;
import tec.letsgoing.ardublock.simulator.simcode.datatypes.SimTypeBool;
import tec.letsgoing.ardublock.simulator.simcode.datatypes.SimTypeInt;
import tec.letsgoing.ardublock.simulator.simcode.io.CodeDigitalWrite;

public class DigitalOutputBlock extends TranslatorBlock
{
	public DigitalOutputBlock(Long blockId, Translator translator, String codePrefix, String codeSuffix, String label)
	{
		super(blockId, translator, codePrefix, codeSuffix, label);
	}

	@Override
	public String toCode() throws SocketNullException, SubroutineNotDeclaredException
	{
		
		TranslatorBlock translatorBlock = this.getRequiredTranslatorBlockAtSocket(0);
		String number = translatorBlock.toCode().replaceAll("\\s*_.new\\b\\s*", "");
		
		//ToDo: 
		if(translatorBlock instanceof NumberBlock || translatorBlock instanceof VariableNumberBlock){
			//translator.addOutputPin(number.trim());
			translator.addSetupCommand("pinMode("+number.trim()+", OUTPUT);");
		}
		
		String ret = "digitalWrite( ";
		ret = ret + number;
		ret = ret + " , ";
		translatorBlock = this.getRequiredTranslatorBlockAtSocket(1);
		ret = ret + translatorBlock.toCode().replaceAll("\\s*_.new\\b\\s*", "");
		ret = ret + " );\n";
		
		return ret;
	}
	
	public SimCode toSim() throws BlockException, SocketNullException {
		TranslatorBlock translatorBlock = this.getRequiredTranslatorBlockAtSocket(0);
		SimTypeInt pin=new SimTypeInt(translatorBlock.toSim());
		translatorBlock = this.getRequiredTranslatorBlockAtSocket(1);
		SimTypeBool lvl=new SimTypeBool(translatorBlock.toSim());
		return new CodeDigitalWrite(pin,lvl);//new CodeDigitalWrite(); 
	}

}
