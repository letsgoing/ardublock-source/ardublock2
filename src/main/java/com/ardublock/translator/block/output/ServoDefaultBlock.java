
package com.ardublock.translator.block.output;
import java.util.ResourceBundle;

import com.ardublock.translator.Translator;
import com.ardublock.translator.block.TranslatorBlock;
import com.ardublock.translator.block.exception.BlockException;
import com.ardublock.translator.block.exception.SocketNullException;
import com.ardublock.translator.block.exception.SubroutineNotDeclaredException;
import com.ardublock.translator.block.numbers.ConstantNumberBlock;
import com.ardublock.translator.block.numbers.LocalVariableNumberBlock;
import com.ardublock.translator.block.numbers.NumberBlock;
import com.ardublock.translator.block.numbers.VariableNumberBlock;

public class ServoDefaultBlock extends TranslatorBlock {
	
	private static ResourceBundle uiMessageBundle = ResourceBundle.getBundle("com/ardublock/block/ardublock");

	public ServoDefaultBlock(Long blockId, Translator translator, String codePrefix, String codeSuffix, String label)
	{
		super(blockId, translator, codePrefix, codeSuffix, label);
	}

	@Override
	public String toCode() throws SocketNullException, SubroutineNotDeclaredException
	{
		TranslatorBlock tb = this.getRequiredTranslatorBlockAtSocket(0);
		
		if (!( tb instanceof NumberBlock ) && !(tb instanceof VariableNumberBlock) && !(tb  instanceof LocalVariableNumberBlock) && !(tb instanceof ConstantNumberBlock) )
		{
			throw new BlockException(this.blockId, "the Pin# of Servo must contain a number");
		}

		String servoSpecs = "";

		String pinNumber = tb.toCode().replaceAll("\\s*_.new\\b\\s*", "");
		
		//****** Bit long w but easy to see what's happening. Any other invalid pins? *********
		/*if ( ! ("2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20 21 22 23 24 25 26 27 28 29 30 31 32/"
				+ " 33 34 35 36 37 38 39 40 41 42 43 44 45 46 47 48 49 50 51 52 53").contains(pinNumber.trim()) )
		{
			throw new BlockException(blockId, uiMessageBundle.getString("ardublock.error_msg.Digital_pin_slot"));
		}*/
		

		String servoName = translator.getServo("servo_"+pinNumber);
		if(servoName == null) {
			throw new BlockException(blockId, uiMessageBundle.getString("ardublock.error_msg.servo.none"));
		}
		//String servoName = "servo_pin_" + pinNumber;

		tb = this.getRequiredTranslatorBlockAtSocket(1);

		String ret = servoName + ".write( " + tb.toCode().replaceAll("\\s*_.new\\b\\s*", "") + " );\n";
		translator.addHeaderFile("Servo.h");
		translator.addDefinitionCommand("Servo " + servoName + ";");
		translator.addSetupCommand(servoName + ".attach(" + pinNumber + servoSpecs + ");");
		return ret;
	}

}
