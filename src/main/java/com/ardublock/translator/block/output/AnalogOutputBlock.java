package com.ardublock.translator.block.output;

import com.ardublock.translator.Translator;
import com.ardublock.translator.block.TranslatorBlock;
import com.ardublock.translator.block.exception.BlockException;
import com.ardublock.translator.block.exception.SocketNullException;
import com.ardublock.translator.block.exception.SubroutineNotDeclaredException;

import tec.letsgoing.ardublock.simulator.simcode.SimCode;
import tec.letsgoing.ardublock.simulator.simcode.datatypes.SimTypeInt;
import tec.letsgoing.ardublock.simulator.simcode.io.CodeAnalogWrite;

public class AnalogOutputBlock extends TranslatorBlock
{
	public AnalogOutputBlock(Long blockId, Translator translator, String codePrefix, String codeSuffix, String label)
	{
		super(blockId, translator, codePrefix, codeSuffix, label);
	}

	@Override
	public String toCode() throws SocketNullException, SubroutineNotDeclaredException
	{
		TranslatorBlock translatorBlock = this.getRequiredTranslatorBlockAtSocket(0);
		String portNum = translatorBlock.toCode().replaceAll("\\s*_.new\\b\\s*", "");
		
		translatorBlock = this.getRequiredTranslatorBlockAtSocket(1);
		String value = translatorBlock.toCode().replaceAll("\\s*_.new\\b\\s*", "");
		
		/* NOTE: AnalogWrite never needs the pin to be set as an OUTPUT */
		String ret = "analogWrite(" + portNum + " , " + value + ");\n";
		return ret;
	}
	public SimCode toSim() throws BlockException, SocketNullException {
		TranslatorBlock translatorBlock = this.getRequiredTranslatorBlockAtSocket(0);
		SimTypeInt pin=new SimTypeInt(translatorBlock.toSim());
		translatorBlock = this.getRequiredTranslatorBlockAtSocket(1);
		SimTypeInt value= new SimTypeInt(translatorBlock.toSim());
		return new CodeAnalogWrite(pin,value);
	}

}


