package com.ardublock.translator.block.output;

import com.ardublock.translator.Translator;
import com.ardublock.translator.block.TranslatorBlock;
import com.ardublock.translator.block.exception.SocketNullException;
import com.ardublock.translator.block.exception.SubroutineNotDeclaredException;

public class NeopixelTheaterChaseBlock  extends TranslatorBlock {

	public NeopixelTheaterChaseBlock (Long blockId, Translator translator, String codePrefix, String codeSuffix, String label)
	{
		super(blockId, translator, codePrefix, codeSuffix, label);
	}
	
	//@Override
		public String toCode() throws SocketNullException, SubroutineNotDeclaredException
		{
			String pin;
			String red, green, blue;
			String wait, times;
			String theaterFunction;

			TranslatorBlock tb_pin = this.getRequiredTranslatorBlockAtSocket(0);
			TranslatorBlock tb_red = this.getRequiredTranslatorBlockAtSocket(1);
			TranslatorBlock tb_green = this.getRequiredTranslatorBlockAtSocket(2);
			TranslatorBlock tb_blue = this.getRequiredTranslatorBlockAtSocket(3);
			TranslatorBlock tb_wait = this.getRequiredTranslatorBlockAtSocket(4);
			TranslatorBlock tb_times = this.getRequiredTranslatorBlockAtSocket(5);
			
			pin = tb_pin.toCode().replaceAll("\\s*_.new\\b\\s*", "");
			red = tb_red.toCode().replaceAll("\\s*_.new\\b\\s*", "");
			green = tb_green.toCode().replaceAll("\\s*_.new\\b\\s*", "");
			blue = tb_blue.toCode().replaceAll("\\s*_.new\\b\\s*", "");
			wait = tb_wait.toCode().replaceAll("\\s*_.new\\b\\s*", "");
			times = tb_times.toCode().replaceAll("\\s*_.new\\b\\s*", "");
			
			String ret = "theaterChase("+red+", "+green+", "+blue+", "+wait+");\n";
			theaterFunction = "void theaterChase(uint8_t red, uint8_t green, uint8_t blue, int wait) {\nuint32_t color = strip_pin"+pin+".Color(  red,  green,   blue);\nfor(int a=0; a<"+times+"; a++) { \nfor(int b=0; b<3; b++) {\nstrip_pin"+pin+".clear();\nfor(int c=b; c<strip_pin"+pin+".numPixels(); c += 3) {\nstrip_pin"+pin+".setPixelColor(c, color);\n}\nstrip_pin"+pin+".show();\ndelay(wait);\n}\n}\n}\n";
			translator.addDefinitionCommand(theaterFunction);
			
			return codePrefix + ret + codeSuffix;
				
		}
}
