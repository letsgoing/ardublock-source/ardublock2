package com.ardublock.translator.block.output;

import com.ardublock.translator.Translator;
import com.ardublock.translator.block.TranslatorBlock;
import com.ardublock.translator.block.exception.SocketNullException;
import com.ardublock.translator.block.exception.SubroutineNotDeclaredException;

public class NeopixelAinToColorBlock  extends TranslatorBlock {

	public NeopixelAinToColorBlock (Long blockId, Translator translator, String codePrefix, String codeSuffix, String label)
	{
		super(blockId, translator, codePrefix, codeSuffix, label);
	}
	
	private final static String colorFunction = "int ainToColor(int value, int color) {\n"
	+ "  int rgbValue = map(constrain(value, 0, 1023), 0, 1023, 0, 255);\n"
	+ "\n"
	+ "  int colorValue1 = 0;\n"
	+ "  int colorValue2 = 0;\n"
	+ "  int colorValue3 = 0;\n"
	+ "\n"
	+ "\n"
	+ "  if (value <= 341) { // Bereich von 0 bis 341: R -> G\n"
	+ "    colorValue1 = 255 - rgbValue * 3;\n"
	+ "    colorValue2 = rgbValue * 3;\n"
	+ "    colorValue3 = 0;\n"
	+ "  } else if (value <= 682) { // Bereich von 342 bis 682: G -> B\n"
	+ "    rgbValue -= 85;\n"
	+ "    colorValue1 = 0;\n"
	+ "    colorValue2 = 255 - rgbValue * 3;\n"
	+ "    colorValue3 = rgbValue * 3;\n"
	+ "  } else { // Bereich von 683 bis 1023: B -> R\n"
	+ "    rgbValue -= 170;\n"
	+ "    colorValue1 = rgbValue * 3;\n"
	+ "    colorValue2 = 0;\n"
	+ "    colorValue3 = 255 - rgbValue * 3;\n"
	+ "  }\n"
	+ "\n"
	+ "  switch (color) {\n"
	+ "    case 1:\n"
	+ "      return colorValue1;\n"
	+ "    case 2:\n"
	+ "      return colorValue2;\n"
	+ "      break;\n"
	+ "\n"
	+ "    case 3:\n"
	+ "      return colorValue3;\n"
	+ "      break;\n"
	+ "\n"
	+ "   return -1;\n"
	+ "  }\n"
	+ "}";
	
	//@Override
		public String toCode() throws SocketNullException, SubroutineNotDeclaredException
		{
			String analogValue ;
			String color;
			
			translator.addDefinitionCommand(colorFunction);

			TranslatorBlock tb_ain = this.getRequiredTranslatorBlockAtSocket(0);
			TranslatorBlock tb_color = this.getRequiredTranslatorBlockAtSocket(1);
			
			analogValue = tb_ain.toCode().replaceAll("\\s*_.new\\b\\s*", "");
			color = tb_color.toCode().replaceAll("\\s*_.new\\b\\s*", "");
			
			String ret = "ainToColor("+analogValue+", "+color+")";
			
			return codePrefix + ret + codeSuffix;				
		}
}
