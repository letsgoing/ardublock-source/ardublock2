package com.ardublock.translator.block.output;

import com.ardublock.translator.Translator;
import com.ardublock.translator.block.TranslatorBlock;
import com.ardublock.translator.block.exception.SocketNullException;
import com.ardublock.translator.block.exception.SubroutineNotDeclaredException;

public class NeopixelRainbowBlock  extends TranslatorBlock {

	public NeopixelRainbowBlock (Long blockId, Translator translator, String codePrefix, String codeSuffix, String label)
	{
		super(blockId, translator, codePrefix, codeSuffix, label);
	}
	
	//@Override
		public String toCode() throws SocketNullException, SubroutineNotDeclaredException
		{
			String pin ;
			String wait;
			String rainbowFunction;

			TranslatorBlock tb_pin = this.getRequiredTranslatorBlockAtSocket(0);
			TranslatorBlock tb_wait = this.getRequiredTranslatorBlockAtSocket(1);
			
			pin = tb_pin.toCode().replaceAll("\\s*_.new\\b\\s*", "");
			wait = tb_wait.toCode().replaceAll("\\s*_.new\\b\\s*", "");
			String ret = "rainbow("+wait+");\n";
			
			rainbowFunction = "void rainbow(int wait) {\nfor(long firstPixelHue = 0; firstPixelHue < 65536; firstPixelHue += 256) {\nfor(int i=0; i<strip_pin"+pin+".numPixels(); i++) {\nint pixelHue = firstPixelHue + (i * 65536L / strip_pin"+pin+".numPixels());\nstrip_pin"+pin+".setPixelColor(i, strip_pin"+pin+".gamma32(strip_pin"+pin+".ColorHSV(pixelHue)));\n}\nstrip_pin"+pin+".show();\ndelay(wait);\n}\n}";
			translator.addDefinitionCommand(rainbowFunction);
			
			return codePrefix + ret + codeSuffix;
				
		}
}
