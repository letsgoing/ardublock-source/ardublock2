package com.ardublock.translator.block.output;

import com.ardublock.translator.Translator;
import com.ardublock.translator.block.TranslatorBlock;
import com.ardublock.translator.block.exception.SocketNullException;
import com.ardublock.translator.block.exception.SubroutineNotDeclaredException;

public class NeopixelWipeBlock  extends TranslatorBlock {

	public NeopixelWipeBlock (Long blockId, Translator translator, String codePrefix, String codeSuffix, String label)
	{
		super(blockId, translator, codePrefix, codeSuffix, label);
	}
	
	//@Override
		public String toCode() throws SocketNullException, SubroutineNotDeclaredException
		{
			String pin;
			String red, green, blue;
			String wait;
			String wipeFunction;

			TranslatorBlock tb_pin = this.getRequiredTranslatorBlockAtSocket(0);
			TranslatorBlock tb_red = this.getRequiredTranslatorBlockAtSocket(1);
			TranslatorBlock tb_green = this.getRequiredTranslatorBlockAtSocket(2);
			TranslatorBlock tb_blue = this.getRequiredTranslatorBlockAtSocket(3);
			TranslatorBlock tb_wait = this.getRequiredTranslatorBlockAtSocket(4);
			
			pin = tb_pin.toCode().replaceAll("\\s*_.new\\b\\s*", "");
			red = tb_red.toCode().replaceAll("\\s*_.new\\b\\s*", "");
			green = tb_green.toCode().replaceAll("\\s*_.new\\b\\s*", "");
			blue = tb_blue.toCode().replaceAll("\\s*_.new\\b\\s*", "");
			wait = tb_wait.toCode().replaceAll("\\s*_.new\\b\\s*", "");
			
			String ret = "colorWipe("+red+", "+green+", "+blue+", "+wait+");\n";
			
			wipeFunction = "void colorWipe(uint8_t red, uint8_t green, uint8_t blue, int wait) {\nuint32_t color = strip_pin"+pin+".Color(  red,  green,   blue);\nfor(int i=0; i<strip_pin"+pin+".numPixels(); i++) {\nstrip_pin"+pin+".setPixelColor(i, color);\nstrip_pin"+pin+".show();\ndelay(wait);\n}\n}\n";
			translator.addDefinitionCommand(wipeFunction);
			
			return codePrefix + ret + codeSuffix;
				
		}
}
