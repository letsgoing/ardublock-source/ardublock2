
package com.ardublock.translator.block.output;

import java.util.ResourceBundle;

import com.ardublock.translator.Translator;
import com.ardublock.translator.block.TranslatorBlock;
import com.ardublock.translator.block.exception.BlockException;
import com.ardublock.translator.block.exception.SocketNullException;
import com.ardublock.translator.block.exception.SubroutineNotDeclaredException;
import com.ardublock.translator.block.numbers.ConstantNumberBlock;
import com.ardublock.translator.block.numbers.LocalVariableNumberBlock;
import com.ardublock.translator.block.numbers.NumberBlock;
import com.ardublock.translator.block.numbers.VariableNumberBlock;

public class ServoDefaultDetachBlock extends TranslatorBlock {
	
	private static ResourceBundle uiMessageBundle = ResourceBundle.getBundle("com/ardublock/block/ardublock");

	public ServoDefaultDetachBlock(Long blockId, Translator translator, String codePrefix, String codeSuffix, String label)
	{
		super(blockId, translator, codePrefix, codeSuffix, label);
	}

	@Override
	public String toCode() throws SocketNullException, SubroutineNotDeclaredException
	{
		TranslatorBlock tb = this.getRequiredTranslatorBlockAtSocket(0);

		//String servoSpecs = "";

		if (!( tb instanceof NumberBlock ) && !(tb instanceof VariableNumberBlock) && !(tb  instanceof LocalVariableNumberBlock) && !(tb instanceof ConstantNumberBlock) )
		{
			throw new BlockException(this.blockId, "the Pin# of Servo must contain a number");
		}
		
		String pinNumber = tb.toCode().replaceAll("\\s*_.new\\b\\s*", "");
		
		String servoName = translator.getServo("servo_"+pinNumber);
		if(servoName == null) {
			throw new BlockException(blockId, uiMessageBundle.getString("ardublock.error_msg.servo.none"));
		}
		//String servoName = "servo_pin_" + pinNumber;

	//	tb = this.getRequiredTranslatorBlockAtSocket(1);

		String ret = servoName + ".detach();\n";
		translator.addHeaderFile("Servo.h");
		translator.addDefinitionCommand("Servo " + servoName + ";");
		//translator.addSetupCommand(servoName + ".detach(" + pinNumber + servoSpecs + ");");
		return ret;
	}

}
