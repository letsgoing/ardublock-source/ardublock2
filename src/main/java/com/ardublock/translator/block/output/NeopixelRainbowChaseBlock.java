package com.ardublock.translator.block.output;

import com.ardublock.translator.Translator;
import com.ardublock.translator.block.TranslatorBlock;
import com.ardublock.translator.block.exception.SocketNullException;
import com.ardublock.translator.block.exception.SubroutineNotDeclaredException;

public class NeopixelRainbowChaseBlock  extends TranslatorBlock {

	public NeopixelRainbowChaseBlock (Long blockId, Translator translator, String codePrefix, String codeSuffix, String label)
	{
		super(blockId, translator, codePrefix, codeSuffix, label);
	}
	
	//@Override
		public String toCode() throws SocketNullException, SubroutineNotDeclaredException
		{
			String pin ;
			String wait;
			String times;
			String rainbowFunction;

			TranslatorBlock tb_pin = this.getRequiredTranslatorBlockAtSocket(0);
			TranslatorBlock tb_wait = this.getRequiredTranslatorBlockAtSocket(1);
			TranslatorBlock tb_times = this.getRequiredTranslatorBlockAtSocket(2);
			
			pin   = tb_pin.toCode().replaceAll("\\s*_.new\\b\\s*", "");
			wait  = tb_wait.toCode().replaceAll("\\s*_.new\\b\\s*", "");
			times = tb_times.toCode().replaceAll("\\s*_.new\\b\\s*", "");
			
			String ret = "theaterChaseRainbow("+wait+", "+ times +");\n";
			
			rainbowFunction = "void theaterChaseRainbow(int wait, int times) {\nint firstPixelHue = 0;\nfor(int a=0; a<times; a++) {\nfor(int b=0; b<3; b++) {\nstrip_pin"+pin+".clear();\nfor(int c=b; c<strip_pin"+pin+".numPixels(); c += 3) {\nint hue = firstPixelHue + c * 65536L / strip_pin"+pin+".numPixels();\nuint32_t color = strip_pin"+pin+".gamma32(strip_pin"+pin+".ColorHSV(hue));\nstrip_pin"+pin+".setPixelColor(c, color);\n}\nstrip_pin"+pin+".show();\ndelay(wait);\nfirstPixelHue += 65536 / 90;\n}\n}\n}";
			translator.addDefinitionCommand(rainbowFunction);
			
			return codePrefix + ret + codeSuffix;
		}
}
