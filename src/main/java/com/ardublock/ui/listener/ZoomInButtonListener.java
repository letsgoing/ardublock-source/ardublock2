package com.ardublock.ui.listener;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import com.ardublock.core.Context;

import edu.mit.blocks.workspace.PageChangeEventManager;
import edu.mit.blocks.workspace.Workspace;

public class ZoomInButtonListener implements ActionListener
{
	private Workspace workspace;
	
	/**workspace zoom settings 
     * match with edu.mit.blocks.codeblockutil.CHoverScrollPane
     */
    private static final double WORKSPACE_MAX_ZOOM = 3.0;
    private static final double WORKSPACE_ZOOM_STEPSIZE = 0.1;
	
	public ZoomInButtonListener(Workspace workspace)
	{
		Context.getContext();
		
		this.workspace = workspace;
	}
	
	public void actionPerformed(ActionEvent e) {
		
		double zoomLevel = workspace.getCurrentWorkspaceZoom();
		
		if(zoomLevel < WORKSPACE_MAX_ZOOM){
			zoomLevel += WORKSPACE_ZOOM_STEPSIZE;
			workspace.setWorkspaceZoom(zoomLevel);
			PageChangeEventManager.notifyListeners();
		}
	}

}
