package com.ardublock.ui.listener;

import java.awt.Desktop;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.ResourceBundle;

import com.ardublock.ui.OpenblocksFrame;


public class SimulatorHelpButtonListener implements ActionListener
{
	public SimulatorHelpButtonListener(OpenblocksFrame obFrame)
	{
		ResourceBundle.getBundle("com/ardublock/block/ardublock");
		
	}
	
	public void actionPerformed(ActionEvent e) {
	
		//System.out.println("Help is pressed");
	/*	ImageIcon icon = new ImageIcon(openBlocksFrame.getToolkit().getImage(GUI.class.getResource("/com/ardublock/block/SimHelp.png")));
        JOptionPane.showMessageDialog(
                null,
                uiMessageBundle.getString("ardublock.ui.simulatorHelp.text"),
                uiMessageBundle.getString("ardublock.ui.simulatorHelp.title"), JOptionPane.INFORMATION_MESSAGE,
                icon);
      */  
		/*
		 * JOptionPane.showMessageDialog( null, new JLabel("Hello world", icon,
		 * JLabel.LEFT), "Hello", JOptionPane.INFORMATION_MESSAGE);
		 */
        
		File cssFile   = null;
		File helpFile  = null;
		File imageFile = null;
		File logoFile  = null;
    	
		//TODO: add css
		String cssResource   ="/com/ardublock/reference/_seitenformatierung.css";
    	String helpResource  ="/com/ardublock/reference/simHelp.html";
    	String logoResource  ="/com/ardublock/reference/_Logo_LGI_page.png";
    	String imageResource ="/com/ardublock/block/SimHelp.png";
    	String tempPath = "";
	
		//get current .jar path for temp files
    	try {
			tempPath = new File(URLDecoder.decode(getClass().getProtectionDomain().getCodeSource().getLocation().getFile(), "UTF-8")).getParentFile().getPath();
		} catch (UnsupportedEncodingException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
    	//copy css File to tmp files
        try {
            InputStream input = getClass().getResourceAsStream(cssResource);
        	if(input != null) {
	            cssFile = new File(tempPath +"/"+ "_seitenformatierung.css");
	            OutputStream out = new FileOutputStream(cssFile);
	            int read;
	            byte[] bytes = new byte[1024];
	    	        while ((read = input.read(bytes)) != -1) {
	   	            out.write(bytes, 0, read);
	   	        }
	   	        out.close();
	   	        cssFile.deleteOnExit();
        	} else {
        		//TODO: use resources
        		System.out.println("Sorry, css stylesheet was not found in reference.");
        		//TODO: open 404-page
        		//!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        	}
   	    } catch (IOException ex) {
   	        ex.printStackTrace();
   	    }
    	
    	//copy help File to tmp files
        try {
            InputStream input = getClass().getResourceAsStream(helpResource);
        	if(input != null) {
	            helpFile = new File(tempPath +"/"+ "simHelp.html");
	            OutputStream out = new FileOutputStream(helpFile);
	            int read;
	            byte[] bytes = new byte[1024];
	    	        while ((read = input.read(bytes)) != -1) {
	   	            out.write(bytes, 0, read);
	   	        }
	   	        out.close();
	   	        helpFile.deleteOnExit();
        	} else {
        		//TODO: use resources
        		System.out.println("Sorry, help file was not found.");
        		//TODO: open 404-page
        		//!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        	}
   	    } catch (IOException ex) {
   	        ex.printStackTrace();
   	    }
        
      //copy logo to tmp files
   	    try {
   	        InputStream input = getClass().getResourceAsStream(logoResource);
	   	     if(input != null) {
	   	        logoFile = new File(tempPath +"/_Logo_LGI_page.png");
	   	        OutputStream out = new FileOutputStream(logoFile);
	   	        int read;
	   	        byte[] bytes = new byte[10000];
	    	        while ((read = input.read(bytes)) != -1) {
	   	            out.write(bytes, 0, read);
	   	        }
	   	        out.close();
	   	     logoFile.deleteOnExit();
	   	     } else {
	        		//TODO: use resources
	        		System.out.println("Sorry, logo image was not found.");
	        	}
   	    } catch (IOException ex) {
   	        ex.printStackTrace();
   	    }
   	    
   	//copy block-image to tmp files
   	    try {
   	        InputStream input = getClass().getResourceAsStream(imageResource);
	   	     if(input != null) {
	   	        imageFile = new File(tempPath +"/simHelp.png");
	   	        OutputStream out = new FileOutputStream(imageFile);
	   	        int read;
	   	        byte[] bytes = new byte[10000];
	    	        while ((read = input.read(bytes)) != -1) {
	   	            out.write(bytes, 0, read);
	   	        }
	   	        out.close();
	   	        imageFile.deleteOnExit();
	   	     } else {
	        		//TODO: use resources
	   	    	System.out.println("Sorry, image for help was not found in reference.");
	        	}
   	    } catch (IOException ex) {
   	        ex.printStackTrace();
   	    }
        
      //open file in standard browser
    	if (helpFile != null && !helpFile.exists() && cssFile != null && !cssFile.exists() && imageFile != null && !imageFile.exists() && !logoFile.exists() ) {
    	    throw new RuntimeException("Error: Resource not found!");
    	}else {    	
	    	try {
				Desktop.getDesktop().browse(helpFile.toURI());
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				//e.printStackTrace();
			} catch (NullPointerException e2) {
				// TODO Auto-generated catch block
				//e.printStackTrace();
			}
    	}
		
	}
}
