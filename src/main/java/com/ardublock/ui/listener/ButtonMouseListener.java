package com.ardublock.ui.listener;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.ImageIcon;
import javax.swing.JButton;


public class ButtonMouseListener extends MouseAdapter {
	
	ImageIcon iconPressed;
	ImageIcon iconHover;
	ImageIcon iconButton;
	ImageIcon iconSelected;
	
	boolean selected = false;
	
	public ButtonMouseListener(ImageIcon iconButton, ImageIcon iconHover, ImageIcon iconPressed, ImageIcon iconSelected) {
		this.iconButton = iconButton;
		this.iconHover = iconHover;
		this.iconPressed = iconPressed;
		this.iconSelected = iconSelected;
	}
	
	public ButtonMouseListener(ImageIcon iconButton, ImageIcon iconHover, ImageIcon iconPressed) {
		this(iconButton, iconHover, iconPressed, iconButton);
	}
	
	@Override
	public void mouseEntered(MouseEvent e) {
		JButton button = (JButton)e.getSource();
		button.setIcon(iconHover);
	}

	@Override
	public void mouseExited(MouseEvent e) {
		JButton button = (JButton)e.getSource();
		if(selected) {
			button.setIcon(iconSelected);
		}else {
			button.setIcon(iconButton);
		}		
	}
	
	@Override
	public void mousePressed(MouseEvent e) {
		JButton button = (JButton)e.getSource();
		button.setIcon(iconPressed);
		super.mousePressed(e);
	}
	
	public void setSelected(boolean selected) {
		this.selected = selected;
	}

}



