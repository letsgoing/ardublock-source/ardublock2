package com.ardublock.ui.listener;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import com.ardublock.ui.OpenblocksFrame;


public class SaveImageButtonListener implements ActionListener
{
	private OpenblocksFrame parentFrame;
	
	public SaveImageButtonListener(OpenblocksFrame frame)
	{
		parentFrame = frame;
	}
	
	public void actionPerformed(ActionEvent e) {
		parentFrame.saveProgramAsImage();
	}

}
