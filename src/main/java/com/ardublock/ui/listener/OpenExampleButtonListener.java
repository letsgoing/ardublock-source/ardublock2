package com.ardublock.ui.listener;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import com.ardublock.ui.OpenblocksFrame;

public class OpenExampleButtonListener implements ActionListener
{

	private String exampleName = "";
	private OpenblocksFrame frame;
	
	public OpenExampleButtonListener(String exampleName, OpenblocksFrame frame)
	{
		this.exampleName = exampleName;
		this.frame = frame;
	}
	
	public void actionPerformed(ActionEvent e) {		
		frame.openArduBlockExample(exampleName);
	}
	


}
