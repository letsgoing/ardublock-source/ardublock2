package com.ardublock.ui;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Desktop;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Graphics2D;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.image.BufferedImage;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.net.URL;
import java.net.URLDecoder;
import java.util.ResourceBundle;

import javax.imageio.ImageIO;
import javax.swing.AbstractButton;
import javax.swing.Box;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JCheckBoxMenuItem;
import javax.swing.JComponent;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.KeyStroke;
import javax.swing.ToolTipManager;
import javax.swing.filechooser.FileFilter;
import javax.swing.filechooser.FileNameExtensionFilter;

import com.ardublock.core.Context;
import com.ardublock.ui.listener.ArdublockWorkspaceListener;
import com.ardublock.ui.listener.ButtonMouseListener;
import com.ardublock.ui.listener.GenerateCodeButtonListener;
import com.ardublock.ui.listener.HelpButtonListener;
import com.ardublock.ui.listener.CopyButtonListener;
import com.ardublock.ui.listener.NewButtonListener;
import com.ardublock.ui.listener.OpenButtonListener;
import com.ardublock.ui.listener.OpenExampleButtonListener;
import com.ardublock.ui.listener.OpenblocksFrameListener;
import com.ardublock.ui.listener.PasteButtonListener;
import com.ardublock.ui.listener.SaveAsButtonListener;
import com.ardublock.ui.listener.SaveButtonListener;
import com.ardublock.ui.listener.SaveDefaultButtonListener;
import com.ardublock.ui.listener.SaveImageButtonListener;
import com.ardublock.ui.listener.SimulateCodeButtonListener;
import com.ardublock.ui.listener.SimulatorHelpButtonListener;
import com.ardublock.ui.listener.ZoomInButtonListener;
import com.ardublock.ui.listener.ZoomOutButtonListener;

import edu.mit.blocks.controller.WorkspaceController;
import edu.mit.blocks.workspace.SearchBar;
import edu.mit.blocks.workspace.SearchableContainer;
import edu.mit.blocks.workspace.Workspace;

import processing.app.PreferencesData;

import tec.letsgoing.ardublock.simulator.view.GUI;


public class OpenblocksFrame extends JFrame
{
	/**
	 * 
	 */
	private static final long serialVersionUID     = 2841155965906223806L;
	private static final int WINDOW_SIZE_WIDTH     = 1200;
	private static final int WINDOW_SIZE_HEIGHT    = 800;
	private static final int TOOLTIP_DISMISS_DELAY = 10000;
	
	private Context context;
	private JFileChooser fileChooser;
	private FileFilter ffilter;
	private boolean workspaceModeState = false;
	private String appPrefix = "";
	//private ResourceBundle uiMessageBundle;
	private static ResourceBundle uiMessageBundle = ResourceBundle.getBundle("com/ardublock/block/ardublock");
	
	private JButton modeBasicButton = null;
	private JButton modeProButton   = null;
	private JButton modeSimButton   = null;
	
	private SearchBar searchBar = null;
	
	private String blockMenuMode = "default";
	
	public void addListener(OpenblocksFrameListener ofl)
	{
		context.registerOpenblocksFrameListener(ofl);
	}
	
	public String makeFrameTitle()
	{	
		String title = Context.APP_NAME + " " + appPrefix +" "+context.getSaveFileName();
		if (context.isWorkspaceChanged())
		{
			title = title + " *";
		}
		return title;
	}

	public OpenblocksFrame()
	{
		//UIManager.getLookAndFeelDefaults().put("Panel.background", new Color(40, 74, 102));
		context = Context.getContext();
		this.setTitle(makeFrameTitle());
		this.setSize(new Dimension(WINDOW_SIZE_WIDTH, WINDOW_SIZE_HEIGHT));
		this.setLayout(new BorderLayout());
		//put the frame to the center of screen
		this.setLocationRelativeTo(null);
		//this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		fileChooser = new JFileChooser();
		ffilter = new FileNameExtensionFilter(uiMessageBundle.getString("ardublock.file.suffix"), "abp");
		fileChooser.setFileFilter(ffilter);
		fileChooser.addChoosableFileFilter(ffilter);
		
		appPrefix = uiMessageBundle.getString("ardublock.ui.appprefix.standard");
        setTitle(makeFrameTitle());

		initOpenBlocks();
		
	}
	
	private void initOpenBlocks()
	{		
		final Context context = Context.getContext();
		
		/*
		WorkspaceController workspaceController = context.getWorkspaceController();
		JComponent workspaceComponent = workspaceController.getWorkspacePanel();
		*/
		
		final Workspace workspace = context.getWorkspace();
		
		
		//override close operation
		this.addWindowListener(new WindowAdapter() {
			  public void windowClosing(WindowEvent we) {
				  PreferencesData.set("ardublock.workspace.mode", blockMenuMode);
				  PreferencesData.setInteger("ardublock.workspace.zoom", (int)(workspace.getCurrentWorkspaceZoom()*10));
				  if(context.isWorkspaceChanged() && askUserSaveFileOnExit()){
					  doSaveArduBlockFile();
				  }
			  }
			});
		
		// WTF I can't add workspacelistener by workspace controller
		workspace.addWorkspaceListener(new ArdublockWorkspaceListener(this));
		
		//display ToolTips for 10 seconds
		ToolTipManager.sharedInstance().setDismissDelay(TOOLTIP_DISMISS_DELAY);	
		
		searchBar = new SearchBar(uiMessageBundle.getString("ardublock.ui.search"), uiMessageBundle.getString("ardublock.ui.search.tooltip"), workspace);
	    addSearchableBlocks(workspace);
	    
	    blockMenuMode = PreferencesData.get("ardublock.workspace.mode");
	    
	    String zoomString = PreferencesData.get("ardublock.workspace.zoom");
	    if(zoomString != null && zoomString.matches("[0-9.]+")) {
	    	workspace.setWorkspaceZoom(((double) Integer.parseInt(zoomString))/10.0);
	    }
	    
	    if(blockMenuMode.contentEquals("custom")) {
			appPrefix = uiMessageBundle.getString("ardublock.ui.appprefix.pro");
		}else if (blockMenuMode.contentEquals("page")) {
			appPrefix = uiMessageBundle.getString("ardublock.ui.appprefix.sim");
		} else {
			appPrefix = uiMessageBundle.getString("ardublock.ui.appprefix.standard");
		}
		
		//NEW
		//**********
		ImageIcon newButtonIcon = new ImageIcon(getToolkit().getImage(GUI.class.getResource("/com/ardublock/block/new1.png")));
		ImageIcon newButtonIconHovered = new ImageIcon(getToolkit().getImage(GUI.class.getResource("/com/ardublock/block/new2.png")));
		ImageIcon newButtonIconPressed = new ImageIcon(getToolkit().getImage(GUI.class.getResource("/com/ardublock/block/new3.png")));
		ButtonMouseListener newButtonMouseListener = new ButtonMouseListener(newButtonIcon, newButtonIconHovered, newButtonIconPressed);
		
		KeyStroke ctrlnKeyStroke = KeyStroke.getKeyStroke(KeyEvent.VK_N, ActionEvent.CTRL_MASK);
		ActionListener newButtonListener = new NewButtonListener(this);	
		
		JButton newButton = new JButton(newButtonIcon);
		newButton.addMouseListener(newButtonMouseListener);
		newButton.setMargin(new Insets(0, 0, 0, 0));
		newButton.setContentAreaFilled(false);
		newButton.setBorderPainted(false);
		//JButton newButton = new JButton(uiMessageBundle.getString("ardublock.ui.new"));
		newButton.addActionListener(newButtonListener);
		newButton.setToolTipText(uiMessageBundle.getString("ardublock.ui.new.tooltip"));
		//newButton.registerKeyboardAction(newButtonListener, ctrlnKeyStroke, JComponent.WHEN_IN_FOCUSED_WINDOW);
		
		JMenuItem newMenuItem = new JMenuItem(uiMessageBundle.getString("ardublock.ui.new"));
		newMenuItem.addActionListener(newButtonListener);
		newMenuItem.registerKeyboardAction(newButtonListener, ctrlnKeyStroke, JComponent.WHEN_IN_FOCUSED_WINDOW);
		newMenuItem.setMnemonic(KeyEvent.VK_N);
		newMenuItem.setToolTipText(uiMessageBundle.getString("ardublock.ui.new.tooltip"));
		
		
		
		//SAVE
		//**********
		ImageIcon saveButtonIcon = new ImageIcon(getToolkit().getImage(GUI.class.getResource("/com/ardublock/block/save1.png")));
		ImageIcon saveButtonIconHovered = new ImageIcon(getToolkit().getImage(GUI.class.getResource("/com/ardublock/block/save2.png")));
		ImageIcon saveButtonIconPressed = new ImageIcon(getToolkit().getImage(GUI.class.getResource("/com/ardublock/block/save3.png")));
		ButtonMouseListener saveButtonMouseListener = new ButtonMouseListener(saveButtonIcon, saveButtonIconHovered, saveButtonIconPressed);
		
		KeyStroke ctrlsKeyStroke = KeyStroke.getKeyStroke(KeyEvent.VK_S, ActionEvent.CTRL_MASK);
		ActionListener saveButtonListener = new SaveButtonListener(this);
		
		JButton saveButton = new JButton(saveButtonIcon);
		saveButton.addMouseListener(saveButtonMouseListener);
		saveButton.setMargin(new Insets(0, 0, 0, 0));
		saveButton.setContentAreaFilled(false);
		saveButton.setBorderPainted(false);
		//JButton saveButton = new JButton(uiMessageBundle.getString("ardublock.ui.save"));
		saveButton.addActionListener(saveButtonListener);
		//saveButton.registerKeyboardAction(saveButtonListener, ctrlsKeyStroke, JComponent.WHEN_IN_FOCUSED_WINDOW);
		saveButton.setToolTipText(uiMessageBundle.getString("ardublock.ui.save.tooltip"));
		
		JMenuItem saveMenuItem = new JMenuItem(uiMessageBundle.getString("ardublock.ui.save"));
		saveMenuItem.addActionListener(saveButtonListener);
		saveMenuItem.setMnemonic(KeyEvent.VK_S);
		saveMenuItem.registerKeyboardAction(saveButtonListener, ctrlsKeyStroke, JComponent.WHEN_IN_FOCUSED_WINDOW);
		saveMenuItem.setToolTipText(uiMessageBundle.getString("ardublock.ui.save.tooltip"));
		
		
		//SAVE AS
		//**********
		KeyStroke ctrlshiftsKeyStroke = KeyStroke.getKeyStroke(KeyEvent.VK_S, ActionEvent.CTRL_MASK | ActionEvent.SHIFT_MASK);
		ActionListener saveAsButtonListener = new SaveAsButtonListener(this);	
		
		//JButton saveAsButton = new JButton(uiMessageBundle.getString("ardublock.ui.saveAs"));
		//saveAsButton.addActionListener(saveAsButtonListener);
		//saveAsButton.registerKeyboardAction(saveAsButtonListener, ctrlshiftsKeyStroke, JComponent.WHEN_IN_FOCUSED_WINDOW);
		//saveAsButton.setToolTipText(uiMessageBundle.getString("ardublock.ui.saveAs.tooltip"));
		
		JMenuItem saveAsMenuItem = new JMenuItem(uiMessageBundle.getString("ardublock.ui.saveAs"));		
		saveAsMenuItem.addActionListener(saveAsButtonListener);	
		saveAsMenuItem.registerKeyboardAction(saveAsButtonListener, ctrlshiftsKeyStroke, JComponent.WHEN_IN_FOCUSED_WINDOW);
		saveAsMenuItem.setToolTipText(uiMessageBundle.getString("ardublock.ui.saveAs.tooltip"));
		
		
		//OPEN
		//*********
		ImageIcon openButtonIcon = new ImageIcon(getToolkit().getImage(GUI.class.getResource("/com/ardublock/block/open1.png")));
		ImageIcon openButtonIconHovered = new ImageIcon(getToolkit().getImage(GUI.class.getResource("/com/ardublock/block/open2.png")));
		ImageIcon openButtonIconPressed = new ImageIcon(getToolkit().getImage(GUI.class.getResource("/com/ardublock/block/open3.png")));
		ButtonMouseListener openButtonMouseListener = new ButtonMouseListener(openButtonIcon, openButtonIconHovered, openButtonIconPressed);
		
		KeyStroke ctrloKeyStroke = KeyStroke.getKeyStroke(KeyEvent.VK_O, ActionEvent.CTRL_MASK);
		ActionListener openButtonListener = new OpenButtonListener(this);
		
		JButton openButton = new JButton(openButtonIcon);
		openButton.addMouseListener(openButtonMouseListener);
		openButton.setMargin(new Insets(0, 0, 0, 0));
		openButton.setContentAreaFilled(false);
		openButton.setBorderPainted(false);
		//JButton openButton = new JButton(uiMessageBundle.getString("ardublock.ui.load"));
		openButton.addActionListener(openButtonListener);
		//openButton.registerKeyboardAction(openButtonListener, ctrloKeyStroke, JComponent.WHEN_IN_FOCUSED_WINDOW);
		openButton.setToolTipText(uiMessageBundle.getString("ardublock.ui.load.tooltip"));
		
		JMenuItem openMenuItem = new JMenuItem(uiMessageBundle.getString("ardublock.ui.load"));
		openMenuItem.addActionListener(openButtonListener);
		openMenuItem.setMnemonic(KeyEvent.VK_O);
		openMenuItem.registerKeyboardAction(openButtonListener, ctrloKeyStroke, JComponent.WHEN_IN_FOCUSED_WINDOW);
		openMenuItem.setToolTipText(uiMessageBundle.getString("ardublock.ui.load.tooltip"));
		
		
		//GENERATE
		//**********
		ImageIcon generateButtonIcon = new ImageIcon(getToolkit().getImage(GUI.class.getResource("/com/ardublock/block/upload1.png")));
		ImageIcon generateButtonIconHovered = new ImageIcon(getToolkit().getImage(GUI.class.getResource("/com/ardublock/block/upload2.png")));
		ImageIcon generateButtonIconPressed = new ImageIcon(getToolkit().getImage(GUI.class.getResource("/com/ardublock/block/upload3.png")));
		ButtonMouseListener generateButtonMouseListener = new ButtonMouseListener(generateButtonIcon, generateButtonIconHovered, generateButtonIconPressed);
		
		KeyStroke ctrluKeyStroke = KeyStroke.getKeyStroke(KeyEvent.VK_U, ActionEvent.CTRL_MASK);
		ActionListener generateButtonListener = new GenerateCodeButtonListener(this, this, context);
		
		JButton generateButton = new JButton(generateButtonIcon);
		generateButton.addMouseListener(generateButtonMouseListener);
		generateButton.setMargin(new Insets(0, 0, 0, 0));
		generateButton.setContentAreaFilled(false);
		generateButton.setBorderPainted(false);
		//JButton generateButton = new JButton(uiMessageBundle.getString("ardublock.ui.upload"));
		generateButton.addActionListener(generateButtonListener);
		//generateButton.registerKeyboardAction(generateButtonListener, ctrluKeyStroke, JComponent.WHEN_IN_FOCUSED_WINDOW);
		generateButton.setToolTipText(uiMessageBundle.getString("ardublock.ui.upload.tooltip"));
		
		JMenuItem generateMenuItem = new JMenuItem(uiMessageBundle.getString("ardublock.ui.upload"));
		generateMenuItem.addActionListener(generateButtonListener);
		generateMenuItem.setMnemonic(KeyEvent.VK_U);
		generateMenuItem.registerKeyboardAction(generateButtonListener, ctrluKeyStroke, JComponent.WHEN_IN_FOCUSED_WINDOW);
		generateMenuItem.setToolTipText(uiMessageBundle.getString("ardublock.ui.upload.tooltip"));
		
		//Simulate
		//**********
		ImageIcon simulateButtonIcon = new ImageIcon(getToolkit().getImage(GUI.class.getResource("/com/ardublock/block/uploadSim1.png")));
		ImageIcon simulateButtonIconHovered = new ImageIcon(getToolkit().getImage(GUI.class.getResource("/com/ardublock/block/uploadSim2.png")));
		ImageIcon simulateButtonIconPressed = new ImageIcon(getToolkit().getImage(GUI.class.getResource("/com/ardublock/block/uploadSim3.png")));
		ButtonMouseListener simulateButtonMouseListener = new ButtonMouseListener(simulateButtonIcon, simulateButtonIconHovered, simulateButtonIconPressed);
		
		ActionListener simulateButtonListener = new SimulateCodeButtonListener(this, this, context);
		
		JButton simulateButton = new JButton(simulateButtonIcon);
		simulateButton.addMouseListener(simulateButtonMouseListener);
		simulateButton.setMargin(new Insets(0, 0, 0, 0));
		simulateButton.setContentAreaFilled(false);
		simulateButton.setBorderPainted(false);
		//JButton simulateButton = new JButton(uiMessageBundle.getString("ardublock.ui.simulation"));
		simulateButton.addActionListener(simulateButtonListener);
		simulateButton.setToolTipText(uiMessageBundle.getString("ardublock.ui.simulation.tooltip"));
		simulateButton.setVisible(false); //hide on startUp
		
		JMenuItem simulateMenuItem = new JMenuItem(uiMessageBundle.getString("ardublock.ui.simulation"));
		simulateMenuItem.addActionListener(simulateButtonListener);
		simulateMenuItem.setToolTipText(uiMessageBundle.getString("ardublock.ui.simulation.tooltip"));
		simulateMenuItem.setVisible(false); //hide on startUp
		
		//HELP
		//**************
		ImageIcon helpButtonIcon = new ImageIcon(getToolkit().getImage(GUI.class.getResource("/com/ardublock/block/helpSim1.png")));
		ImageIcon helpButtonIconHovered = new ImageIcon(getToolkit().getImage(GUI.class.getResource("/com/ardublock/block/helpSim2.png")));
		ImageIcon helpButtonIconPressed = new ImageIcon(getToolkit().getImage(GUI.class.getResource("/com/ardublock/block/helpSim3.png")));
		ButtonMouseListener helpButtonMouseListener = new ButtonMouseListener(helpButtonIcon, helpButtonIconHovered, helpButtonIconPressed);
		
		ActionListener simulatorHelpButtonListener = new SimulatorHelpButtonListener(this);
		ActionListener helpButtonListener = new HelpButtonListener(this);
		
		JButton helpButton = new JButton(helpButtonIcon);
		helpButton.addMouseListener(helpButtonMouseListener);
		helpButton.setMargin(new Insets(0, 0, 0, 0));
		helpButton.setContentAreaFilled(false);
		helpButton.setBorderPainted(false);
		//JButton simulatorHelpButton = new JButton(uiMessageBundle.getString("ardublock.ui.simulatorHelp"));
		//helpButton.addActionListener(simulatorHelpButtonListener);
		//helpButton.setToolTipText(uiMessageBundle.getString("ardublock.ui.simulatorHelp.tooltip"));
		//helpButton.setVisible(false); //hide on startUp
		helpButton.addActionListener(helpButtonListener);
		helpButton.setToolTipText(uiMessageBundle.getString("ardublock.ui.help.tooltip"));
		
		JMenuItem simulatorHelpMenuItem = new JMenuItem(uiMessageBundle.getString("ardublock.ui.simulatorHelp"));
		simulatorHelpMenuItem.addActionListener(simulatorHelpButtonListener);
		simulatorHelpMenuItem.setToolTipText(uiMessageBundle.getString("ardublock.ui.simulatorHelp.tooltip"));
		//simulatorHelpMenuItem.setVisible(false); //hide on startUp
		
		JMenuItem helpMenuItem = new JMenuItem(uiMessageBundle.getString("ardublock.ui.help"));
		helpMenuItem.addActionListener(helpButtonListener);
		helpMenuItem.setToolTipText(uiMessageBundle.getString("ardublock.ui.help.tooltip"));
		
		JMenuItem addLibrariesMenuItem = new JMenuItem(uiMessageBundle.getString("ardublock.ui.addLibraries"));
		ActionListener addLibrariesMenuItemListener = new ActionListener () {
			public void actionPerformed(ActionEvent e) {
				//addLibrary("LGI_QTouch");
				//addLibrary("DidacticNet");
				//addLibrary("DidacticEnc");
				//TODO: TEST addLibrary functionality
				Desktop desktop = Desktop.isDesktopSupported() ? Desktop.getDesktop() : null;
			    URL url;
			    if (desktop != null && desktop.isSupported(Desktop.Action.BROWSE)) {
			        try {
			        	url = new URL(uiMessageBundle.getString("ardublock.ui.libraries.domain"));
			            desktop.browse(url.toURI());
			        } catch (Exception e1) {
			            //e1.printStackTrace();
			        }
			    }
			}
		};
		addLibrariesMenuItem.addActionListener(addLibrariesMenuItemListener);
		addLibrariesMenuItem.setToolTipText(uiMessageBundle.getString("ardublock.ui.addLibraries.tooltip"));
		
        
		
		
		//SERIAL MONITOR
		//**************
		ImageIcon serialMonitorButtonIcon = new ImageIcon(getToolkit().getImage(GUI.class.getResource("/com/ardublock/block/serialMonitor1.png")));
		ImageIcon serialMonitorButtonIconHovered = new ImageIcon(getToolkit().getImage(GUI.class.getResource("/com/ardublock/block/serialMonitor2.png")));
		ImageIcon serialMonitorButtonIconPressed = new ImageIcon(getToolkit().getImage(GUI.class.getResource("/com/ardublock/block/serialMonitor3.png")));
		ButtonMouseListener serialMonitorButtonMouseListener = new ButtonMouseListener(serialMonitorButtonIcon, serialMonitorButtonIconHovered, serialMonitorButtonIconPressed);
		
		KeyStroke ctrlmKeyStroke = KeyStroke.getKeyStroke(KeyEvent.VK_M, ActionEvent.CTRL_MASK | ActionEvent.SHIFT_MASK);
		ActionListener serialButtonListener = new ActionListener () {
			public void actionPerformed(ActionEvent e) {
				context.getEditor().handleSerial();
			}
		};
		
		JButton serialMonitorButton = new JButton(serialMonitorButtonIcon);
		serialMonitorButton.addMouseListener(serialMonitorButtonMouseListener);
		serialMonitorButton.setMargin(new Insets(0, 0, 0, 0));
		serialMonitorButton.setContentAreaFilled(false);
		serialMonitorButton.setBorderPainted(false);
		//JButton serialMonitorButton = new JButton(uiMessageBundle.getString("ardublock.ui.serialMonitor"));
		serialMonitorButton.addActionListener(serialButtonListener);
		//serialMonitorButton.registerKeyboardAction(serialButtonListener, ctrlmKeyStroke, JComponent.WHEN_IN_FOCUSED_WINDOW);
		serialMonitorButton.setToolTipText(uiMessageBundle.getString("ardublock.ui.serialMonitor.tooltip"));
		
		JMenuItem serialMonitorMenuItem = new JMenuItem(uiMessageBundle.getString("ardublock.ui.serialMonitor"));
		serialMonitorMenuItem.addActionListener(serialButtonListener);
		serialMonitorMenuItem.registerKeyboardAction(serialButtonListener, ctrlmKeyStroke, JComponent.WHEN_IN_FOCUSED_WINDOW);
		serialMonitorMenuItem.setToolTipText(uiMessageBundle.getString("ardublock.ui.serialMonitor.tooltip"));
		
		
		//SERIAL PLOTTER
		//**************
		ImageIcon serialPlotterButtonIcon = new ImageIcon(getToolkit().getImage(GUI.class.getResource("/com/ardublock/block/serialPlotter1.png")));
		ImageIcon serialPlotterButtonIconHovered = new ImageIcon(getToolkit().getImage(GUI.class.getResource("/com/ardublock/block/serialPlotter2.png")));
		ImageIcon serialPlotterButtonIconPressed = new ImageIcon(getToolkit().getImage(GUI.class.getResource("/com/ardublock/block/serialPlotter3.png")));
		ButtonMouseListener serialPlotterButtonMouseListener = new ButtonMouseListener(serialPlotterButtonIcon, serialPlotterButtonIconHovered, serialPlotterButtonIconPressed);
		
		KeyStroke ctrlLKeyStroke = KeyStroke.getKeyStroke(KeyEvent.VK_L, ActionEvent.CTRL_MASK | ActionEvent.SHIFT_MASK);
		ActionListener serialPlotterButtonListener = new ActionListener () {
			public void actionPerformed(ActionEvent e) {
				context.getEditor().handlePlotter();
			}
		};
		
		JButton serialPlotterButton = new JButton(serialPlotterButtonIcon);
		serialPlotterButton.addMouseListener(serialPlotterButtonMouseListener);
		serialPlotterButton.setMargin(new Insets(0, 0, 0, 0));
		serialPlotterButton.setContentAreaFilled(false);
		serialPlotterButton.setBorderPainted(false);
		//JButton serialPlotterButton = new JButton(uiMessageBundle.getString("ardublock.ui.serialPlotter"));
		serialPlotterButton.addActionListener(serialPlotterButtonListener);
		//serialPlotterButton.registerKeyboardAction(serialPlotterButtonListener, ctrlLKeyStroke, JComponent.WHEN_IN_FOCUSED_WINDOW);
		serialPlotterButton.setToolTipText(uiMessageBundle.getString("ardublock.ui.serialPlotter.tooltip"));
		
		JMenuItem serialPlotterMenuItem = new JMenuItem(uiMessageBundle.getString("ardublock.ui.serialPlotter"));
		serialPlotterMenuItem.addActionListener(serialPlotterButtonListener);
		serialPlotterMenuItem.registerKeyboardAction(serialPlotterButtonListener, ctrlLKeyStroke, JComponent.WHEN_IN_FOCUSED_WINDOW);
		serialPlotterMenuItem.setToolTipText(uiMessageBundle.getString("ardublock.ui.serialPlotter.tooltip"));
		

		//****************************
		//BOTTOM
		//******************************
		
		//SAVE IMAGE
		//*****************************************
		ActionListener saveImageButtonListener = new SaveImageButtonListener(this);
		KeyStroke ctrlpKeyStroke = KeyStroke.getKeyStroke(KeyEvent.VK_P, ActionEvent.CTRL_MASK);
		
		JButton saveImageButton = new JButton(uiMessageBundle.getString("ardublock.ui.saveImage"));
		saveImageButton.addActionListener(saveImageButtonListener);	
		//saveImageButton.registerKeyboardAction(saveImageButtonListener, ctrlpKeyStroke, JComponent.WHEN_IN_FOCUSED_WINDOW);
		saveImageButton.setToolTipText(uiMessageBundle.getString("ardublock.ui.saveImage.tooltip"));
		
		JMenuItem saveImageMenuItem = new JMenuItem(uiMessageBundle.getString("ardublock.ui.saveImage"));
		saveImageMenuItem.addActionListener(saveImageButtonListener);
		saveImageMenuItem.registerKeyboardAction(saveImageButtonListener, ctrlpKeyStroke, JComponent.WHEN_IN_FOCUSED_WINDOW);
		saveImageMenuItem.setToolTipText(uiMessageBundle.getString("ardublock.ui.saveImage.tooltip"));
		//*****************************************
		
		//LMS SITE BUTTON
		//*****************************************
		JButton lmssiteButton = new JButton(uiMessageBundle.getString("ardublock.ui.lmssite"));
		lmssiteButton.addActionListener(new ActionListener () {
			public void actionPerformed(ActionEvent e) {
			    Desktop desktop = Desktop.isDesktopSupported() ? Desktop.getDesktop() : null;
			    URL url;
			    if (desktop != null && desktop.isSupported(Desktop.Action.BROWSE)) {
			        try {
			        	url = new URL(uiMessageBundle.getString("ardublock.ui.lmssite.domain"));
			            desktop.browse(url.toURI());
			        } catch (Exception e1) {
			            //e1.printStackTrace();
			        }
			    }
			}
		});
		lmssiteButton.setToolTipText(uiMessageBundle.getString("ardublock.ui.lmssite.tooltip"));
		//*****************************************
		
		//BLOCK REFERENCE BUTTON
		//*****************************************
		ActionListener blockreferenceButtonListener = new ActionListener () {
			public void actionPerformed(ActionEvent e) {
			    Desktop desktop = Desktop.isDesktopSupported() ? Desktop.getDesktop() : null;
			    URL url;
			    if (desktop != null && desktop.isSupported(Desktop.Action.BROWSE)) {
			        try {
			        	url = new URL(uiMessageBundle.getString("ardublock.ui.blockreference.domain"));
			            desktop.browse(url.toURI());
			        } catch (Exception e1) {
			            //e1.printStackTrace();
			        }
			    }
			}
		};
		
		JButton blockreferenceButton = new JButton(uiMessageBundle.getString("ardublock.ui.blockreference"));
		blockreferenceButton.addActionListener(blockreferenceButtonListener);
		blockreferenceButton.setToolTipText(uiMessageBundle.getString("ardublock.ui.blockreference.tooltip"));
		
		JMenuItem blockreferenceMenuItem = new JMenuItem(uiMessageBundle.getString("ardublock.ui.blockreference"));
		blockreferenceMenuItem.addActionListener(blockreferenceButtonListener);
		blockreferenceMenuItem.setToolTipText(uiMessageBundle.getString("ardublock.ui.blockreference.tooltip"));
		
		
		ActionListener languagestyleCheckboxListener = new ActionListener () {
			public void actionPerformed(ActionEvent e) {
				AbstractButton cButton = (AbstractButton) e.getSource();  
                boolean selected = cButton.getModel().isSelected();  
                if(selected) {
                	PreferencesData.setBoolean("ardublock.cstyle", true);
                } else {
                	PreferencesData.setBoolean("ardublock.cstyle", false);
                }
			}
		};
		
		JCheckBoxMenuItem languagestyleMenuItem = new JCheckBoxMenuItem(uiMessageBundle.getString("ardublock.ui.languagestyle"), PreferencesData.getBoolean("ardublock.cstyle"));
		languagestyleMenuItem.addActionListener(languagestyleCheckboxListener);
		languagestyleMenuItem.setToolTipText(uiMessageBundle.getString("ardublock.ui.languagestyle.tooltip"));
		
		ActionListener autosaveCheckboxListener = new ActionListener () {
			public void actionPerformed(ActionEvent e) {
				AbstractButton cButton = (AbstractButton) e.getSource();  
                boolean selected = cButton.getModel().isSelected();  
                if(selected) {
                	PreferencesData.setBoolean("ardublock.autosave", true);
                } else {
                	PreferencesData.setBoolean("ardublock.autosave", false);
                }
			}
		};
		
		JCheckBoxMenuItem autosaveMenuItem = new JCheckBoxMenuItem(uiMessageBundle.getString("ardublock.ui.autosave"), PreferencesData.getBoolean("ardublock.autosave"));
		autosaveMenuItem.addActionListener(autosaveCheckboxListener);
		autosaveMenuItem.setToolTipText(uiMessageBundle.getString("ardublock.ui.autosave.tooltip"));
		
		//*****************************************
		
		// VERSION LABEL
		//*****************************************
		//JLabel versionLabel = new JLabel(uiMessageBundle.getString("ardublock.ui.version"));
		//*****************************************
		
		//ADD ZOOM (currently only by CTRL +/- or mouse wheel
		//***************************************************
		//ZoomSlider zoomSlider = new ZoomSlider(workspace);
		//zoomSlider.reset();

		
		//TODO: TEST SAVE DEFAULT
		//Save new default program
		//*************************
		ActionListener saveDefaultButtonListener = new SaveDefaultButtonListener(this);	
		KeyStroke ctrlaltsKeyStroke  = KeyStroke.getKeyStroke(KeyEvent.VK_S, ActionEvent.CTRL_MASK | ActionEvent.ALT_MASK);
		
		/*JButton saveDefaultButton = new JButton(uiMessageBundle.getString("ardublock.ui.saveDefaultProgram"));
		saveDefaultButton.addActionListener(saveDefaultButtonListener);
		//saveDefaultButton.registerKeyboardAction(saveDefaultButtonListener, ctrlaltsKeyStroke, JComponent.WHEN_IN_FOCUSED_WINDOW);
		saveDefaultButton.setToolTipText(uiMessageBundle.getString("ardublock.ui.saveDefaultProgram.tooltip"));*/
		
		JMenuItem saveDefaultMenuItem = new JMenuItem(uiMessageBundle.getString("ardublock.ui.saveDefaultProgram"));
		saveDefaultMenuItem.addActionListener(saveDefaultButtonListener);
		saveDefaultMenuItem.registerKeyboardAction(saveDefaultButtonListener, ctrlaltsKeyStroke, JComponent.WHEN_IN_FOCUSED_WINDOW);
		saveDefaultMenuItem.setToolTipText(uiMessageBundle.getString("ardublock.ui.saveDefaultProgram.tooltip"));
		
		
		//SWITCH BLOCK MENU BASIC/PRO/SIM
		//*****************************************		
		ImageIcon modeBasicButtonIcon = new ImageIcon(getToolkit().getImage(GUI.class.getResource("/com/ardublock/block/basic1.png")));
		ImageIcon modeBasicButtonIconHovered = new ImageIcon(getToolkit().getImage(GUI.class.getResource("/com/ardublock/block/basic2.png")));
		ImageIcon modeBasicButtonIconPressed = new ImageIcon(getToolkit().getImage(GUI.class.getResource("/com/ardublock/block/basic3.png")));
		ImageIcon modeBasicButtonIconSelected = new ImageIcon(getToolkit().getImage(GUI.class.getResource("/com/ardublock/block/basic4.png")));
		
		ImageIcon modeProButtonIcon = new ImageIcon(getToolkit().getImage(GUI.class.getResource("/com/ardublock/block/full1.png")));//"com/ardublock/block/pro1.png")));
		ImageIcon modeProButtonIconHovered = new ImageIcon(getToolkit().getImage(GUI.class.getResource("/com/ardublock/block/full2.png")));//"/com/ardublock/block/pro2.png")));
		ImageIcon modeProButtonIconPressed = new ImageIcon(getToolkit().getImage(GUI.class.getResource("/com/ardublock/block/full3.png")));//"/com/ardublock/block/pro3.png")));
		ImageIcon modeProButtonIconSelected = new ImageIcon(getToolkit().getImage(GUI.class.getResource("/com/ardublock/block/full4.png")));//"/com/ardublock/block/pro4.png")));
	
/*		ImageIcon modeProButtonIcon = new ImageIcon(getToolkit().getImage(GUI.class.getResource("/com/ardublock/block/pro1.png")));
		ImageIcon modeProButtonIconHovered = new ImageIcon(getToolkit().getImage(GUI.class.getResource("/com/ardublock/block/pro2.png")));
		ImageIcon modeProButtonIconPressed = new ImageIcon(getToolkit().getImage(GUI.class.getResource("/com/ardublock/block/pro3.png")));
		ImageIcon modeProButtonIconSelected = new ImageIcon(getToolkit().getImage(GUI.class.getResource("/com/ardublock/block/pro4.png"))); */
		
		ImageIcon modeSimButtonIcon = new ImageIcon(getToolkit().getImage(GUI.class.getResource("/com/ardublock/block/sim1.png")));
		ImageIcon modeSimButtonIconHovered = new ImageIcon(getToolkit().getImage(GUI.class.getResource("/com/ardublock/block/sim2.png")));
		ImageIcon modeSimButtonIconPressed = new ImageIcon(getToolkit().getImage(GUI.class.getResource("/com/ardublock/block/sim3.png")));
		ImageIcon modeSimButtonIconSelected = new ImageIcon(getToolkit().getImage(GUI.class.getResource("/com/ardublock/block/sim4.png")));
		
		ButtonMouseListener modeBasicButtonMouseListener = new ButtonMouseListener(modeBasicButtonIcon, modeBasicButtonIconHovered, modeBasicButtonIconPressed,modeBasicButtonIconSelected); 
		ButtonMouseListener modeProButtonMouseListener = new ButtonMouseListener(modeProButtonIcon, modeProButtonIconHovered, modeProButtonIconPressed, modeProButtonIconSelected);
		ButtonMouseListener modeSimButtonMouseListener = new ButtonMouseListener(modeSimButtonIcon, modeSimButtonIconHovered, modeSimButtonIconPressed, modeSimButtonIconSelected);
		
		
		
		//Workspace: BASIC
		//*****************
		ActionListener modeBasicButtonListener = new ActionListener () {
			public void actionPerformed(ActionEvent e) {
				//get current zoomLevel
				
				//remove highlights
				context.resetHightlightBlock();
				
				//int currentWorkspaceZoom = zoomSlider.getValue();
					
				//get current zoomLevel
				//TODO: check why blocks are rearranged while changing WS
				double currentWorkspaceZoom = workspace.getCurrentWorkspaceZoom();
		        try {
		        	removeSearchableBlocks(workspace);
		        	WorkspaceController workspaceController = context.getWorkspaceController();
			        workspaceController.loadProject(getArduBlockString(), null , "default");	
			        addSearchableBlocks(workspace);
			        generateButton.setVisible(true);
			        generateMenuItem.setVisible(true);
			        generateMenuItem.registerKeyboardAction(generateButtonListener, ctrluKeyStroke, JComponent.WHEN_IN_FOCUSED_WINDOW);
			        simulateButton.setVisible(false);
			        simulateMenuItem.setVisible(false);
			        simulateMenuItem.unregisterKeyboardAction(ctrluKeyStroke);
			        //helpButton.setVisible(false);
			        removeAllActionListeners(helpButton);
			        helpButton.addActionListener(helpButtonListener);
					helpButton.setToolTipText(uiMessageBundle.getString("ardublock.ui.help.tooltip"));
			        serialMonitorButton.setVisible(true);
				    serialPlotterButton.setVisible(true);
				    modeBasicButtonMouseListener.setSelected(true);
				    modeProButtonMouseListener.setSelected(false);
				    modeSimButtonMouseListener.setSelected(false);
				    modeBasicButton.setIcon(modeBasicButtonIconSelected);
				    modeProButton.setIcon(modeProButtonIcon);
				    modeSimButton.setIcon(modeSimButtonIcon);
				    //modeBasicButton.setFont(new Font("Sans", Font.BOLD, 12));
			        //modeProButton.setFont(new Font("Sans", Font.PLAIN, 12));
			        //modeSimButton.setFont(new Font("Sans", Font.PLAIN, 12));
				    
			        appPrefix = uiMessageBundle.getString("ardublock.ui.appprefix.standard");
			        setTitle(makeFrameTitle());
			        blockMenuMode = "default";
			        //zoomSlider.reset();
			        //zoomSlider.setValue(currentWorkspaceZoom);
			        workspace.setWorkspaceZoom(currentWorkspaceZoom);
			        workspaceModeState=false;			        
		        } catch (Exception e1) {
		            //e1.printStackTrace();
		        }
			}
		};
		
		modeBasicButton = new JButton(modeBasicButtonIconSelected);
		modeBasicButton.setMargin(new Insets(0, 0, 0, 0));
		modeBasicButton.setContentAreaFilled(false);
		modeBasicButton.setBorderPainted(false);
		modeBasicButton.addMouseListener(modeBasicButtonMouseListener);
		modeBasicButtonMouseListener.setSelected(true);
		//modeBasicButton = new JButton(uiMessageBundle.getString("ardublock.ui.mode.standard"));
		//modeBasicButton.setFont(new Font("Sans", Font.BOLD, 12));
		//modeBasicButton.setForeground(Color.RED);
		modeBasicButton.addActionListener(modeBasicButtonListener);
		modeBasicButton.setToolTipText(uiMessageBundle.getString("ardublock.ui.mode.standard.tooltip"));
		
		
		JMenuItem modeBasicMenuItem = new JMenuItem("Blockset "+uiMessageBundle.getString("ardublock.ui.mode.standard"));
		modeBasicMenuItem.addActionListener(modeBasicButtonListener);
		modeBasicMenuItem.setToolTipText(uiMessageBundle.getString("ardublock.ui.mode.standard.tooltip"));
		
		//Workspace: PRO
		//*****************
		//*****************
		ActionListener modeProButtonListener = new ActionListener () {
			public void actionPerformed(ActionEvent e) {
				
				//remove highlights
				context.resetHightlightBlock();

				//int currentWorkspaceZoom = zoomSlider.getValue();
				
				//get current zoomLevel
				//TODO: check why blocks are rearanged while changing WS
				double currentWorkspaceZoom = workspace.getCurrentWorkspaceZoom();
		        try {
		        	removeSearchableBlocks(workspace);
		        	WorkspaceController workspaceController = context.getWorkspaceController();
		        	workspaceController.loadProject(getArduBlockString(), null , "custom");
				    addSearchableBlocks(workspace);
			        generateButton.setVisible(true);
			        generateMenuItem.setVisible(true);
			        generateMenuItem.registerKeyboardAction(generateButtonListener, ctrluKeyStroke, JComponent.WHEN_IN_FOCUSED_WINDOW);
			        simulateButton.setVisible(false);
			        simulateMenuItem.setVisible(false);
			        simulateMenuItem.unregisterKeyboardAction(ctrluKeyStroke);
			        //helpButton.setVisible(false);
			        removeAllActionListeners(helpButton);
			        helpButton.addActionListener(helpButtonListener);
					helpButton.setToolTipText(uiMessageBundle.getString("ardublock.ui.help.tooltip"));
			        serialMonitorButton.setVisible(true);
				    serialPlotterButton.setVisible(true);
				    modeBasicButtonMouseListener.setSelected(false);
				    modeProButtonMouseListener.setSelected(true);
				    modeSimButtonMouseListener.setSelected(false);
				    modeBasicButton.setIcon(modeBasicButtonIcon);
				    modeProButton.setIcon(modeProButtonIconSelected);
				    modeSimButton.setIcon(modeSimButtonIcon);
			        //modeBasicButton.setFont(new Font("Sans", Font.PLAIN, 12));
			        //modeProButton.setFont(new Font("Sans", Font.BOLD, 12));
			        //modeSimButton.setFont(new Font("Sans", Font.PLAIN, 12));
			        appPrefix = uiMessageBundle.getString("ardublock.ui.appprefix.pro");
			        setTitle(makeFrameTitle());
			        blockMenuMode = "custom";
			        //zoomSlider.reset();
			        //zoomSlider.setValue(currentWorkspaceZoom);
			        workspace.setWorkspaceZoom(currentWorkspaceZoom);
			        workspaceModeState=true;
			        
		        } catch (Exception e1) {
		            //e1.printStackTrace();
		        }
			}
		};
		
		modeProButton = new JButton(modeProButtonIcon);
		modeProButton.setMargin(new Insets(0, 0, 0, 0));
		modeProButton.setContentAreaFilled(false);
		modeProButton.setBorderPainted(false);
		modeProButton.addMouseListener(modeProButtonMouseListener);
		//modeProButton = new JButton(uiMessageBundle.getString("ardublock.ui.mode.expert"));
		//modeProButton.setFont(new Font("Sans", Font.PLAIN, 12));
		modeProButton.addActionListener(modeProButtonListener);
		modeProButton.setToolTipText(uiMessageBundle.getString("ardublock.ui.mode.expert.tooltip"));
		
		JMenuItem modeProMenuItem = new JMenuItem("Blockset "+uiMessageBundle.getString("ardublock.ui.mode.expert"));
		modeProMenuItem.addActionListener(modeProButtonListener);
		modeProMenuItem.setToolTipText(uiMessageBundle.getString("ardublock.ui.mode.expert.tooltip"));
		
		//Workspace: SIM
		//*****************
		ActionListener modeSimButtonListener = new ActionListener () {
			public void actionPerformed(ActionEvent e) {
				
				//remove highlights
				context.resetHightlightBlock();
				
				//int currentWorkspaceZoom = zoomSlider.getValue();
				
				//get current zoomLevel
				//TODO: check why blocks are rearanged while changing WS
				double currentWorkspaceZoom = workspace.getCurrentWorkspaceZoom();
				
				try {
					removeSearchableBlocks(workspace);
				  	WorkspaceController workspaceController = context.getWorkspaceController();
				  	//TODO check if reduced sim-blockset is necessary
				   	workspaceController.loadProject(getArduBlockString(), null , "page");
				   	addSearchableBlocks(workspace);
				    generateButton.setVisible(false);
				    generateMenuItem.setVisible(false);
				    generateMenuItem.unregisterKeyboardAction(ctrluKeyStroke);
				    simulateButton.setVisible(true);
				    simulateMenuItem.setVisible(true);
				    simulateMenuItem.registerKeyboardAction(simulateButtonListener, ctrluKeyStroke, JComponent.WHEN_IN_FOCUSED_WINDOW);
				    //helpButton.setVisible(true);
				    removeAllActionListeners(helpButton);
				    helpButton.addActionListener(simulatorHelpButtonListener);
				    helpButton.addActionListener(helpButtonListener);
					helpButton.setToolTipText(uiMessageBundle.getString("ardublock.ui.simulatorHelp.tooltip"));
				    serialMonitorButton.setVisible(false);
				    serialPlotterButton.setVisible(false);
				    modeBasicButtonMouseListener.setSelected(false);
				    modeProButtonMouseListener.setSelected(false);
				    modeSimButtonMouseListener.setSelected(true);
				    modeBasicButton.setIcon(modeBasicButtonIcon);
				    modeProButton.setIcon(modeProButtonIcon);
				    modeSimButton.setIcon(modeSimButtonIconSelected);
			        //modeBasicButton.setFont(new Font("Sans", Font.PLAIN, 12));
			        //modeProButton.setFont(new Font("Sans", Font.PLAIN, 12));
			        //modeSimButton.setFont(new Font("Sans", Font.BOLD, 12));
				    appPrefix = uiMessageBundle.getString("ardublock.ui.appprefix.sim");
				    setTitle(makeFrameTitle());
			        blockMenuMode = "page";
				    //zoomSlider.reset();
				    //zoomSlider.setValue(currentWorkspaceZoom);
				    workspace.setWorkspaceZoom(currentWorkspaceZoom); 
				    workspaceModeState=false;
			      
		        } catch (Exception e1) {
		           //e1.printStackTrace();
		        }
			}
		};
		
		modeSimButton = new JButton(modeSimButtonIcon);
		modeSimButton.setMargin(new Insets(0, 0, 0, 0));
		modeSimButton.setContentAreaFilled(false);
		modeSimButton.setBorderPainted(false);
		modeSimButton.addMouseListener(modeSimButtonMouseListener);
		//modeSimButton = new JButton(uiMessageBundle.getString("ardublock.ui.mode.sim"));
		//modeSimButton.setFont(new Font("Sans", Font.PLAIN, 12));
		modeSimButton.addActionListener(modeSimButtonListener);
		modeSimButton.setToolTipText(uiMessageBundle.getString("ardublock.ui.mode.sim.tooltip"));
		
		JMenuItem modeSimMenuItem = new JMenuItem("Blockset "+uiMessageBundle.getString("ardublock.ui.mode.sim"));
		modeSimMenuItem.addActionListener(modeSimButtonListener);
		modeSimMenuItem.setToolTipText(uiMessageBundle.getString("ardublock.ui.mode.sim.tooltip"));
		//*****************************************
		
		
		if(PreferencesData.get("ardublock.workspace.mode").contentEquals("custom")) {
			generateButton.setVisible(true);
	        generateMenuItem.setVisible(true);
	        generateMenuItem.registerKeyboardAction(generateButtonListener, ctrluKeyStroke, JComponent.WHEN_IN_FOCUSED_WINDOW);
	        simulateButton.setVisible(false);
	        simulateMenuItem.setVisible(false);
	        simulateMenuItem.unregisterKeyboardAction(ctrluKeyStroke);
	        //helpButton.setVisible(false);
	        helpButton.removeActionListener(simulatorHelpButtonListener);
	        helpButton.addActionListener(helpButtonListener);
			helpButton.setToolTipText(uiMessageBundle.getString("ardublock.ui.help.tooltip"));
	        serialMonitorButton.setVisible(true);
		    serialPlotterButton.setVisible(true);
		    modeBasicButtonMouseListener.setSelected(false);
		    modeProButtonMouseListener.setSelected(true);
		    modeSimButtonMouseListener.setSelected(false);
		    modeBasicButton.setIcon(modeBasicButtonIcon);
		    modeProButton.setIcon(modeProButtonIconSelected);
		    modeSimButton.setIcon(modeSimButtonIcon);
		}else if (PreferencesData.get("ardublock.workspace.mode").contentEquals("page")) {
			generateButton.setVisible(false);
		    generateMenuItem.setVisible(false);
		    generateMenuItem.unregisterKeyboardAction(ctrluKeyStroke);
		    simulateButton.setVisible(true);
		    simulateMenuItem.setVisible(true);
		    simulateMenuItem.registerKeyboardAction(simulateButtonListener, ctrluKeyStroke, JComponent.WHEN_IN_FOCUSED_WINDOW);
		    //helpButton.setVisible(true);
		    helpButton.removeActionListener(helpButtonListener);
		    helpButton.addActionListener(simulatorHelpButtonListener);
			helpButton.setToolTipText(uiMessageBundle.getString("ardublock.ui.simulatorHelp.tooltip"));
		    serialMonitorButton.setVisible(false);
		    serialPlotterButton.setVisible(false);
		    modeBasicButtonMouseListener.setSelected(false);
		    modeProButtonMouseListener.setSelected(false);
		    modeSimButtonMouseListener.setSelected(true);
		    modeBasicButton.setIcon(modeBasicButtonIcon);
		    modeProButton.setIcon(modeProButtonIcon);
		    modeSimButton.setIcon(modeSimButtonIconSelected);
		} else {
			//modeBasicButtonMouseListener.setSelected(true);
			//modeProButtonMouseListener.setSelected(false);
			//modeSimButtonMouseListener.setSelected(false);
		}
		
				
		//COPY PROGRAM
		//*****************************************
		KeyStroke ctrlcKeyStroke = KeyStroke.getKeyStroke(KeyEvent.VK_C, ActionEvent.CTRL_MASK | ActionEvent.SHIFT_MASK);
		ActionListener copyButtonListener = new CopyButtonListener(context);
		
		/*JButton copyButton = new JButton(uiMessageBundle.getString("ardublock.ui.copy"));
		copyButton.addActionListener(copyButtonListener);
		//copyButton.registerKeyboardAction(copyButtonListener, ctrlcKeyStroke, JComponent.WHEN_IN_FOCUSED_WINDOW);
		copyButton.setToolTipText(uiMessageBundle.getString("ardublock.ui.copy.tooltip"));
		*/
		
		JMenuItem copyButtonMenuItem = new JMenuItem(uiMessageBundle.getString("ardublock.ui.copy"));
		copyButtonMenuItem.registerKeyboardAction(serialButtonListener, ctrlcKeyStroke, JComponent.WHEN_IN_FOCUSED_WINDOW);
		copyButtonMenuItem.addActionListener(copyButtonListener);
		copyButtonMenuItem.setToolTipText(uiMessageBundle.getString("ardublock.ui.copy.tooltip"));
		
		
		//PASTE PROGRAM
		//*****************************************
		KeyStroke ctrlvKeyStroke = KeyStroke.getKeyStroke(KeyEvent.VK_V, ActionEvent.CTRL_MASK | ActionEvent.SHIFT_MASK);
		ActionListener pasteButtonListener = new PasteButtonListener(this, context, uiMessageBundle, workspaceModeState);
		
		/*JButton pasteButton = new JButton(uiMessageBundle.getString("ardublock.ui.paste"));
		pasteButton.addActionListener(pasteButtonListener);
		//pasteButton.registerKeyboardAction(pasteButtonListener, ctrlvKeyStroke, JComponent.WHEN_IN_FOCUSED_WINDOW);
		pasteButton.setToolTipText(uiMessageBundle.getString("ardublock.ui.paste.tooltip"));*/
		
		JMenuItem pasteButtonMenuItem = new JMenuItem(uiMessageBundle.getString("ardublock.ui.paste"));
		pasteButtonMenuItem.registerKeyboardAction(pasteButtonListener, ctrlvKeyStroke, JComponent.WHEN_IN_FOCUSED_WINDOW);
		pasteButtonMenuItem.addActionListener(pasteButtonListener);
		pasteButtonMenuItem.setToolTipText(uiMessageBundle.getString("ardublock.ui.paste.tooltip"));
		
		
		//WEBSITE
		//*****************************************		
		ActionListener websiteButtonListener = new ActionListener () {
			public void actionPerformed(ActionEvent e) {
			    Desktop desktop = Desktop.isDesktopSupported() ? Desktop.getDesktop() : null;
			    URL url;
			    if (desktop != null && desktop.isSupported(Desktop.Action.BROWSE)) {
			        try {
			        	url = new URL(uiMessageBundle.getString("ardublock.ui.website.domain"));
			            desktop.browse(url.toURI());
			        } catch (Exception e1) {
			            //e1.printStackTrace();
			        }
			    }
			}
		};
		
		//JButton websiteButton = new JButton(uiMessageBundle.getString("ardublock.ui.website"));
		//websiteButton.addActionListener(websiteButtonListener);
		//websiteButton.setToolTipText(uiMessageBundle.getString("ardublock.ui.website.tooltip"));
		
		JMenuItem websiteButtonMenuItem = new JMenuItem(uiMessageBundle.getString("ardublock.ui.website"));
		websiteButtonMenuItem.addActionListener(websiteButtonListener);
		websiteButtonMenuItem.setToolTipText(uiMessageBundle.getString("ardublock.ui.website.tooltip"));
		//*****************************************
		
		//VERSION
		//*********
		ActionListener repositoryButtonListener = new ActionListener () {
			public void actionPerformed(ActionEvent e) {
			    Desktop desktop = Desktop.isDesktopSupported() ? Desktop.getDesktop() : null;
			    URL url;
			    if (desktop != null && desktop.isSupported(Desktop.Action.BROWSE)) {
			        try {
			        	url = new URL(uiMessageBundle.getString("ardublock.ui.website.repository"));
			            desktop.browse(url.toURI());
			        } catch (Exception e1) {
			            //e1.printStackTrace();
			        }
			    }
			}
		};
		
		
		
		JMenuItem versionMenuItem = new JMenuItem("ArduBlock " + uiMessageBundle.getString("ardublock.ui.version"));
		versionMenuItem.addActionListener(repositoryButtonListener);
		versionMenuItem.setToolTipText(uiMessageBundle.getString("ardublock.ui.repository.tooltip"));
		
		
		//************************************
		//EXAMPLES
		//*************************************
		//TODO: test if adding automatically is possible
		JMenuItem blinkExample = new JMenuItem(uiMessageBundle.getString("ardublock.examples.blink"));
		blinkExample.addActionListener(new OpenExampleButtonListener("Blink.abp", this));
		
		JMenuItem digitalSerialExample = new JMenuItem(uiMessageBundle.getString("ardublock.examples.digitalserial"));
		digitalSerialExample.addActionListener(new OpenExampleButtonListener("DigitalReadSerial.abp", this));
		
		JMenuItem analogSerialExample = new JMenuItem(uiMessageBundle.getString("ardublock.examples.analogserial"));
		analogSerialExample.addActionListener(new OpenExampleButtonListener("DigitalReadSerial.abp", this));
		
		JMenuItem buttonLedExample = new JMenuItem(uiMessageBundle.getString("ardublock.examples.buttonled"));
		buttonLedExample.addActionListener(new OpenExampleButtonListener("ButtonLED.abp", this));
		
		JMenuItem digitalVariableToggleExample = new JMenuItem(uiMessageBundle.getString("ardublock.examples.digitalvariabletoggle"));
		digitalVariableToggleExample.addActionListener(new OpenExampleButtonListener("DigitalVariableToggle.abp", this));
		
		JMenuItem counterVariableExample = new JMenuItem(uiMessageBundle.getString("ardublock.examples.countervariable"));
		counterVariableExample.addActionListener(new OpenExampleButtonListener("CounterVariable.abp", this));
		
		JMenuItem whileButtonExample = new JMenuItem(uiMessageBundle.getString("ardublock.examples.whilebutton"));
		whileButtonExample.addActionListener(new OpenExampleButtonListener("WhileButtonBlink.abp", this));
		
		JMenuItem psnBrokerExample = new JMenuItem(uiMessageBundle.getString("ardublock.examples.psnBroker"));
		psnBrokerExample.addActionListener(new OpenExampleButtonListener("Broker.abp", this));
		
		JMenuItem psnClient1Example = new JMenuItem(uiMessageBundle.getString("ardublock.examples.psnClient1"));
		psnClient1Example.addActionListener(new OpenExampleButtonListener("Client1.abp", this));
		
		JMenuItem psnClient2Example = new JMenuItem(uiMessageBundle.getString("ardublock.examples.psnClient2"));
		psnClient2Example.addActionListener(new OpenExampleButtonListener("Client2.abp", this));
		
		JMenuItem psnChatExample = new JMenuItem(uiMessageBundle.getString("ardublock.examples.psnChat"));
		psnChatExample.addActionListener(new OpenExampleButtonListener("Chat.abp", this));

		
		//************************************
		//PANELS
		//*************************************
		//PANELS
		JPanel topPanel = new JPanel(new BorderLayout()); //topPanel.setLayout(new FlowLayout());
		JPanel bottomPanel = new JPanel(new FlowLayout(FlowLayout.LEFT));
		JPanel menuePanel = new JPanel(new BorderLayout());//new FlowLayout(FlowLayout.LEFT));
		JPanel buttonPanel = new JPanel(new FlowLayout(FlowLayout.LEFT));
		JComponent searchBarComponent = searchBar.getComponent();
		
		bottomPanel.setBackground(new Color(40, 76, 102));
		buttonPanel.setBackground(new Color(40, 76, 102));
		
		//MENUS
		JMenuBar menuBar = new JMenuBar();
		JMenu fileMenu = new JMenu(uiMessageBundle.getString("ardublock.ui.menu.file"));
		JMenu programMenu = new JMenu(uiMessageBundle.getString("ardublock.ui.menu.program"));
		JMenu toolsMenu = new JMenu(uiMessageBundle.getString("ardublock.ui.menu.tools"));
		JMenu blocksMenu = new JMenu(uiMessageBundle.getString("ardublock.ui.menu.blocks"));
		JMenu aboutMenu = new JMenu(uiMessageBundle.getString("ardublock.ui.menu.about"));
		JMenu helpMenu = new JMenu(uiMessageBundle.getString("ardublock.ui.menu.help"));
		
		JMenu examplesSubMenu = new JMenu(uiMessageBundle.getString("ardublock.ui.submenu.examples"));
		JMenu basicExamplesSubMenu = new JMenu(uiMessageBundle.getString("ardublock.ui.submenu.examples.basic"));
		JMenu digitalExamplesSubMenu = new JMenu(uiMessageBundle.getString("ardublock.ui.submenu.examples.digital"));
		JMenu analogExamplesSubMenu = new JMenu(uiMessageBundle.getString("ardublock.ui.submenu.examples.analog"));
		JMenu communicationExamplesSubMenu = new JMenu(uiMessageBundle.getString("ardublock.ui.submenu.examples.communication"));
		
		menuBar.add(fileMenu);
		menuBar.add(programMenu);
		menuBar.add(toolsMenu);
		menuBar.add(blocksMenu);
		menuBar.add(aboutMenu);
		menuBar.add(helpMenu);
		
		fileMenu.add(newMenuItem);
		fileMenu.add(openMenuItem);
		fileMenu.addSeparator();
		fileMenu.add(examplesSubMenu);
		fileMenu.addSeparator();
		fileMenu.add(saveMenuItem);
		fileMenu.add(saveAsMenuItem);
		fileMenu.addSeparator();
		fileMenu.add(autosaveMenuItem);
		
		examplesSubMenu.add(basicExamplesSubMenu);
		examplesSubMenu.add(digitalExamplesSubMenu);
		examplesSubMenu.add(analogExamplesSubMenu);
		examplesSubMenu.add(communicationExamplesSubMenu);
		
		basicExamplesSubMenu.add(blinkExample);
		digitalExamplesSubMenu.add(digitalSerialExample);
		digitalExamplesSubMenu.add(buttonLedExample);
		digitalExamplesSubMenu.add(digitalVariableToggleExample);
		digitalExamplesSubMenu.add(whileButtonExample);
		analogExamplesSubMenu.add(analogSerialExample);
		analogExamplesSubMenu.add(counterVariableExample);
		communicationExamplesSubMenu.add(psnBrokerExample);
		communicationExamplesSubMenu.add(psnClient1Example);
		communicationExamplesSubMenu.add(psnClient2Example);
		communicationExamplesSubMenu.add(psnChatExample);
		
		programMenu.add(generateMenuItem);
		programMenu.add(simulateMenuItem);
		programMenu.addSeparator();
		programMenu.add(copyButtonMenuItem);
		programMenu.add(pasteButtonMenuItem);
		programMenu.addSeparator();
		programMenu.add(saveImageMenuItem);
		programMenu.add(saveDefaultMenuItem);		
		
		toolsMenu.add(serialMonitorMenuItem);
		toolsMenu.add(serialPlotterMenuItem);
		
		blocksMenu.add(modeBasicMenuItem);
		blocksMenu.add(modeProMenuItem);
		blocksMenu.add(modeSimMenuItem);
		//blocksMenu.addSeparator();
		//blocksMenu.add(blockreferenceMenuItem);
		blocksMenu.addSeparator();
		blocksMenu.add(languagestyleMenuItem);
		
		aboutMenu.add(websiteButtonMenuItem);
		//aboutMenu.add(simulatorHelpMenuItem);
		aboutMenu.add(versionMenuItem);
		
		helpMenu.add(helpMenuItem);
		helpMenu.add(simulatorHelpMenuItem);
		//	helpMenu.add(addLibrariesMenuItem);
		
		menuePanel.add(menuBar, BorderLayout.CENTER );
		topPanel.add(menuePanel, BorderLayout.NORTH); //TEST
		topPanel.add(buttonPanel, BorderLayout.CENTER); //TEST
		
		buttonPanel.add(generateButton);
		buttonPanel.add(simulateButton); 
		buttonPanel.add(Box.createRigidArea(new Dimension(30, 0))); //std value 30,0
		buttonPanel.add(newButton);
		buttonPanel.add(openButton);
		buttonPanel.add(saveButton);
		//topPanel.add(saveAsButton);	
		buttonPanel.add(Box.createRigidArea(new Dimension(15, 0))); //std value 30,0
		buttonPanel.add(helpButton);
		buttonPanel.add(Box.createRigidArea(new Dimension(30, 0))); //std value 30,0
		buttonPanel.add(serialMonitorButton);
		buttonPanel.add(serialPlotterButton);
		
		bottomPanel.add(modeBasicButton);
		bottomPanel.add(modeProButton);
		bottomPanel.add(modeSimButton);
		bottomPanel.add(Box.createRigidArea(new Dimension(30, 0))); //std value 30,0
		bottomPanel.add(searchBarComponent);
		
		
		//bottomPanel.add(blockreferenceButton);
		//bottomPanel.add(Box.createRigidArea(new Dimension(5, 0))); //std value 30,0
		//bottomPanel.add(zoomSlider);
		//bottomPanel.add(zoomInButton);
		//bottomPanel.add(zoomResetButton);
		//bottomPanel.add(zoomOutButton);
		//bottomPanel.add(Box.createRigidArea(new Dimension(5, 0)));//std value 30,0
		//bottomPanel.add(copyButton);
		//bottomPanel.add(pasteButton);
		//bottomPanel.add(Box.createRigidArea(new Dimension(30, 0))); //std value 30,0
		//bottomPanel.add(saveImageButton);
		//bottomPanel.add(saveDefaultButton);
		//bottomPanel.add(Box.createRigidArea(new Dimension(5, 0))); //std value 30,0
		//bottomPanel.add(versionLabel);
		//bottomPanel.add(Box.createRigidArea(new Dimension(5, 0))); //std value 30,0
		//bottomPanel.add(websiteButton);
		
		
		//bottomPanel.add(lmssiteButton);
		
		this.add(topPanel, BorderLayout.NORTH);
		this.add(bottomPanel, BorderLayout.SOUTH);
		this.add(workspace, BorderLayout.CENTER);

		//** ActionListener without buttons*/
		ActionListener zoomInButtonListener = new ZoomInButtonListener(workspace);
		KeyStroke ctrlPlusKeyStroke = KeyStroke.getKeyStroke(KeyEvent.VK_PLUS, ActionEvent.CTRL_MASK);
		workspace.registerKeyboardAction(zoomInButtonListener, ctrlPlusKeyStroke, JComponent.WHEN_IN_FOCUSED_WINDOW);
		
		ActionListener zoomOutButtonListener = new ZoomOutButtonListener(workspace);
		KeyStroke ctrlMinusKeyStroke = KeyStroke.getKeyStroke(KeyEvent.VK_MINUS, ActionEvent.CTRL_MASK);
		workspace.registerKeyboardAction(zoomOutButtonListener, ctrlMinusKeyStroke, JComponent.WHEN_IN_FOCUSED_WINDOW);
	}
	
	public void doOpenArduBlockFile()
	{
		if (context.isWorkspaceChanged())
		{
			int optionValue = JOptionPane.showOptionDialog(this, uiMessageBundle.getString("message.content.open_unsaved"), uiMessageBundle.getString("message.title.question"), JOptionPane.YES_NO_CANCEL_OPTION, JOptionPane.QUESTION_MESSAGE, null, null, JOptionPane.YES_OPTION);
			if (optionValue == JOptionPane.YES_OPTION)
			{
				doSaveArduBlockFile();
				this.loadFile();
			}
			else
			{
				if (optionValue == JOptionPane.NO_OPTION)
				{
					this.loadFile();
				}
			}
		}
		else
		{
			this.loadFile();
		}
		this.setTitle(makeFrameTitle());
	}
	
	private void loadFile()
	{	
		//get current workspace-zoomLevel
		//TODO: get Page non static
		//TODO: check if zoom handling is possible in WorkspacecController
		@SuppressWarnings("static-access")
		double currentWorkspaceZoom = context.getWorkspace().getPageNamed("Main").getZoomLevel();
		
		int result = fileChooser.showOpenDialog(this);
		if (result == JFileChooser.APPROVE_OPTION)
		{
			File savedFile = fileChooser.getSelectedFile();
			if (!savedFile.exists())
			{
				//JOptionPane.showOptionDialog(this, uiMessageBundle.getString("message.file_not_found"), uiMessageBundle.getString("message.title.error"), JOptionPane.DEFAULT_OPTION, JOptionPane.ERROR_MESSAGE, null, null, JOptionPane.OK_OPTION);
				int optionValue = JOptionPane.showOptionDialog(this, uiMessageBundle.getString("message.file_not_found_new_file"), uiMessageBundle.getString("message.title.error"), JOptionPane.YES_NO_OPTION, JOptionPane.ERROR_MESSAGE, null, null, JOptionPane.OK_OPTION);
				if(optionValue == JOptionPane.YES_OPTION) {
					context.resetWorkspace();
					context.setWorkspaceChanged(false);
					chooseFileAndSave(getArduBlockString());
				}
				return ;
			}
			
			try
			{
				this.setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
				context.loadArduBlockFile(savedFile);	
				context.setWorkspaceChanged(false);
			}
			catch (IOException e)
			{
				JOptionPane.showOptionDialog(this, uiMessageBundle.getString("message.file_not_found"), uiMessageBundle.getString("message.title.error"), JOptionPane.DEFAULT_OPTION, JOptionPane.ERROR_MESSAGE, null, null, JOptionPane.OK_OPTION);
				//e.printStackTrace();
			}
			finally
			{
				this.setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
			}
		}
		
		try {
			context.getWorkspaceController().loadProject(getArduBlockString(), null , blockMenuMode);
	        /*if(workspaceModeState){
	        	context.getWorkspaceController().loadProject(getArduBlockString(), null , "custom");	
	        }*/
        } catch (Exception e1) {
            //e1.printStackTrace();
        }
		
		//Set workspace to current zoomLevel
		context.getWorkspace().setWorkspaceZoom(currentWorkspaceZoom);
	}
	
	//load example-ArduBlock Files

	//TODO: TESTTESTTEST
	public void openArduBlockExample(String exampleName) {
		String examplePath = "/com/ardublock/examples/"+ exampleName;
		
		if (context.isWorkspaceChanged())
		{
			int optionValue = JOptionPane.showOptionDialog(this, uiMessageBundle.getString("message.content.open_unsaved"), uiMessageBundle.getString("message.title.question"), JOptionPane.YES_NO_CANCEL_OPTION, JOptionPane.QUESTION_MESSAGE, null, null, JOptionPane.YES_OPTION);
			if (optionValue == JOptionPane.YES_OPTION)
			{
				doSaveArduBlockFile();
			}
		}

		try {
	    	InputStream is = getClass().getResourceAsStream(examplePath);
		    InputStreamReader isr = new InputStreamReader(is);
		    BufferedReader br = new BufferedReader(isr);
		    String defaultFileString = new String();
		    
		    String line = null;
		    while ((line = br.readLine()) != null) {
		    	defaultFileString += line;
		    }
		    
	    	br.close();
	 	    isr.close();
			is.close();
			
			context.getWorkspaceController().loadProject(defaultFileString, null , blockMenuMode);
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		context.setSaveFileName(exampleName);
		//TODO: SET programm title 
		this.setTitle(makeFrameTitle());
	}
	
	public void doSaveArduBlockFile()
	{
		if (!context.isWorkspaceChanged())
		{
			return ;
		}
		
		String saveString = getArduBlockString();
		
		if (context.getSaveFilePath() == null)
		{
			chooseFileAndSave(saveString);
		}
		else
		{
			File saveFile = new File(context.getSaveFilePath());
			writeFileAndUpdateFrame(saveString, saveFile);
		}
	}
	
	//TODO: TEST safe new defaultProgram
	public void doSaveDefaultArduBlockFile()
	{
		if(askUserOverwriteDefaultFile()){
			String saveString = getArduBlockString();	
			String savePath = "";
			try {
				savePath = new File(URLDecoder.decode(getClass().getProtectionDomain().getCodeSource().getLocation().getFile(), "UTF-8")).getParentFile().getPath();
			} catch (UnsupportedEncodingException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			//TODO: set defaultFileName in ardublock.properties
			File saveFile = checkFileSuffix(new File (savePath+ File.separator +"defaultProgram.abp"));
			try {
				saveArduBlockToFile(saveString, saveFile);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
	
	public void doSaveAsArduBlockFile()
	{
		if (context.isWorkspaceEmpty())
		{
			return ;
		}
		
		String saveString = getArduBlockString();
		
		chooseFileAndSave(saveString);
		
	}
	
	private void chooseFileAndSave(String ardublockString)
	{
		File saveFile = letUserChooseSaveFile();
		if (saveFile == null)
		{
			return ;
		}
		
		saveFile = checkFileSuffix(saveFile);
		
		if (saveFile.exists() && !askUserOverwriteExistedFile())
		{
			return ;
		}
		
		writeFileAndUpdateFrame(ardublockString, saveFile);
	}
	
	private String getArduBlockString()
	{
		WorkspaceController workspaceController = context.getWorkspaceController();
		return workspaceController.getSaveString();
	}
	
	private void writeFileAndUpdateFrame(String ardublockString, File saveFile) 
	{
		try
		{
			saveArduBlockToFile(ardublockString, saveFile);
			context.setWorkspaceChanged(false);
			this.setTitle(this.makeFrameTitle());
		}
		catch (IOException e)
		{
			//e.printStackTrace();
		}
		
	}
	
	private File letUserChooseSaveFile()
	{
		int chooseResult;
		chooseResult = fileChooser.showSaveDialog(this);
		if (chooseResult == JFileChooser.APPROVE_OPTION)
		{
			return fileChooser.getSelectedFile();
		}
		return null;
	}
	
	private boolean askUserSaveFileOnExit()
	{
		int optionValue = JOptionPane.showOptionDialog(this, uiMessageBundle.getString("message.content.saveOnExit"), uiMessageBundle.getString("message.title.question"), JOptionPane.YES_NO_CANCEL_OPTION, JOptionPane.QUESTION_MESSAGE, null, null, JOptionPane.YES_OPTION);
		return (optionValue == JOptionPane.YES_OPTION);
	}
	
	private boolean askUserOverwriteExistedFile()
	{
		int optionValue = JOptionPane.showOptionDialog(this, uiMessageBundle.getString("message.content.overwrite"), uiMessageBundle.getString("message.title.question"), JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE, null, null, JOptionPane.YES_OPTION);
		return (optionValue == JOptionPane.YES_OPTION);
	}
	
	
	private boolean askUserOverwriteDefaultFile()
	{
		int optionValue = JOptionPane.showOptionDialog(this, uiMessageBundle.getString("message.content.overwriteDefault"), uiMessageBundle.getString("message.title.question"), JOptionPane.YES_NO_CANCEL_OPTION, JOptionPane.QUESTION_MESSAGE, null, null, JOptionPane.YES_OPTION);
		return (optionValue == JOptionPane.YES_OPTION);
	}
	
	private boolean askUserOverwriteExistedImage()
	{
		int optionValue = JOptionPane.showOptionDialog(this, uiMessageBundle.getString("message.content.overwriteImage"), uiMessageBundle.getString("message.title.question"), JOptionPane.YES_NO_CANCEL_OPTION, JOptionPane.QUESTION_MESSAGE, null, null, JOptionPane.YES_OPTION);
		return (optionValue == JOptionPane.YES_OPTION);
	}
	
	private void saveArduBlockToFile(String ardublockString, File saveFile) throws IOException
	{
		context.saveArduBlockFile(saveFile, ardublockString);
		context.setSaveFileName(saveFile.getName());
		context.setSaveFilePath(saveFile.getAbsolutePath());
	}
	
	public void doNewArduBlockFile()
	{
		//get current workspace-zoomLevel
		//TODO: get Page non static
		//TODO: check if zoom handling is possible in WorkspacecController
		@SuppressWarnings("static-access")
		double currentWorkspaceZoom = context.getWorkspace().getPageNamed("Main").getZoomLevel();
		
		if (context.isWorkspaceChanged())
		{
			int optionValue = JOptionPane.showOptionDialog(this, uiMessageBundle.getString("message.question.newfile_on_workspace_changed"), uiMessageBundle.getString("message.title.question"), JOptionPane.YES_NO_CANCEL_OPTION, JOptionPane.QUESTION_MESSAGE, null, null, JOptionPane.YES_OPTION);
			if (optionValue != JOptionPane.YES_OPTION)
			{
				return ;
			}
		}
		this.setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
		context.resetWorkspace();
		context.setWorkspaceChanged(false);
		this.setTitle(this.makeFrameTitle());
		this.setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
		
		//set Menu Mode letsgoING
		try {
			context.getWorkspaceController().loadProject(getArduBlockString(), null , blockMenuMode);
	        /*if(workspaceModeState){
	        	context.getWorkspaceController().loadProject(getArduBlockString(), null , "custom");	
	        }*/
		} catch (Exception e1) {
			//e1.printStackTrace();
		}
		//Set workspace to current zoomLevel
		context.getWorkspace().setWorkspaceZoom(currentWorkspaceZoom);

	}
	
	private File checkFileSuffix(File saveFile)
	{
		String filePath = saveFile.getAbsolutePath();
		if (filePath.endsWith(".abp"))
		{
			return saveFile;
		}
		else
		{
			return new File(filePath + ".abp");
		}
	}
	
	private void removeSearchableBlocks(Workspace workspace) {
		for (SearchableContainer con : getAllSearchableContainers(workspace)) {
	    	searchBar.removeSearchableContainer(con);
	    }
	}
	
	private void addSearchableBlocks(Workspace workspace) {
		for (SearchableContainer con : getAllSearchableContainers(workspace)) {
	    	searchBar.addSearchableContainer(con);
	    }
	}
	
	private Iterable<SearchableContainer> getAllSearchableContainers(Workspace workspace) {
        return workspace.getAllSearchableContainers();
    }
	
	
	public void saveProgramAsImage() {
		Workspace workspace = context.getWorkspace();
		Dimension size = workspace.getCanvasSize();
		Color transparent = new Color(0, 0, 0, 0);

		//System.out.println("size: " + size);
		BufferedImage bi = new BufferedImage(size.width, size.height, BufferedImage.TYPE_INT_ARGB); //letsgoING

		Graphics2D g = (Graphics2D)bi.createGraphics();
		g.setBackground(transparent);
		//double theScaleFactor = (300d/72d); 
		//g.scale(theScaleFactor,theScaleFactor);

		workspace.getBlockCanvas().getPageAt(0).getJComponent().paint(g);
		
		//CHANGE BACKGROUND COLOR FOR PRINT
		//*****************************************
		int maxPixelX = 0;
		int maxPixelY = 0;
		
	    for( int x = 0; x < bi.getWidth(); x++ ) {          // loop through the pixels
	        for( int y = 0; y < bi.getHeight(); y++ ) {
	            Color pixelColor = new Color(bi.getRGB(x, y));
	            if( pixelColor.getRed() == 128 && pixelColor.getGreen() == 128 && pixelColor.getBlue() == 128 ) {    
	            	bi.setRGB(x, y,transparent.getRGB());
	            }
	            else{ //GET SIZE OF BLOCKS ON IMAGE
	            	maxPixelX = Math.max(x,maxPixelX); 
					maxPixelY =  Math.max(y,maxPixelY);
	            }
	        }
	    }
	    
	    //System.out.println("Max X: " + maxPixelX +"  Max Y: " + maxPixelY +"Image W: " + bi.getWidth() +  "Image H: " +bi.getHeight());
	    
	    BufferedImage ci = bi.getSubimage(0, 0, Math.min(maxPixelX+20, bi.getWidth()), Math.min(maxPixelY+20, bi.getHeight())); 
		//*****************************************
	    
		try{
			final JFileChooser fc = new JFileChooser();
			String fileName = "ardublock.png";
			
			FileNameExtensionFilter filterPNG = new FileNameExtensionFilter("png", "png");
	        fc.setFileFilter(filterPNG);
	        fc.setAcceptAllFileFilterUsed(false);
	        
	        if(context.getSaveFileName().contains(".")) {
	        	fileName = context.getSaveFileName().substring(0, context.getSaveFileName().lastIndexOf('.')) + ".png";
	        }
	        else {	
	        	fileName = context.getSaveFileName() + ".png";
	        }
	        
	        //set std-name if program is unsaved
	        if(context.getSaveFileName().equals("untitled")){
	        	fileName = "ardublock.png";
	        }
	    
	        //open SaveDialog with filename and save image
			fc.setSelectedFile(new File(fileName));
			int returnVal = fc.showSaveDialog(workspace.getBlockCanvas().getJComponent());
			
	        if (returnVal == JFileChooser.APPROVE_OPTION) {
	        	File file = fc.getSelectedFile();
	        	
	        	if(!file.getName().contains(".png")) {
	        		file = new File (file.getAbsolutePath()+".png");
	        	}

	            if(file.exists() && !askUserOverwriteExistedImage()) {
	            	return;
	            }
	            else {
	            	ImageIO.write(ci,"png",file); 
	            }
	        }
		} catch (Exception e1) {
			
		} finally {
			g.dispose();
		}
	}

	@SuppressWarnings("unused")
	private void resetModeState(){ //letsgoING
		workspaceModeState = false; 
	}
	
	public boolean getModeState(){ //letsgoING
		return workspaceModeState; 
	}
	
	private void removeAllActionListeners(JButton button) { //letsgoING
		for( ActionListener al : button.getActionListeners() ) {
			button.removeActionListener( al );
	    }
	}
	
	//TODO: TESTTESTTEST
	private void addLibrary(String libraryName) 
	{
		//TEST TODO: validate
		//copy provided libraries to ArduinoLibraries
		String libraryPath           = null;
		String libraryResource        ="/com/ardublock/libraries/"+libraryName;
		String arduinoLibraryPath     = null;
		File   libraryHeaderFile        = null;	
		File   librarySourceFile        = null;	
		
		//get current .jar path for temp files
		//TODO: change path to sketchbook-folder
        try {
    		arduinoLibraryPath = new File(URLDecoder.decode(getClass().getProtectionDomain().getCodeSource().getLocation().getFile(), "UTF-8")).getParentFile().getPath();
    		arduinoLibraryPath = arduinoLibraryPath +File.separatorChar+".."+File.separatorChar+".."+File.separatorChar+".."+File.separatorChar+"libraries";
		} catch (UnsupportedEncodingException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
    	
    	File arduinoLibFolder = new File(arduinoLibraryPath);
    	
    	/*if(!arduinoLibFolder.exists()) {
    		System.out.println("No libraries-folder - Nothing to do for me...");
    	}else {*/
    	if(arduinoLibFolder.exists()) {
    		libraryPath = arduinoLibraryPath + File.separatorChar + libraryName;
    		File libraryFolder = new File(libraryPath);
    		if(!libraryFolder.exists()) {
    			libraryFolder.mkdir();
    			libraryFolder.deleteOnExit();
    			while(!libraryFolder.exists());
    		}
			try {
	            InputStream inputHeader = getClass().getResourceAsStream(libraryResource+".h");
	        	if(inputHeader != null) {
		            libraryHeaderFile = new File(libraryPath+File.separatorChar+libraryName+".h");
		            OutputStream outHeader = new FileOutputStream(libraryHeaderFile);
		            int read;
		            byte[] bytes = new byte[1024];
		    	    while ((read = inputHeader.read(bytes)) != -1) {
		   	            outHeader.write(bytes, 0, read);
		   	        }
		    	    outHeader.close();
		    	    //libraryHeaderFile.deleteOnExit();
	        	}else {
	        		//TODO: handle if no lib is found
	        	}
	            InputStream inputSource = getClass().getResourceAsStream(libraryResource+".cpp");
	    	  	if(inputSource != null) {
  		    	  	int read;
		            byte[] bytes = new byte[1024];
		            librarySourceFile = new File(libraryPath+File.separatorChar+libraryName+".cpp");
	    	        OutputStream outSource = new FileOutputStream(librarySourceFile);
		    	    while ((read = inputSource.read(bytes)) != -1) {
		    	    	outSource.write(bytes, 0, read);
		   	        }
		    	    outSource.close();
		    	    //librarySourceFile.deleteOnExit();
	    	  	}else {
	    	  	//TODO: handle if no lib is found
	        	}
	    	  	InputStream inputCSource = getClass().getResourceAsStream(libraryResource+".c");
	    	  	if(inputCSource != null) {
  		    	  	int read;
		            byte[] bytes = new byte[1024];
		            librarySourceFile = new File(libraryPath+File.separatorChar+libraryName+".c");
	    	        OutputStream outSource = new FileOutputStream(librarySourceFile);
		    	    while ((read = inputSource.read(bytes)) != -1) {
		    	    	outSource.write(bytes, 0, read);
		   	        }
		    	    outSource.close();
		    	    //librarySourceFile.deleteOnExit();
	    	  	}else {
	    	  	//TODO: handle if no lib is found
	    	  	}
	   	    } catch (IOException ex) {
	   	        ex.printStackTrace();
	   	    }
    	}
	}

}
