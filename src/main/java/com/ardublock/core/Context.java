package com.ardublock.core;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.ResourceBundle;
import java.util.Set;


import processing.app.Editor;
import processing.app.PreferencesData;

import com.ardublock.ui.listener.OpenblocksFrameListener;
import edu.mit.blocks.controller.WorkspaceController;
import edu.mit.blocks.renderable.RenderableBlock;
import edu.mit.blocks.workspace.Workspace;


public class Context
{
	public final static String LANG_DTD_PATH = "/com/ardublock/block/lang_def.dtd";
	public final static String ARDUBLOCK_LANG_PATH = "/com/ardublock/block/ardublock.xml";
	public final static String ARDUBLOCK_LANG_RESOURCE_BUNDLE = "com/ardublock/block/ardublock";
	public final static String DEFAULT_ARDUBLOCK_PROGRAM_PATH = "/com/ardublock/defaultProgram.abp";
	public final static String DEFAULT_ARDUBLOCK_PREFRENCES_PATH = "/com/ardublock/defaultArduBlockPreferences.txt";
	public final static String ARDUINO_VERSION_UNKNOWN = "unknown";
	public final boolean isNeedAutoFormat = true;	
	
	private static Context singletonContext;
	
	private boolean workspaceChanged;
	private boolean workspaceEmpty;
	
	private Set<RenderableBlock> highlightBlockSet;
	private Set<OpenblocksFrameListener> ofls;
	private boolean isInArduino = false;
	private String arduinoVersionString = ARDUINO_VERSION_UNKNOWN;
	private OsType osType; 

	//TODO change AppName? by letsgoING
	final public static String APP_NAME = "ArduBlock";
	
	
	private Editor editor;
	
	public enum OsType
	{
		LINUX,
		MAC,
		WINDOWS,
		UNKNOWN,
	};
	
	private String saveFilePath;
	private String saveFileName;
	
	//final public static String VERSION_STRING = " ";
	
	public static Context getContext()
	{
		if (singletonContext == null)
		{
			synchronized (Context.class)
			{
				if (singletonContext == null)
				{
					singletonContext = new Context();
				}
			}
		}
		return singletonContext;
	}
	
	private WorkspaceController workspaceController;
	private Workspace workspace;
	
	private Context()
	{
		//set default values to preferences.txt
		if(!PreferencesData.has("ardublock")){
			setDefaultPreferences();
		}
				
		workspaceController = new WorkspaceController();
		resetWorkspace();
		workspace = workspaceController.getWorkspace();
		workspaceChanged = false;
		highlightBlockSet = new HashSet<RenderableBlock>();
		ofls = new HashSet<OpenblocksFrameListener>();
		this.workspace = workspaceController.getWorkspace();
		
		isInArduino = false;
		
		osType = determineOsType();
	}
	
	public void resetWorkspace()
	{
		/*
		 * workspace = new Workspace(); workspace.reset(); workspace.setl
		 */

		// Style list
		List<String[]> list = new ArrayList<String[]>();
		String[][] styles = {};
		
		//		{ "//BlockGenus[@name[starts-with(.,\"Tinker\")]]/@color", "128 0 0" },
		//		{ "//BlockGenus[@name[starts-with(.,\"df_\")]]/@color",	"0 128 0" } };

		for (String[] style : styles) {
			list.add(style);
		}
		workspaceController.resetWorkspace();
		workspaceController.resetLanguage();
		
		if(PreferencesData.getBoolean("ardublock.cstyle")) {
			if(Locale.getDefault().equals(Locale.GERMANY)) {
				workspaceController.setLangResourceBundle(ResourceBundle.getBundle(ARDUBLOCK_LANG_RESOURCE_BUNDLE, Locale.GERMAN));
			} else if(Locale.getDefault().equals(Locale.ENGLISH)) {
				workspaceController.setLangResourceBundle(ResourceBundle.getBundle(ARDUBLOCK_LANG_RESOURCE_BUNDLE, Locale.UK));
			} else {
				workspaceController.setLangResourceBundle(ResourceBundle.getBundle(ARDUBLOCK_LANG_RESOURCE_BUNDLE));
			}
		} else {
			workspaceController.setLangResourceBundle(ResourceBundle.getBundle(ARDUBLOCK_LANG_RESOURCE_BUNDLE));
		}
		
		workspaceController.setStyleList(list);
		workspaceController.setLangDefDtd(this.getClass().getResourceAsStream(LANG_DTD_PATH));
		workspaceController.setLangDefStream(this.getClass().getResourceAsStream(ARDUBLOCK_LANG_PATH));
		workspaceController.loadFreshWorkspace();
		loadDefaultArdublockProgram();

		saveFilePath = null;
		saveFileName = "untitled";
		workspaceEmpty = true;
	}
	
	private void loadDefaultArdublockProgram()
	{	
		workspaceController.resetWorkspace();
		String loadPath = "";
		
		try {
			//TODO: set defaultFileName in ardublock.properties
			loadPath = new File(URLDecoder.decode(getClass().getProtectionDomain().getCodeSource().getLocation().getFile(), "UTF-8")).getParentFile().getPath()+ File.separator +"defaultProgram.abp";
		} catch (Exception e2) {
			// TODO Auto-generated catch block
			//e2.printStackTrace();
			System.out.println("No default programm.");
		} 
		try {
			if(PreferencesData.get("ardublock.workspace.mode").contains("custom") || PreferencesData.get("ardublock.workspace.mode").contains("page") || PreferencesData.get("ardublock.workspace.mode").contains("default") ) {
				workspaceController.loadProjectFromPath(loadPath, PreferencesData.get("ardublock.workspace.mode"));
			}else {
				workspaceController.loadProjectFromPath(loadPath, "default");
			}
			
		} catch (Exception e1) {
			try {
		    	InputStream is = getClass().getResourceAsStream(DEFAULT_ARDUBLOCK_PROGRAM_PATH);
			    InputStreamReader isr = new InputStreamReader(is);
			    BufferedReader br = new BufferedReader(isr);
			    String defaultFileString = new String();
			    
			    String line = null;
			    while ((line = br.readLine()) != null) {
			    	defaultFileString += line;
			    }
			    
		    	br.close();
		 	    isr.close();
				is.close();
				
				
				try {
					if(PreferencesData.get("ardublock.workspace.mode").contains("custom") || PreferencesData.get("ardublock.workspace.mode").contains("page") || PreferencesData.get("ardublock.workspace.mode").contains("default") ) {
						workspaceController.loadProject(defaultFileString, null , PreferencesData.get("ardublock.workspace.mode"));
					} else {
						workspaceController.loadProject(defaultFileString, null , "default");
					}
				} catch (Exception e) {
					workspaceController.loadProject(defaultFileString, null , "default");
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
	
	private void setDefaultPreferences() {//letsgoING
		PreferencesData.setBoolean("ardublock", true);
		PreferencesData.setBoolean("ardublock.cstyle", false);
		PreferencesData.setBoolean("ardublock.autosave", true);
		//PreferencesData.set("ardublock.ui.language", "DE");			//TODO: implement
		PreferencesData.set("ardublock.workspace.mode", "default"); //"default" / "custom" / "page"
		PreferencesData.setInteger("ardublock.workspace.zoom", 10); // float values between 
		//PreferencesData.set("ardublock.workspace.code", "simple");  //TODO: implement
}
	
	//determine OS
	private OsType determineOsType()
	{
		String osName = System.getProperty("os.name");
		osName = osName.toLowerCase();

		if (osName.contains("win"))
		{
			return Context.OsType.WINDOWS;
		}
		if (osName.contains("linux"))
		{
			return Context.OsType.LINUX;
		}
		if(osName.contains("mac"))
		{
			return Context.OsType.MAC;
		}
		return Context.OsType.UNKNOWN;
	}
	
	public File getArduinoFile(String name)
	{
		//String path = System.getProperty("user.dir");
		String path = null;
		
		try {
			path = new File(URLDecoder.decode(getClass().getProtectionDomain().getCodeSource().getLocation().getFile(), "UTF-8")).getParentFile().getPath();
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		path = path + File.separatorChar+".."+ File.separatorChar+".."+ File.separatorChar+".."+ File.separatorChar; //from tools/ArduBlockTool/tool/ to Arduino-root 
		
		//TODO: check on MAC and WIN
		/*
		if (osType.equals(OsType.MAC))
		{
			String javaroot = System.getProperty("javaroot");
			if (javaroot != null)
			{
				path = javaroot;
			}
		}*/
		File workingDir = new File(path);
		return new File(workingDir, name);
	}

	public WorkspaceController getWorkspaceController() {
		return workspaceController;
	}
	
	public Workspace getWorkspace()
	{
		return workspace;
	}

	public boolean isWorkspaceChanged()
	{
		return workspaceChanged;
	}

	public void setWorkspaceChanged(boolean workspaceChanged) {
		this.workspaceChanged = workspaceChanged;
	}
	
	public void highlightBlock(RenderableBlock block)
	{
		block.updateInSearchResults(true);
		highlightBlockSet.add(block);
	}
	
	public void cancelHighlightBlock(RenderableBlock block)
	{
		block.updateInSearchResults(false);
		highlightBlockSet.remove(block);
	}
	
	public void resetHightlightBlock()
	{
		for (RenderableBlock rb : highlightBlockSet)
		{
			rb.getBlock().setBad(false); //reset bad-state after error - added by letsgoING 
			rb.updateInSearchResults(false);
		}
		highlightBlockSet.clear();
	}
	
	public void saveArduBlockFile(File saveFile, String saveString) throws IOException
	{
		if (!saveFile.exists())
		{
			saveFile.createNewFile();
		}
		FileOutputStream fos = new FileOutputStream(saveFile, false);
		fos.write(saveString.getBytes("UTF8"));
		fos.flush();
		fos.close();
		didSave();
	}
	
	public void loadArduBlockFile(File savedFile) throws IOException
	{
		if (savedFile != null)
		{
			saveFilePath = savedFile.getAbsolutePath();
			saveFileName = savedFile.getName();
			workspaceController.resetWorkspace();
			workspaceController.loadProjectFromPath(saveFilePath);
			didLoad();
		}
	}
	
	public void setEditor(Editor e) {
		editor = e;
	}
	
	public Editor getEditor() {
		return editor;
	}
	
	
	public boolean isInArduino() {
		return isInArduino;
	}

	public void setInArduino(boolean isInArduino) {
		this.isInArduino = isInArduino;
	}

	public String getArduinoVersionString() {
		return arduinoVersionString;
	}

	public void setArduinoVersionString(String arduinoVersionString) {
		this.arduinoVersionString = arduinoVersionString;
	}

	public OsType getOsType() {
		return osType;
	}

	public void registerOpenblocksFrameListener(OpenblocksFrameListener ofl)
	{
		ofls.add(ofl);
	}
	
	public void didSave()
	{
		for (OpenblocksFrameListener ofl : ofls)
		{
			ofl.didSave();
		}
	}
	
	public void didLoad()
	{
		for (OpenblocksFrameListener ofl : ofls)
		{
			ofl.didLoad();
		}
	}
	
	public void didGenerate(String sourcecode, boolean upload) //added param "upload" by letsgoING
	{
		for (OpenblocksFrameListener ofl : ofls)
		{
			ofl.didGenerate(sourcecode, upload); //added param "upload" by letsgoING
		}
	}

	public String getSaveFileName() {
		return saveFileName;
	}

	public void setSaveFileName(String saveFileName) {
		this.saveFileName = saveFileName;
	}

	public String getSaveFilePath() {
		return saveFilePath;
	}

	public void setSaveFilePath(String saveFilePath) {
		this.saveFilePath = saveFilePath;
	}

	public boolean isWorkspaceEmpty() {
		return workspaceEmpty;
	}

	public void setWorkspaceEmpty(boolean workspaceEmpty) {
		this.workspaceEmpty = workspaceEmpty;
	}
}
