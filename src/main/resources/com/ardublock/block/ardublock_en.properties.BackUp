
###############
#  ARDUBLOCK  #
###############

#FILE
#****************************
ardublock.file.suffix=Ardublock Program File(*.abp)

#UI
#****************************
ardublock.ui.menu.file=File
ardublock.ui.menu.program=Program
ardublock.ui.menu.tools=Tools
ardublock.ui.menu.blocks=Blocks
ardublock.ui.menu.about=About
ardublock.ui.menu.help=?

ardublock.ui.new=New
ardublock.ui.new.tooltip=new program
ardublock.ui.save=Save
ardublock.ui.save.tooltip=save program
ardublock.ui.saveAs=Save As
ardublock.ui.saveAs.tooltip=save program as
ardublock.ui.load=Open
ardublock.ui.load.tooltip=open program
ardublock.ui.upload=Upload
ardublock.ui.upload.tooltip=Upload to Arduino
ardublock.ui.clone=Clone (rightclick-Ctrl)
ardublock.ui.clone_all=Clone following (rightclick-Shift-Ctrl)
ardublock.ui.add_comment=Add Comment
ardublock.ui.delete_comment=Delete Comment
ardublock.ui.organize_blocks=Organize all blocks
ardublock.ui.create_refer=create reference
ardublock.ui.website=letsgoING
ardublock.ui.website.domain=https://letsgoing.org
ardublock.ui.website.tooltip=goto letsgoING project-page
ardublock.ui.lmssite=Online LMS
ardublock.ui.lmssite.domain=moodle.letsgoing.org
ardublock.ui.lmssite.tooltip=go to letsgoING online-LMS
ardublock.ui.reference=open block reference
ardublock.ui.blockreference=open block reference
ardublock.ui.blockreference.domain=https://blockreferenz.letsgoing.org/
ardublock.ui.blockreference.tooltip=open block reference
ardublock.ui.website.repository=https://gitlab.reutlingen-university.de/letsgoing/ardublock
ardublock.ui.website.repository.tooltip=go to ArduBlock Repository
ardublock.ui.mode.expert=Pro
ardublock.ui.mode.expert.tooltip=Mehr Bl�cke und komplexere Funktionen fuer Fortgeschrittene
ardublock.ui.mode.standard=Basic
ardublock.ui.mode.standard.tooltip=Die wichtigsten Bl�cke f�r den Einstieg
ardublock.ui.mode.sim=Sim
ardublock.ui.mode.sim.tooltip=ArduBlock Basic mit Simulation "letsgoING-Grundlagen-Hardware"
ardublock.ui.search=search block
ardublock.ui.search.tooltip=Search for blocks in the drawers and workspace
ardublock.ui.serialMonitor=Serial Monitor
ardublock.ui.serialMonitor.tooltip=open Serial Monitor
ardublock.ui.serialPlotter=Serial Plotter
ardublock.ui.serialPlotter.tooltip=open Serial Plotter
ardublock.ui.undo=Undo
ardublock.ui.undo.tooltip=undo
ardublock.ui.redo=Redo
ardublock.ui.redo.tooltip=redo
ardublock.ui.saveImage=Save as image
ardublock.ui.saveImage.tooltip=save current program as image... 
ardublock.ui.saveDefaultProgram=save as default
ardublock.ui.saveDefaultProgram.tooltip=saves current program as default program
ardublock.ui.zoomIn=+
ardublock.ui.zoomIn.tooltip=zoom in Ctrl-Plus
ardublock.ui.zoomOut=-
ardublock.ui.zoomOut.tooltip=zoom out Ctrl-Minus
ardublock.ui.zoomReset=0
ardublock.ui.zoomReset.tooltip=reset zoom
ardublock.ui.copy=copy
ardublock.ui.copy.tooltip=copy program to clipboard
ardublock.ui.paste=paste
ardublock.ui.paste.tooltip=paste program from
ardublock.ui.help=Help ArduBlock
ardublock.ui.help.tooltip=Help ArduBlock
ardublock.ui.simulation=simulate
ardublock.ui.simulation.tooltip=start simulation with basic block-set and letsgoING modules
ardublock.ui.simulatorHelp=Help Simulator
ardublock.ui.simulatorHelp.tooltip=About the letsgoING-simulator
ardublock.ui.simulatorHelp.title=About the letsgoING-simulator
ardublock.ui.simulatorHelp.text=

ardublock.ui.version=v2.3

ardublock.ui.appprefix.standard=Basic
ardublock.ui.appprefix.pro=Pro
ardublock.ui.appprefix.sim=Simulator

#ERROR
#****************************
ardublock.error_msg.unset_variable=The variable is undefined. Before the variable can be read, a value must be assigned to it.
ardublock.error_msg.digital_var_slot=Digital variable slot must take a 'digital variable' name.\nHint: Look at the 'variable' socket on the highlighted block
ardublock.error_msg.number_var_slot=Standard variable slot must take a standard 'analog variable name'.\nHint: Look at the 'variable' socket on the highlighted block
ardublock.error_msg.number_local_var_slot=Variable slot must take a standard 'local analog variable name'.\nHint: Look at the 'variable' socket on the highlighted block
ardublock.error_msg.number_slot=This slot must take a standard 'analog variable name' or "analog value".\nHint: Look at the 'variable' socket on the highlighted block
ardublock.error_msg.long_number_var_slot=Large integer variable slot must take a 'Large Integer' variable name.\nHint: Look at the 'variable' socket on the highlighted block
ardublock.error_msg.double_number_var_slot=variable slot must take a 'Decimal' variable name.\nHint: Look at the 'variable' socket on the highlighted block
ardublock.error_msg.char_var_slot=Char variable slot must take a char variable name.\nHint: Look at the 'variable' socket on the highlighted block
ardublock.error_msg.number_const_write=Constants can be written only once. Don\'t use the setter-block twice!
ardublock.error_msg.array_var_slot='Array variable' slot must take an 'array variable' name.\nHint: Look at the 'variable' socket on the highlighted block
ardublock.error_msg.array_size_slot='Array size' slot must take a standard integer constant
ardublock.error_msg.string_var_slot=String variable slot must take a String variable name.\nHint: Look at the 'variable' socket on the highlighted block
ardublock.error_msg.string_slot=String slot must take a String variable or a String variable name.\nHint: Look at the 'variable' socket on the highlighted block
ardublock.error_msg.Digital_pin_slot=Pin# must be a valid digital pin number found on an Arduino Board
ardublock.error_msg.Analog_pin_slot=Pin# must be a valid analog input pin number found on an Arduino Board
ardublock.error_msg.local_var_slot=Variable slot must take a standard 'local X variable name'.\nHint: Look at the 'variable' socket on the highlighted block
ardublock.error_msg.var_value_slot=The slot must take a 'variable' or 'value' block.\nHint: Look at the highlighted block
ardublock.error_msg.return_var_slot=Only one return-parameter is valid
ardublock.error_msg.Serial_Read_slot=This Block needs an 'Serial read'- or 'softSerial read'-block
ardublock.error_msg.wrong_block_header=This Block cannot be used in head-section
ardublock.error_msg.stepper_duplicated=There is already a stepper with this name
ardublock.error_msg.stepper_not_existing=There is no stepper with this name
ardublock.error_msg.paste.invalid=clipboard contains no blocks
ardublock.error_msg.paste.notpossible=Past not possible without Blocks

ardublock.error_msg.no_sim_for_block=This Block is not suitable for simulation

#TRANSLATOR
#****************************
ardublock.translator.exception.subroutineNameDuplicated=two or more subroutines have the same name!
ardublock.translator.exception.noLoopFound=No loop found!
ardublock.translator.exception.multipleLoopFound=multiple loop block found!
ardublock.translator.exception.socketNull=A required Value, Variable Name or 'Test' is missing.\nHint:: Look for an empty 'socket' in the highlighted block.
ardublock.translator.exception.socketNull.consolePrint=Name of the block:
ardublock.translator.exception.subroutineNotDeclared=subroutine not declared
ardublock.translator.exception.subroutineNotDeclared.consolePrint=Name of the subroutine:

#MESSAGE
#****************************
message.title.error=Error
message.title.question=Question
message.content.open_unsaved=Ardublock program has changed, do you want to save?
message.content.overwrite=Do you want to overwrite existing file?
message.content.overwriteDefault=Do you want to overwrite default file?
message.file_not_found=File not found or permission denied
message.file_not_found_new_file=File Not Found or permission denied.\nCreate new file?
message.content.overwriteImage=Do you want to overwrite existing image?

message.question.newfile_on_workspace_changed=The program has changed, do you want to create a new Ardublock file?

##################
#  BLOCK CONFIG  #
##################

#COMMON
#****************************
bg.dummy=dummy
bg.dummy.description=

bc.commands=Commands
bc.pin_number=pin
bc.pin_number_analog=pin A
bc.pwm_pin_number=~pin
bc.value=value
bc.variable=variable
bc.digital=digital
bc.analog=analog
bc.char=character


#CONTROL
#****************************
bd.control=Control

bg.controlDivider1=|       main loops       |
bg.controlDivider2=|       branches       |
bg.controlDivider3=|       loops       |
bg.controlDivider4=|       delays       |

bg.loop=loop
#legacy support
bg.loop1=loop
bg.loop1.description=loop
bg.loop2=loop
bg.loop2.description=loop
bg.loop3=loop
bg.loop3.description=loop
#End of legacy support
bg.program=program
bg.sketch=program
bg.setup=setup
bg.if=if
bg.elseif=else if
bg.else=else
bg.ifelse=if/else
bg.repeat_times=for
bg.repeat=for (count var)
bg.repeat_control=
bg.break=break
bg.delay=delay MILLIS
bg.delay_microseconds=delay MICROS
bg.while=while
bg.do_while=do while

bg.wait=wait millis

bg.program.description=Main program with loop and setup
bg.sketch.description=program with head, setup and loop
bg.loop.description=Main Program loop without setup
bg.delay.description=Delay for "milliseconds" indicated.\nNOTE: 1000 millis = 1 second
bg.if.description=Run the commands if the "test" is true
bg.elseif.description=Run the commands if "if-block" is false and "test" ist true \n-> if-block is needed!!
bg.else.description=Run the commands if "if-block (and "else if") is false \n-> if-block is needed!!
bg.ifelse.description=Run the first set of commands if the "test" is true, otherwise it will do the second set of commands.
bg.while.description=Run the commands as long as the "test" is true
bg.do_while.description=Run the list of commands. Then, if 'test' is true, keep on repeating it until 'test' is 'false'
bg.repeat_times.description=Repeat a list of commands a number of times.
bg.repeat.description=Repeat a list of commands a number of times. "variable" counts the repetitions.
bg.repeat_control.description=
bg.break.description=break 
bg.delay_microseconds.description=Delay for "microseconds" indicated. \nNOTE: 1000000 micros = 1 second
bg.wait.description=wait millis


bc.head=
bc.setup=setup
bc.loop=loop
bc.loop.description=

bc.do=loop
bc.var=iteration variable name
bc.microseconds=microseconds
bc.milliseconds=milliseconds
bc.transmit=transmit
bc.repeat_times=times
bc.test=test
bc.then=then
bc.else=else
bc.condition=test
bc.start=start
bc.end=stop
bc.increment=steps of
bc.time=Second
bc.sub_var = subroutine return value
bc.sub_return = return val
bc.irName = isr name
bc.irNumber = number
bc.irType = type
bc.fcn_description=function description


#SUBROUTINES
#****************************
bd.subroutines = Subroutines

bg.subroutineDivider1=|       without parameters       |
bg.subroutineDivider2=|       with parameters       |
bg.subroutineDivider3=|       interrupt       |

bg.subroutine_param = subroutine
bg.subroutine_ref = subroutine
bg.subroutine_ref_digital = subroutine
bg.subroutine_ref_number = subroutine
bg.param_digital = connect
bg.param_number = connect
bg.param_char = connect
bg.param_ref_digital = connect
bg.param_ref_number = connect
bg.param_ref_char = connect
bg.interrupt=interrupt

bg.subroutine_param.description = sub
bg.subroutine_ref.description = sub
bg.subroutine_ref_digital.description = subroutine
bg.subroutine_ref_number.description = subroutine
bg.param_digital.description = bool parameter connector
bg.param_number.description = int parameter connector
bg.param_char.description = char parameter connector
bg.param_ref_digital.description = bool parameter connector
bg.param_ref_number.description = int parameter connector
bg.param_ref_char.description = char parameter connector
bg.interrupt.description=interrupt block\nIR-Nr.0 = Pin2\nIR-Nr.1 = Pin3\n\nIR-Mode\n0: LOW\n1: HIGH\n2: RISING\n3: FALLING\n4: CHANGE\n

bc.param_connector=next parameter



#legacy support
bg.subroutine_com=subroutine
bg.subroutine-ref=subroutine
bg.subroutine_var_com=subroutine_with_send
bg.subroutine-ref_var=subroutine_with_send
bg.subroutine_varret_com=subroutine_with_return
bg.subroutine-ref_varret=subroutine_with_return
bg.ref_var=variable_transmitted

bg.subroutine_com.description=A list of commands we can call by a single name.\nThey will run only if 'called', see CONTROL menu.
bg.subroutine-ref.description=Run the list of commands in the subroutine called...
bg.subroutine_var_com.description=A list of commands we can call by a single name.\nThey will run only if 'called', see CONTROL menu.
bg.subroutine-ref_var.description=Run the list of commands in the subroutine called...
bg.subroutine_varret_com.description=A list of commands we can call by a single name.\nThey will run only if 'called', see CONTROL menu.
bg.subroutine-ref_varret.description=Run the list of commands in the subroutine called...
bg.subroutine_varret=subroutine_with_return
bg.subroutine_var=subroutine_with_send
bg.subroutine=subroutine
bg.subroutine.description=A list of commands we can call by a single name.\nThey will run only if 'called', see CONTROL menu.
bg.subroutine_var.description=A list of commands we can call by a single name.\nThey will run only if 'called', see CONTROL menu.
bg.subroutine_varret.description=A list of commands we can call by a single name.\nThey will run only if 'called', see CONTROL menu.

#OUTPUT
#****************************
bd.output=Output

bg.outputDivider1=|       pins       |
bg.outputDivider2=|       NeoPixel LEDs       |
bg.outputDivider3=|       Toene       |
bg.outputDivider4=|       servomotors       |
bg.outputDivider5=|       steppermotors       |

bg.pin-write-analog=analogWrite
bg.pin-write-digital=digitalWrite
bg.pin-mode-output= pinMode OUTPUT
bg.servo_default=Servo
bg.tone=tone
bg.tone_time=tone
bg.no_tone=no tone
bg.attach_servo_default=attach servo
bg.detach_servo_default=detach Servo

bg.neopixel_init=Neopixel Init
bg.neopixel_pixel_color=Neopixel Pixel Color
bg.neopixel_show=Neopixel Show
bg.neopixel_brightness=Neopixel Brightness
bg.stepper_2pin=Setup for Stepper
bg.stepper_4pin=Setup for Stepper
bg.stepper_set_rpm=Stepper set rpm
bg.stepper_steps=Stepper steps

bg.attach_servo_default.description=attach servo
bg.detach_servo_default.description=detach servo
bg.servo_default.description=Control a servo
bg.stepper_2pin.description=Setup for stepper motor with 2-pin-wiring
bg.stepper_4pin.description=Setup for stepper motor with 4-pin-wiring
bg.stepper_set_rpm.description=Set stepper speed in rotation per minute
bg.stepper_steps.description=Move stepper x steps
bg.tone.description=Generates a square wave of the specified frequency (and 50%	duty cycle) on a pin.
bg.tone_time.description=Generates a square wave of the specified frequency (and 50% duty cycle) on a pin.
bg.no_tone.description=Stops the generation of a square wave triggered by tone().
bg.pin-write-digital.description=Set digital pin to HIGH or LOW
bg.pin-write-analog.description=Write analog voltage to pin. Eg \n 0 = Zero volts. 255 = five volts.
bg.pin-mode-output.description= set pinMode to OUTPUT
bg.neopixel_pixel_color.description=Pixel Color between 0 and 255 for each color
bg.neopixel_brightness.description=Brightness between 0 and 255
bg.neopixel_init.description=Init Neopixel\nBus: NEO_KHZ800 / NEO_KHZ400\nColor: NEO_GRB / NEO_RGB
bg.neopixel_show.description= Push the color data to the strip

bc.steps=Steps/r
bc.speed=rpm
bc.red=Red Start
bc.blue=Blue Start
bc.green=Green Start
bc.brightness=Brightness
bc.Nb_of_Led=how many pixel
bc.Nb_of_Led.description=how many pixel
bc.Pixel_Nb=number of the pixel
bc.Pixel_Nb.description=which pixel to program
bc.schnittstelle=Bus
bc.farbfolge=Color
bc.angle=angle
bc.frequency =frequency

#INPUT
#****************************
bd.input=Input

bg.inputDivider1=|       pins       |
bg.inputDivider2=|       touch       |
bg.inputDivider3=|       sensors       |

bg.pin-read-analog=analog input
bg.pin-read-digital=digital pin
bg.pin-read-digital-pullup=input pullup
bg.pin-mode-input=pinMode INPUT
bg.LGI_Ultrasonic=ultrasonic

bg.LGI_Qtouch_Init1=LGI_Qtouch_Init1
bg.LGI_Qtouch_Init2=LGI_Qtouch_Init2
bg.LGI_Qtouch_Init3=LGI_Qtouch_Init3
bg.LGI_Qtouch_Button1_getOffset=getOffset1
bg.LGI_Qtouch_Button2_getOffset=getOffset2
bg.LGI_Qtouch_Button3_getOffset=getOffset3
bg.LGI_Qtouch_Button1_getRaw=Raw-Value Q-Touch-Button1
bg.LGI_Qtouch_Button2_getRaw=Raw-Valuen Q-Touch-Button2
bg.LGI_Qtouch_Button3_getRaw=Raw-Value Q-Touch-Button3
bg.LGI_Qtouch_Button1_isTouched=Q-Button1 is touched
bg.LGI_Qtouch_Button2_isTouched=Q-Button2 is touched
bg.LGI_Qtouch_Button3_isTouched=Q-Button3 is touched
bg.LGI_Qtouch_Slider_Init=Slider Init
bg.LGI_Qtouch_Slider_getTouchPosition=get touch position
bg.LGI_Qtouch_Slider_getOffset1=getOffset1
bg.LGI_Qtouch_Slider_getOffset2=getOffset2
bg.LGI_Qtouch_Slider_getOffset3=getOffset3
bg.dht_sensor_setup=Setup DHT Sensor
bg.dht_sensor_read_temp=DHT read temperature
bg.dht_sensor_read_humid=DHT read humidity
bg.dht_sensor_calc_index=DHT calc heatindex

bg.pin-read-analog.description=Read analog value from pin. Zero volts = 0. Five volts = 1023
bg.pin-read-digital.description=Read digital value (HIGH or LOW) from pin.
bg.pin-read-digital-pullup.description=Set pin to digital input\nPin will report HIGH if unconnected.
bg.pin-mode-input.description= Set Pin-Mode to INPUT
bg.LGI_Ultrasonic.description=ultrasonic sensor
bg.LGI_Qtouch_Init1.description=LGI_Qtouch_Init1
bg.LGI_Qtouch_Init2.description=LGI_Qtouch_Init2
bg.LGI_Qtouch_Init3.description=LGI_Qtouch_Init3
bg.LGI_Qtouch_Button1_getOffset.description=getOffset1
bg.LGI_Qtouch_Button2_getOffset.description=getOffset2
bg.LGI_Qtouch_Button3_getOffset.description=getOffset3
bg.LGI_Qtouch_Button1_getRaw.description=get raw value
bg.LGI_Qtouch_Button2_getRaw.description=get raw value
bg.LGI_Qtouch_Button3_getRaw.description=get raw value
bg.LGI_Qtouch_Button1_isTouched.description=Button1 is touched
bg.LGI_Qtouch_Button2_isTouched.description=Button2 is touched
bg.LGI_Qtouch_Button3_isTouched.description=Button3 is touched
bg.LGI_Qtouch_Slider_Init.description=Slider Init
bg.LGI_Qtouch_Slider_getTouchPosition.description=get touch position
bg.LGI_Qtouch_Slider_getOffset1.description=getOffset1
bg.LGI_Qtouch_Slider_getOffset2.description=getOffset2
bg.LGI_Qtouch_Slider_getOffset3.description=getOffset3
bg.dht_sensor_setup.description=Setup for DHT sensor\nPin Nr.\nsensor-type\nDHT11, DHT21, DHT22
bg.dht_sensor_read_temp.description=read temperature in °C
bg.dht_sensor_read_humid.description=read humidity in %
bg.dht_sensor_calc_index.description=calculate heatindex in °C\n(felt temperature)

bc.analogpin_number=#AnalogPin
bc.dhtType=DHT
bc.DHTtemp=temperature
bc.DHThumid=humidity

#LOGIC
#****************************
bd.logic=Logical Operators
#bd.logic=Tests

bg.logicDivider1=|       logic operators      |
bg.logicDivider2=|       compare       |
bg.logicDivider3=|       bit operations       |

bg.not_equal_poly= !=
bg.equal_poly= ==
bg.equal_digital===
bg.not_equal_digital=!=
bg.greater=>
bg.greater_equal=>=
bg.less=<
bg.less_equal=<=
bg.not=not
bg.not_equal=!=
bg.and=and
bg.or=or
bg.equal_digital===
bg.not_equal_digital=!=
bg.bit_and=&
bg.bit_or=|
bg.bit_xor=^
bg.bit_not=~
bg.bit_shift_left=<<
bg.bit_shift_right=>>

bg.greater.description=True if the first number is greater the second number
bg.less.description=True if the first number is less then the second number
bg.equal.description=True if the first number equals to the second number
bg.equal_digital.description=True if the first number equals to the second number
bg.greater_equal.description=True if the first number is greater or equal to the second number
bg.less_equal.description=True if the first number is less or equal to the second number
bg.not_equal.description=True if the first number is not equal to the second number
bg.not_equal_digital.description=True if the first number is not equal to the second number
bg.and.description=True if both of the two conditions are true.
bg.or.description=True if one or both of the two conditions are true.
bg.not.description=The opposite of
bg.equal_poly.description=True if first character is equal to the second character
bg.not_equal_poly.description=True if first character is not equal to the second character
bg.bit_and.description=bitwise AND
bg.bit_or.description=bitwise OR
bg.bit_xor.description=bitwise XOR
bg.bit_not.description=bitwise NOT
bg.bit_shift_left.description=shift bits to the left
bg.bit_shift_right.description=shift bits to the right

#OPERATORS
#****************************
bd.operators=Math Operators

bg.mathDivider1=|       operators       |
bg.mathDivider2=|       signal attributes      |
bg.mathDivider3=|       random       |

bg.constrain=constrain
bg.abs=abs
bg.addition=+
bg.cos=cos
bg.division=\u00f7
bg.equal===
bg.map=map
bg.map_common=map [0, 1023] to [0, 255]
bg.max=max
bg.min=min
bg.modulo=%
bg.multiplication=x
bg.pow=power
bg.random=random
bg.random_range=random
bg.sin=sin
bg.sqrt=sqrt
bg.subtraction=-
bg.tan=tan

bg.addition.description=Sum of two numbers
bg.subtraction.description=Difference of two numbers
bg.multiplication.description=Product of two numbers
bg.division.description=Quotient of two numbers
bg.random.description=Generate a random number less than the number
bg.map_common.description=map value from [0, 1023] to [0, 255]
bg.map.description=Map value from "from" range to "to" range
bg.constrain.description=Constrain a number between two values
bg.random_range.description=Generate random number between min and (max - 1)

bc.max=max
bc.min=min
bc.to=to
bc.fromLow= from Low
bc.fromHigh=from High
bc.toLow=to Low
bc.toHigh=to High
bc.from=from
bc.base=base
bc.exponent=exponent
bc.low=lower
bc.high=higher
bc.element = element

#CAST
#****************************
bd.cast=type cast

bg.cast_number=cast analog
bg.cast_number_byte=cast short analog
bg.cast_number_long=cast long analog
bg.cast_number_float=cast decimal
bg.cast_byte_char=cast byte to char 
bg.cast_char_byte=cast char to byte
bg.cast_low_int_char=cast lower byte of int to char
bg.cast_up_int_char=cast upper byte of int to char
bg.cast_char_int=cast int from two char
bg.cast_byte_int=cast int from two byte

bg.cast_number.description=cast int
bg.cast_number_byte.description=cast byte
bg.cast_number_long.description=cast long
bg.cast_number_float.description=cast float
bg.cast_byte_char.description=cast byte to char
bg.cast_char_byte.description=cast char to byte
bg.cast_low_int_char.description=cast lower byte of int to char
bg.cast_up_int_char.description=cast upper byte of int to char
bg.cast_char_int.description=cast integer from two char
bg.cast_byte_int.description=cast integer from two byte

bc.lowerByte=lower Byte
bc.upperByte=upper Byte

#NUMBERS
#****************************
bd.number=Variables/Constants

bg.numberDivider1=|       system time      |
bg.numberDivider2=|       digital      |
bg.numberDivider3=|       analog       |
bg.numberDivider4=|       character/text       |

bg.millis=Milliseconds
bg.digital-high=HIGH
bg.digital-low=LOW
bg.true=true
bg.false=false

bg.string=ABCD
bg.char=A
bg.number=1
bg.setter_variable_digital=set digital variable
bg.setter_variable_number=set analog variable
bg.setter_variable_number_byte=set short analog variable
bg.setter_variable_number_long=set long analog variable
bg.setter_variable_number_float=set decimal variable
bg.setter_variable_char=set character variable
bg.setter_variable_string=set string variable
bg.variable_digital=digital variable
bg.constant_digital=digital constant
bg.variable_number=analog variable
bg.constant_number=analog constant
bg.variable_char=character_variable
bg.local_variable_char=local_character_variable
bg.variable_string=string variable
bg.local_variable_string=local string variable
bg.variable_file=fileVariable
bg.local_variable_file=lokalFileVariable
bg.local_variable_number=localAnalogVariable
bg.local_variable_digital= local digital variable

bg.millis.description=Returns milliseconds since last power-up or reset.\nNote: Use to set a 'Large Integer' variable.
bg.true.description=Boolean True.
bg.false.description=Boolean False.
bg.digital-high.description=Pin HIGH
bg.digital-low.description=Pin LOW
bg.char.description=character variable
bg.number.description=analog variable
bg.variable_number.description=analog variable.\nType depends on setter block
bg.constant_number.description= analog constant\nEvery constant can be written only once\nType depends on setter block
bg.variable_digital.description=Digital variable
bg.constant_digital.description=digital constant
bg.variable_string.description=string variable
bg.local_variable_string.description=local string variable
bg.setter_variable_number.description=Set standard integer variable.\n-32,768 to +32,767
bg.setter_variable_number_byte.description=Set standard byte (uint8_t) variable.\n0 to 255
bg.setter_variable_number_long.description=Set large integer variable. \n-2,147,483,648 to 2,147,483,647
bg.setter_variable_number_float.description=Set 'Decimal' variable.\n +/- 3.4028235E+38
bg.setter_variable_digital.description=Set digital variable.\nHIGH or LOW
bg.setter_variable_string.description=set string variable
bg.setter_variable_char.description=Set a character variable
bg.variable_char.description=Character variable name
bg.local_variable_char.description=local character variable name
bg.string.description=Edit message text
bg.variable_file.description=file variable
bg.local_variable_file.description=local file variable
bg.local_variable_number.description=local Variable\nType depends on setter block
bg.local_variable_digital.description=local digital Var

bc.message=String
bc.num=#
bc.variable_digital=Bool
bc.variable_number=Integer
bc.variable_number_unsigned_long=Unsigned_long
bc.variable_number_long = Long
bc.variable_number_float=float

#TEST NEW BLOCKDESIGN
bg.setter_variable_analog==
bg.setter_variable_analog.description=
bg.create_variable_analog==
bg.create_variable_analog.description=
bg.create_array_analog==
bg.create_array_analog.description=

#ARRAYS
#****************************
bd.array=Arrays

bg.arrayDivider1=|       analog      |
bg.arrayDivider2=|       string      |

bg.create_char_array=create char[]
bg.read_char_array=read character
bg.setter_char_array=set character
bg.read_number_array=read element
bg.create_number_array=create an int array
bg.create_number_byte_array=create an byte array
bg.create_number_long_array=create an long array
bg.create_number_float_array=create an float array
bg.setter_number_array=set an array member

bg.create_char_array.description=create char[]-Array
bg.read_char_array.description=read character at position
bg.setter_char_array.description=set character at psition
bg.read_number_array.description=Get the value of an array member
bg.create_number_array.description=Create an array of standard integers.\n-32,768 to +32,767
bg.create_number_byte_array.description=Create an array of byte values
bg.create_number_long_array.description=Create an array of long
bg.create_number_float_array.description=Create an array of float
bg.setter_number_array.description=Set the value of an array member

bc.position=position
bc.array=Name the array
bc.vecsize=size
bc.number_chars=number of characters

#COMMUNICATION
#****************************
bd.communication=Communication

bg.communicationDivider1=|       serial      |
bg.communicationDivider2=|       software serial      |
bg.communicationDivider3=|       serial connectors      |
bg.communicationDivider4=|       PubSub Network      |

bg.glue_poly=connect
bg.glue_msg=connect
bg.glue_sb=connect
bg.glue_sn=connect
bg.serial_parseInt=Serial.parseInt
bg.serial_available=Serial.available
bg.serial_read=Serial.read
bg.serial_print=Serial.print
bg.serial_begin=Serial.begin 
bg.soft_serial_begin=softSerial.begin 
bg.soft_serial_print=softSerial.print
bg.soft_serial_read=softSerial.read
bg.soft_serial_available=softSerial.available
#bg.serial_println=serial println
bg.serial_write=serial.write()
bg.serialEvent=serialEvent
bg.glue_digital=glue
bg.glue_number=glue
bg.glue_char=glue
bg.glue_string=glue
bg.tele_set_frame=set Frame
bg.tele_read_frame=read serial data Frame
bg.tele_read_data_frame=read from Frame
bg.mqtt_set_frame=set MQTT Frame
bg.mqtt_read_frame=read MQTT Frame

bg.psn_broker=PubSub Broker
bg.psn_client=PubSub Client
bg.psn_callback=psnClientCallback
bg.psn_handle_network=PubSub handleNetwork
bg.psn_data_to_send=PubSub isDataToSend?
bg.psn_publish=PubSub publish
bg.psn_subscribe=PubSub subscribe
bg.psn_unsubscribe=PubSub unsubscribe


bg.serial_write.description=Send message via Serial
bg.serial_print.description=Send message via Serial port
bg.serial_read.description=read char via Serial port as byte
bg.serial_available.description=Data available at Serial port
#bg.serial_println.description=Send message via Serial port with a return
bg.serial_begin.description=start Serial with baudrate
bg.soft_serial_begin.description=start SoftwareSerial with baudrate
bg.soft_serial_print.description=Send message via Software Serial port
bg.soft_serial_read.description=Read message via Software Serial port
bg.soft_serial_available.description=Data available at Software Serial port
bg.serial_parseInt.description=read next int value via Serial
bg.glue_sn.description=connector number/char-Array
bg.glue_sb.description=connector digital/char-Array
bg.glue_poly.description=connector char/char-Array
bg.glue_msg.description=connector char-Array/char-Array
bg.serialEvent.description=Unterprogramm serialEvent z.B. read Remote
bg.glue_digital.description=Glue boolean to string
bg.glue_number.description=Glue number to string
bg.glue_char.description=Glue char to string
bg.glue_string.description=Glue string to string
bg.tele_set_frame.description=write data to telegram-frame
bg.tele_read_frame.description=read serial data in telegram-frame-format
bg.tele_read_data_frame.description=read data from telegram-frame \n receiver-address 0 reads data from all sender-addresses
bg.mqtt_set_frame.description=write data to MQTT-frame\n(letsgoING-IOT-module)
bg.mqtt_read_frame.description=read data from MQTT-frame\n(letsgoING-IOT-module)

bg.psn_broker.description=PubSub Server
bg.psn_client.description=PubSub Client
bg.psn_callback.description=PubSub_Callback
bg.psn_handle_network.description=PubSub handleNetwork
bg.psn_data_to_send.description=PubSub isDataToSend?
bg.psn_publish.description=PubSub publish
bg.psn_subscribe.description=PubSub subscribe
bg.psn_unsubscribe.description=PubSub unsubscribe

bc.ps_name=name
bc.callback=callback
bc.payloadVar=payload var
bc.payloadLengthVar=payload length var
bc.topicVar=topic var
bc.topicLengthVar=topic length var
bc.print=print
bc.ln=new line
bc.rx_pin_number=Rx Pin
bc.tx_pin_number=Tx Pin
bc.baudrate=baud
bc.sendAddress=sender address
bc.recAddress=receiver address
bc.dataLength=data length
bc.sendBuffer=send Buffer
bc.recBuffer=receive Buffer
bc.recFrame=received Frame
bc.recChars=received characters
bc.payload=payload
bc.payloadLength=payload length
bc.topic=topic
bc.topicLength=topic length
bc.command=command type
bc.nextChar=next char
bc.dataComplete=data complete

#STORAGE
#****************************
bd.storage=Storage

bg.storageDivider1=|       EEPROM      |
bg.storageDivider2=|       SD Card      |

bg.eeprom_read=read 'byte' EEPROM
bg.eeprom_write=write 'byte' EEPROM
bg.eeprom_get=read custom Datatype EEPROM
bg.eeprom_put=write custom Datatype EEPROM
bg.eeprom_length=size of EEPROM
bg.sd_begin=start SD card
bg.sd_open=open file on SD
bg.sd_close=close file on SD
bg.sd_write=write to SD
bg.sd_read=read from SD
bg.sd_data_available=data available SD
bg.sd_file_available=file available SD

bg.eeprom_read.description=Read a byte (0-255) from EEPROM. Each byte consumes just one EEPROM address. Using consecutive addresses is OK
bg.eeprom_write.description=Write a byte (0-255) to EEPROM. Each byte consumes just one EEPROM address. Using consecutive addresses is OK
bg.eeprom_get.description=Read a custom datatype from EEPROM. Each byte consumes one EEPROM address. Check length of your datatype.
bg.eeprom_put.description=Write a custom datatype to EEPROM. Each byte consumes one EEPROM address. Check length of your datatype.
bg.eeprom_length.description=Returns the size of EEPROM 
bg.sd_begin.description=start SD card
bg.sd_open.description=open file on SD
bg.sd_close.description=close file on SD
bg.sd_write.description=write to SD
bg.sd_read.description=read from SD
bg.sd_data_available.description=unread data available on SD
bg.sd_file_available.description=file available

bc.eeprom_address=address
bc.eeprom_data=data
bc.File=File
bc.sdFileVar  = File Variable
bc.sdFileName = File Name
bc.sdFileMode = Mode
bc.sdWriteData= Data
bc.sdCSPin    = CS

#CODE
#****************************
bd.code=Comments/Code

bg.codeDivider1=|       comments      |
bg.codeDivider2=|       c-code insert      |

bg.program_comment=program description 
bg.code_head=add code on Head
bg.code_comment_head=//add comment to program head
bg.code_loop=add code
bg.code_comment= //add comment

bg.program_comment.description=Add information about the program
bg.code_loop.description=Add custom code
bg.code_head.description=Add custom code to head
bg.code_comment_head.description=Add comment to program head
bg.code_comment.description=add custom command

bc.prog_name=program name
bc.prog_author= author
bc.prog_description=program description
bc.prog_date=date
bc.prog_version=version
#****************************